# PlantUML

Vytvořit [**zde**](http://www.plantuml.com/plantuml/) (viz [reference](http://plantuml.com/class-diagram#More)) + použít odkaz z *View as PNG/SVG*.


## Stručně

![První příklad](http://www.plantuml.com/plantuml/svg/LO_VIiSm34RlynJVdI3q7UQVgMyXtHgrq3QZlmoiwzxTXO7CgoJVoI5akAclkkP4Np4mvbZdn6VFUT66p7dYUYFYel6OnxDi1E0UCLZyqMBvVFUgDKwhiY7dpeNiI_0gjVt7icYKujCbtOdkyCDL81LIc1xGnJdfU3J2ttxMr63a2RqxrpkUBuwTQZHYDsd3Yo4f_4TUiPkquOq6Bk5u-HS0)

```plantUML
@startuml
hide circle
hide empty members

entity Entity {
   * identifying_attribute
   --
   * mandatory_attribute
   optional_attribute
}

' zero or one
A |o--o| B

' exactly one
C ||--|| D

' zero or many
E }o--o{ F

' one or many
G }|--|{ H
@enduml
```

## Podrobněji

![Druhý příklad](http://www.plantuml.com/plantuml/svg/ZLLDJnin4BtlhvWk1x9LFcYfUeYYY2WDBH8VKaXxAIXouadMZTVUsbx2gkf_xzZPPHD05Lp6ddb-xytC0cVMCUEAJ4R7KAYbpZ9K3boA88L2MA3KMn0hA7LnR10AAzGQ29ON3WriXKk1WK4d50CkR2vPIKns8rJE3Ck0vm9EJqwYTnnNdd365X9RYcNOvcYN2ImbinPy0fY5s2TZ60oWDMiVdP-Fw2U1uP0ujrwT76d9OQKDv4Pap9ImGQ-uDeYAZ4bjGhLGudU1L62AquMZiKjjCDIKTdDLI5cRggdcHDLwJ62m63uEUejXNQ_8GYaez8EQ1uLh0woP0xJmWC9RrpHF0uwjBR079gIVGP_o0p4Sz0GXI98jD_tnpUNDT5Ypjc5Bpx1gIj3RwXY6HJ5rcQnIIyrziwqmil4ueRD5x1F4vVrUZzQY_9AxjBbUc3ZQhdz-TAKDHgdW21cwL7ExkrX7ojgLETg8k50vuOG_x_P7Yp1jY7suKwtl540eq8K1MzBJB8aJ-1FjZrNm12wkPvElasdqDALghfx6zvGay6Cq7NyRJN-UVhnlO3dvtMh3c_BdJ_VHtzeCzMA5LkIdEf6bwXJMxUdVQeYWy_zOhNa3vbKpWI4tUYKaagFzAzcwrHc2K2iDUlMwhvSMs1hXzcfqUKc9Q8t-Ksxk2s5Soba9NqQpoUpYQj8uMHjTv6GXHD8EiTQrRnn8MCl-LfvrMeiq5r8oA9cZATbKF0c6tyC0U4FB-yNmvg3wImk5VCxSGTF7i2zhwQuLTfpkK3WeHS5EfmFxku4-nAEGeMUoX3kLCkiPe0B4Eq9m6Z9MlaeuXFghQyWyDcKFj7iwgh96M4yN4e5F7H9IRSGvvSsc-jCBKcnmfnD_Y8DM_3w6PkhF91T87OXACm18CSJc_KctU_sI8Xmz1n4ijNAiPl508Zb3nVr_Yty0)

```plantUML
@startuml
' uncomment the line below if you're using computer with a retina display
' skinparam dpi 300

!define Enum(name,desc) class name as "desc" << (E,#FFFFAA) >>
!define Table(name,desc) class name as "desc" << (T,#FFAAAA) >>
' we use bold for primary key
' green color for unique
' and underscore for not_null
!define primary_key(x) <b>x</b>
!define unique(x) <color:green>x</color>
!define not_null(x) <u>x</u>
' other tags available:
' <i></i>
' <back:COLOR></color>, where color is a color name or html color code
' (#FFAACC)
' see: http://plantuml.com/classes.html#More
hide methods
hide stereotypes

' entities

Table(user, "user\n(User in our system)") {
    primary_key(id) INTEGER
    not_null(unique(username)) VARCHAR[32]
    not_null(password) VARCHAR[64]
}

Table(session, "session\n(session for user)") {
    primary_key(id) INTEGER
    not_null(user_id) INTEGER
    not_null(unique(session_id) VARCHAR[64]
}

Table(user_profile, "user_profile\n(Some info of user)") {
    primary_key(user_id) INTEGER
    age SMALLINT
    gender SMALLINT
    birthday DATETIME
}

Table(group, "group\n(group of users)") {
    primary_key(id) INTEGER
    not_null(name) VARCHAR[32]
}

Table(user_group, "user_group\n(relationship of user and group)") {
    primary_key(user_id) INTEGER
    primary_key(group_id) INTEGER
    joined_at DATETIME
}

' relationships
' one-to-one relationship
user -- user_profile : "A user only \nhas one profile"
' one to may relationship
user --> session : "A user may have\n many sessions"
' many to many relationship
' Add mark if you like

user "1" --> "*" user_group : "A user may be \nin many groups"
group "1" --> "0..N" user_group : "A group may \ncontain many users"
@enduml
```