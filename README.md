# Nemocnice

Aplikace pro správu nemocnice.

# Zadání

Aplikace bude obsahovat minimálně následující funkcionalitu: 

* Správa uživatelů
    * [x] Existence (aspoň) 2 úrovní oprávnění - doktor a pacient (a oprávnění tomu logicky odpovídající)
    * [x] Doktoři tvoří účty pacientům
    * [x] Upravování vlastního profilu (jméno, datum narození)
* Správa karet pacientů
    * [x] Prohlížení karet (pacienty)
    * [x] Úprava karet (doktory) - přidávání chorob
* Správa chorob
    * [x] Vytvoření, zobrazení, mazání a editace choroby a jejich symptomů
    * [x] Diagnostika dle symptomů (zadání symptomů vede k určení choroby
* API
    * [x] Bude umožňovat zobrazit informace o pacientovi dle rodného čísla

## Další požadavky

* [x] Minimálně pět tabulek v databázi
* [x] Dodržení architektury prezentované na cvičeních
* [x] Integrace probraných návrhových vzorů a best practices (Unit of Work, Query Object, Repository, Facade pattern, Services, Data Transfer Objects…)
* [x] Netriviální (nonCRUD) funkcionalita
* [x] Dependency Injection
* [x] Web API
* [x] Přihlašování uživatelů
* [x] ID entit jako GUID, ne integer

## Doplnění po prezentaci

* [ ] Homepage
* [ ] Otisk hesla
* [ ] Přesměrování chyb na vlastní stránku
* [ ] Víc iniciálních dat 
* [ ] Filtrování/vyhledávání symptomů (při větším počtu)
* [ ] Chyby databáze
    * [ ] První registrace
    * [ ] Úprava nemoci
* [ ] Skrytí obsahu podle oprávnění uživatele

# Návrh

![Model](http://www.plantuml.com/plantuml/svg/bLLBRzj64BxpLqnT59PGoOdJno5mXQYsd1X9PCEL0nHf4Ap8iRaMkSlkBjL8SO2SYbvowQb1JZhcOEHU8BxG-YFj7-aClQGbeuOJmJ0vxvbl7hnddJ0kJvCWbX62TMXGkqc6zeA1QUOca68wH6E3u8i83wL2wAayRIYHOYj26pOXJ8Irm0mG5bQOkG8R6z3ejOvq4ZMXqwbj1sAOu2SP3rfVRcztwSVsby4p8rDX9iz7E6cyQCB6iFDYuzQm0tTXfp_etU_jrvg7sg0yKhLcg1DjxXuPHCKs9SbssxlxlPtx_USFUp_Nji8SNMzJQ-LA_ffZgPYpEByKAUsUgpn9Ig5aeQGYLWax9zqa24eCZKvmG72tO6MVNg64VgK9BmEWtvvm4fM37-1sIM_fq6b3v3fJ5onfq3FlmArL64_Sk9ZDtw8gJY7I8nBDFxGWFN-dw8zuEZF5REm9aUbe3LPl1Q-2eEmB9rGbKwUn2BYCcYLlROqVbqLwdBZRg8oEEbLb5MHDmhdNFtZCPakDgLraQ59fhTIgsQmJMk1IMdirmScnfsiJhDsAq94-Xt_ItTzyqDr_-lstptmDroV3wL-aGIo4eNPEf-JZeLOZKxntZ5wcnwIbYlTm_aOgfQsJb8g6Oup8A95Zah2VjL_K_2-CfCC45AOwL394IESVvcU4jHAaD8sAqmHqv5icMf3Dtyg8EYKSA4sUl6G2ecocT4kTNAlWoYqQ0kg07WpHpV3u2MOxNi7c8b3Vt7a603nM2eSQB79vrfs_Ws4UwRpCCKx4VAOfJQRGmiuMpzDIY4nO-visqPKUiCwoub0Q5_TpFZQiouhTpK5l_-dwUfM8t_zYPhW3ewGuFNy3xGxncLw_VVihtibHCNDCBsVY0sm9X-n-gple3NOUzxpdyIJJvs-AKrVCkD8f61rHRsCDpBM9094xdGedmz6ayYoeoqY-VjpTVTJhzcaBdSdnKwVhgZmK15Amokol4tqaLME1qhcho7OOOpYwN8O67wlbzV5pc-2Xup1y9VUkuiPNauNBHsvJJXSqIedNoZi7bz0MVqVY6EdYUQ7ixEcnL8IO5q6Zlpk0_i6ZHytozA6AW7Eg9fMBfKdTf0TFwgUiifCfRXkTLjqhfSmyK4ucddaZ-48cAIgtU6H2hPoGod8-_lM6wtTDxwvmwrXrGumyYXrbEj1nWf3g-PcYUHlFFmmnz4ESQKURD1HqDXhM2Pw-YvDYQV6fVHzvqsWYNPu1emV__FudT7sW-X3uo5IH5ttMEIXFmRMrsacQEPqk01WBnkw1-6pOQe1kZk4v5EUdYpSVw_z2NQqdGXiQcV6NOEd2tVdk6GTOXITJ9-994F0TX5UwtJuvAMztmF_WvAJTrY_XSiKksEqsL9yvRragdW0F0qLP-VVrwwtoRFFTpoqrCgK30tQiZoMC4s5TkOt2k7nj9IYjJdGPgff8x-L9SHgo7Zh_tG0zj28kxwxJ-VmCmbWRFSn1m9XqFLaf3ukF3iRWYlT4iH-wuJnUUtgKHo1u4caACn5AL0uyq9CMZ2Ai7IkSqgYcs08dHc393T2t9oM1vCcDoC5KP5Qu9fVYqsSe6H3Vvi0_Aq1ZKHNOHyXf3MOOnb36AJw6iS-4Yk1uaRPvn1zTZ181qsDfdNG-FLwMXs9-nbbuJbcIi9cU_y7hWwhoHkd5eixCCuDxgA8yJVu3)