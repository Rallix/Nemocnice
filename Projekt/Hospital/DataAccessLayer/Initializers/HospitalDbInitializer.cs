﻿using System;
using System.Data.Entity;
using DataAccessLayer.Enums;
using DataAccessLayer.Entities;

namespace DataAccessLayer.Initializers
{

    class HospitalDbInitializer : DropCreateDatabaseIfModelChanges<HospitalDbContext>
    {

        protected override void Seed(HospitalDbContext context)
        {
            //! Create
            var directorRole = new Role
            {
                    Id = Guid.Parse("69ff4b9b-7ceb-486a-8261-176d6aa1508d"),
                    RoleType = RoleTypes.Director
            };
            context.Permissions.Add(new Permission
            {
                    Id = Guid.Parse("5824e875-6db9-40e7-b203-d322bf3277bb"),
                    Action = "Everything",
                    Role = directorRole
            });
            var director = new User
            {
                    Id = Guid.Parse("fd4a7ecc-c46f-4c6f-b298-c3838417cba3"),
                    Role = directorRole,
                    FirstName="Andrej",
                    Surname="Novak",
                    Username = "director",
                    Password = "heslo123",
                    Alive = true,
                    BirthDate = new DateTime(1965, 4, 8),
                    BirthNumber = "650408/2112",
                    Card = new Card()
            };
            var ailment = new Ailment
            {
                    Id = Guid.Parse("0e825ac0-9724-40c7-8720-19f5229da72a"),
                    Name = "Arthritis"
            };
            var symptom1 = new Symptom
            {
                    Id = Guid.Parse("c15abe9b-a917-42f5-91df-a851a39540cf"),
                    Description = "joint pain"
            };
            var symptom2 = new Symptom
            {
                    Id = Guid.Parse("8a889bc3-f7db-452f-9bbb-17afcac04070"),
                    Description = "stiffness"
            };
            var issue = new Issue
            {
                    Id = Guid.Parse("ad68fc25-4a50-4ee5-8691-ac73163981e7"),
                    Card = director.Card,
                    Since = DateTime.Now,
                    Until = DateTime.Now,
                    Solved = false
            };
            var treatment = new Treatment
            {
                    Id = Guid.Parse("3e03c382-f59a-43e7-94f0-0a334faf9770"),
                    Issue = issue,
                    Date = DateTime.Now,
                    Message = "The director seems to be ill."
            };
            var treatmentSymptom1 = new TreatmentSymptom
            {
                    Id = Guid.Parse("a62409ca-6fcc-4cc9-aae7-fe0a6e84fb8e"),
                    Symptom = symptom1,
                    Treatment = treatment
            };

            var treatmentSymptom2 = new TreatmentSymptom
            {
                    Id = Guid.Parse("f362ecba-53a8-4217-82ba-981ca749ca70"),
                    Symptom = symptom2,
                    Treatment = treatment
            };

            var ailmentSymptom1 = new AilmentSymptom
            {
                    Id = Guid.Parse("b65a0fd6-e6b7-45ab-be86-c020f5661af0"),
                    Ailment = ailment,
                    Symptom = symptom1
            };

            var ailmentSymptom2 = new AilmentSymptom
            {
                    Id = Guid.Parse("4b92eefe-88d9-47fb-8161-02d6db82ad17"),
                    Ailment = ailment,
                    Symptom = symptom2
            };


            //! Add to database
            context.Users.Add(director);
            context.Issues.Add(issue);
            context.Treatments.Add(treatment);
            context.Ailments.Add(ailment);
            context.Cards.Add(director.Card);
            context.AilmentSymptoms.Add(ailmentSymptom1);
            context.AilmentSymptoms.Add(ailmentSymptom2);
            context.TreatmentSymptoms.Add(treatmentSymptom1);
            context.TreatmentSymptoms.Add(treatmentSymptom2);

            base.Seed(context);
        }

    }

}
