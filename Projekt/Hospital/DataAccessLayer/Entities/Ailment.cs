﻿using Hospital.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace DataAccessLayer.Entities
{

    /// <summary> An illness defined by its name and a set of symptoms. </summary>
    public class Ailment : IEntity
    {

        /// <summary> A global identifier unique to each entry in the table. </summary>
        public Guid Id { get; set; }

        /// <summary> Name of the database table for this entity. </summary>
        [NotMapped] public string TableName { get; } = nameof(HospitalDbContext.Ailments);

        /// <summary> A unique name of an illness or a certain condition. </summary>
        [Required, MaxLength(64), Index(IsUnique = true)] public string Name { get; set; }

        /// <summary> A list of symptoms this illness exhibits. </summary>
        [Required] public List<AilmentSymptom> SymptomAilments { get; set; }

        /// <summary> Adds symptoms to the ailment. </summary>
        /// <param name="symptoms">Symptoms of the ailment to add.</param>
        public void AddSymptoms(params Symptom[] symptoms)
        {            
            foreach (Symptom symptom in symptoms)
            {
                if (SymptomAilments.Select(sa => sa.Symptom.Id).Contains(symptom.Id))
                {
                    // already contains this symptom
                    continue;
                }
                var relation = new AilmentSymptom(this, symptom);
            }
        }

        public override string ToString() {
            return $"{Name} ({SymptomAilments?.Count ?? 0} symptom{((SymptomAilments?.Count ?? 0) != 1 ? "s" : "")})";
        }
    }

}
