﻿using Hospital.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccessLayer.Entities
{

    /// <summary> A user account in the system. </summary>
    public class User : IEntity, IValidatableObject
    {

        /// <summary> A global identifier unique to each entry in the table. </summary>
        public Guid Id { get; set; }

        /// <summary> Name of the database table for this entity. </summary>
        [NotMapped] public string TableName { get; } = nameof(HospitalDbContext.Users);

        /// <summary> A short unique name chosen by the user for login. </summary>
        [Required, Index(IsUnique = true), MaxLength(32)] public string Username { get; set; }
        /// <summary> A password used to log into the account. </summary>
        [Required, MaxLength(64)] public string Password { get; set; }

        // TODO: ~ secure password
        // [Required, StringLength(100)] public string PasswordSalt { get; set; }
        // [Required, StringLength(100)] public string PasswordHash { get; set; }


        [Required, MaxLength(30)] public string FirstName { get; set; }

        [Required, MaxLength(30)] public string Surname { get; set; }

        /// <summary> A national identification number of a citizen. </summary>
        /// <remarks> Birth date can be discerned from a birth number.
        /// Birth numbers are *not* guaranteed to be unique.</remarks>
        [Required, MaxLength(11, ErrorMessage = "{0} can only have up to 11 characters, including the slash.")]
        public string BirthNumber { get; set; }

        /// <summary> The date of birth. </summary>
        [Required] public DateTime BirthDate { get; set; }

        /// <summary> Is the patient currently still alive? </summary>
        public bool Alive { get; set; }

        /// <summary> All health documentation about a patient. </summary>
        /// <remarks> Doctors and directors do not necessarily have their own card.</remarks>
        [Key] public Card Card { get; set; }
        public Guid? CardId { get; set; }

        /// <summary> The role the user has in the system. Sets appropriate permissions. </summary>
        public Role Role { get; set; }
        [Required] public Guid RoleId { get; set; }

        /// <summary>Determines whether the specified object is valid.</summary>
        /// <returns>A collection that holds failed-validation information.</returns>
        /// <param name="validationContext">The validation context.</param>
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!BirthNumber.Contains("/"))
            {
                yield return new ValidationResult("The birth number should contain a forward slash to separate the digits.",
                                                  new[] {nameof(BirthNumber)});
            }
        }

        public override string ToString()
        {
            return $"{Username} ({Role})";
        }
    }

}
