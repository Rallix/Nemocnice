﻿using Hospital.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccessLayer.Entities
{

    /// <summary> An event or a problem with which the patient goes to a doctor. </summary>
    public class Issue : IEntity
    {

        /// <summary> A global identifier unique to each entry in the table. </summary>
        public Guid Id { get; set; }

        /// <summary> Name of the database table for this entity. </summary>
        [NotMapped] public string TableName { get; } = nameof(HospitalDbContext.Issues);

        /// <summary> The first time when this problem was discovered. </summary>
        public DateTime Since { get; set; }
        /// <summary> The time when this problem was finished or stopped being actively treated. </summary>
        public DateTime? Until { get; set; }
        /// <summary> Is this issue already solved? </summary>
        public bool Solved { get; set; }
        /// <summary> All medical records of a patient. </summary>
        public Card Card { get; set; }
        [Required] public Guid CardId { get; set; }
        /// <summary> The name of an ailment if discovered/applicable. </summary>
        public Ailment Ailment { get; set; }
        public Guid? AilmentId { get; set; }
        /// <summary> Treatment sessions related to this issue. </summary>
        public List<Treatment> Treatments { get; set; }

        public override string ToString()
        {            
            return $"Issue '{Ailment}' ({(Solved ? "✔️" : "❌")}) | {Treatments?.Count ?? 0} treatment session{((Treatments?.Count ?? 0) != 1 ? "s" : "" )}";
        }

    }

}
