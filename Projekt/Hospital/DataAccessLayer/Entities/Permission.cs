﻿using Hospital.Infrastructure;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccessLayer.Entities
{

    /// <summary> The actual permissions for specific actions available to concrete roles. </summary>
    /// <remarks> The <see cref="Entities.Role.RoleTypes"/> should be understood as subsets
    /// – e.g. it's not possible to have a permission for patients only without the director having it as well.</remarks>
    public class Permission : IEntity
    {
        /// <summary> A global identifier unique to each entry in the table. </summary>
        public Guid Id { get; set; }

        /// <summary> Name of the database table for this entity. </summary>
        [NotMapped] public string TableName { get; } = nameof(HospitalDbContext.Permissions);

        /// <summary> An action a user can perform. </summary>
        [Required] public string Action { get; set; }
        /// <summary> The minimal role with an access to the action. </summary>
        public Role Role { get; set; }
        public Guid RoleId { get; set; }

        public override string ToString()
        {
            return $"{Role}: {Action}";
        }

    }

}
