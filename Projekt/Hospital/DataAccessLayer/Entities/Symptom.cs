﻿using Hospital.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccessLayer.Entities
{

    /// <summary> A symptom of an ailment. </summary>
    public class Symptom : IEntity
    {

        /// <summary> A global identifier unique to each entry in the table. </summary>
        public Guid Id { get; set; }

        /// <summary> Name of the database table for this entity. </summary>
        [NotMapped]
        public string TableName { get; } = nameof(HospitalDbContext.Symptoms);

        /// <summary> A short summary of the symptom. </summary>
        [Required, Index(IsUnique = true), MaxLength(25)]
        public string Description { get; set; }

        /// <summary> A list of ailments having this specific symptom. </summary>
        public List<AilmentSymptom> SymptomAilments { get; set; }

        /// <summary> A list of patient treatments during which this symptom was discovered. </summary>
        /// <summary>Returns a string that represents the current object.</summary>
        /// <returns>A string that represents the current object.</returns>
        public override string ToString()
        {
            var symptom = (Description.Length > 24)
                    ? Description.Substring(0, 24) + "…"
                    : Description;
            return symptom;
        }

    }

}
