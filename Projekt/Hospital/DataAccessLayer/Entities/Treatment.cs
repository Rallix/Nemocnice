﻿using Hospital.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccessLayer.Entities
{
    /// <summary> A single session of a doctor and a patient. </summary>
    /// <remarks> Provides information about a single visit or some important event, e.g. an operation, new discovery or death.</remarks>
    public class Treatment : IEntity
    {

        /// <summary> A global identifier unique to each entry in the table. </summary>
        public Guid Id { get; set; }

        /// <summary> Name of the database table for this entity. </summary>
        [NotMapped] public string TableName { get; } = nameof(HospitalDbContext.Treatments);

        /// <summary> The date in which this treatment session occured. </summary>
        public DateTime Date { get; set; }
        /// <summary> The details of this treatment session. </summary>
        [MaxLength(1000)] public string Message { get; set; }

        public Issue Issue { get; set; }
        public Guid IssueId { get; set; }

        /// <summary> A list of new symptoms discovered during this treatment session. </summary>
        public List<TreatmentSymptom> TreatmentSymptoms { get; set; }

        public override string ToString()
        {
            var message = (Message.Length > 24) ? Message.Substring(0, 24) + "…" : Message;
            return $"[{Date.ToShortDateString()}] {message}";
        }

    }

}
