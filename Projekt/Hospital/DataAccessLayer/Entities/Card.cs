﻿using Hospital.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccessLayer.Entities
{

    /// <summary> All medical records of a single patient. </summary>
    public class Card : IEntity
    {

        /// <summary> A global identifier unique to each entry in the table. </summary>
        public Guid Id { get; set; }

        /// <summary> Name of the database table for this entity. </summary>
        [NotMapped] public string TableName { get; } = nameof(HospitalDbContext.Cards);

        /// <summary> The user which this card belongs to. </summary>
        [Key, Required, ForeignKey("User")] public Guid UserId { get; set; }
        public User User { get; set; }
        // TODO: Reference: Přiřazený konkrétní doktor ke každému uživateli?

        /// <summary> A list of issues (illnesses, important events) related to the patient's health. </summary>
        public List<Issue> Issues { get; set; }

        public override string ToString()
        {
            return $"Card ({User}) | {Issues?.Count ?? 0} issue{((Issues?.Count ?? 0) != 1 ? "s" : "")}";
        }
    }

}
