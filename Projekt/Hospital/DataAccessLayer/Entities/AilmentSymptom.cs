﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Hospital.Infrastructure;

namespace DataAccessLayer.Entities
{

    /// <summary> A relation of a <see cref="Symptom"/> and a specific <see cref="Ailment"/>. </summary>
    public class AilmentSymptom : IEntity
    {

        /// <summary> A global identifier unique to each entry in the table. </summary>
        public Guid Id { get; set; }

        /// <summary> Name of the database table for this entity. </summary>
        [NotMapped] public string TableName { get; } = nameof(HospitalDbContext.AilmentSymptoms);

        public Ailment Ailment { get; set; }
        [Required] public Guid AilmentId { get; set; }

        public Symptom Symptom { get; set; }
        [Required] public Guid SymptomId { get; set; }

        public AilmentSymptom() { }
        public AilmentSymptom(Ailment ailment, Symptom symptom)
        {
            Ailment = ailment;
            AilmentId = ailment.Id;
            Symptom = symptom;
            SymptomId = symptom.Id;

            ailment.SymptomAilments?.Add(this);
            symptom.SymptomAilments?.Add(this);
        }

        public override string ToString()
        {
            return $"{Ailment}↔{Symptom}";
        }

    }

}
