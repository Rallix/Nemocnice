﻿using Hospital.Infrastructure;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccessLayer.Entities
{

    /// <summary> A relation of <see cref="Symptom"/> discovered during a patient's <see cref="Treatment"/>. </summary>
    public class TreatmentSymptom : IEntity
    {

        /// <summary> A global identifier unique to each entry in the table. </summary>
        public Guid Id { get; set; }

        /// <summary> Name of the database table for this entity. </summary>
        [NotMapped] public string TableName { get; } = nameof(HospitalDbContext.TreatmentSymptoms);

        /// <summary> A symptom found during a treatment session. </summary>
        public Symptom Symptom { get; set; }
        [Required] public Guid SymptomId { get; set; }

        /// <summary> A single session of a doctor and a patient. </summary>
        /// <remarks> Provides information about a single visit or some important event, e.g. an operation, new discovery or death.</remarks>
        public Treatment Treatment { get; set; }
        [Required] public Guid TreatmentId { get; set; }

        public TreatmentSymptom() { }
        public TreatmentSymptom(Treatment treatment, Symptom symptom)
        {
            Treatment = treatment;
            TreatmentId = treatment.Id;
            Symptom = symptom;
            SymptomId = symptom.Id;

            treatment.TreatmentSymptoms?.Add(this);
        }

        public override string ToString()
        {
            return $"[Treatment {Treatment.Date.ToShortDateString()}] Discovered: {Symptom}";
        }

    }

}
