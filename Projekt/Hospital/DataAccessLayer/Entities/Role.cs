﻿using DataAccessLayer.Enums;
using Hospital.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccessLayer.Entities
{

    /// <summary> A role a <see cref="User"/> can have in the system. A permission tier tied to a list of permissions. </summary>
    public class Role : IEntity
    {

        /// <summary> The available permission tiers. </summary>
        /// 
        /// <summary> A global identifier unique to each entry in the table. </summary>
        public Guid Id { get; set; }

        /// <summary> Name of the database table for this entity. </summary>
        [NotMapped] public string TableName { get; } = nameof(HospitalDbContext.Roles);

        /// <summary> The permission tier of the role. </summary>
        [Required] public RoleTypes RoleType { get; set; }
        
        /// <summary> A list of permissions available to this specific role. </summary>
        public List<Permission> Permissions { get; set; }

        public override string ToString()
        {
            return $"{RoleType}";
        }

    }

}
