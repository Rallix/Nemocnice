﻿using System;
using System.Data.Entity;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Hospital.Infrastructure;
using Hospital.Infrastructure.EntityFramework;
using Hospital.Infrastructure.EntityFramework.UnitOfWork;
using Hospital.Infrastructure.Query;
using Hospital.Infrastructure.UnitOfWork;
using Component = Castle.MicroKernel.Registration.Component;

namespace DataAccessLayer.Config
{
    public class EntityFrameworkInstaller : IWindsorInstaller
    {
        internal const string ConnectionString = "Data source=(localdb)\\mssqllocaldb;Database=PV179Hospital;Trusted_Connection=True;MultipleActiveResultSets=true";

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                               Component.For<Func<DbContext>>()
                                        .Instance(() => new HospitalDbContext())
                                        .LifestyleTransient(), 
                               Component.For<IUnitOfWorkProvider>()
                                        .ImplementedBy<EntityFrameworkUnitOfWorkProvider>()
                                        .LifestyleSingleton(),
                               Component.For(typeof(IRepository<>))
                                        .ImplementedBy(typeof(EntityFrameworkRepository<>))
                                        .LifestyleTransient(),
                               Component.For(typeof(IQuery<>))
                                        .ImplementedBy(typeof(EntityFrameworkQuery<>))
                                        .LifestyleTransient()
                              );
        }
    }
}
