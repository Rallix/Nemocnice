﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Enums
{
    public enum RoleTypes
    {

        /// <summary> Basic permission set for hospital patients.
        /// A patient can generally only view and edit their own account.</summary>
        Patient = 0,
        /// <summary> Permission set for hospital employees.
        /// A doctor can create a patient's profile and log information about their ailemnts and treatments. </summary>
        Doctor = 1,
        /// <summary> The least restrictive permission set for administrators.
        /// A (hospital) director can create accounts for doctors, change permissions and edit data. </summary>
        Director = 2,

        Unregistered = 3

    }
}
