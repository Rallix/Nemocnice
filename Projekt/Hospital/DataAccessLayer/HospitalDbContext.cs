﻿using System.Data.Common;
using System.Data.Entity;
using DataAccessLayer.Config;
using DataAccessLayer.Entities;
using DataAccessLayer.Initializers;

namespace DataAccessLayer
{

    public class HospitalDbContext : DbContext
    {
        /// <summary> A role a <see cref="User"/> can have in the system. A permission tier tied to a list of permissions. </summary>
        public DbSet<Role> Roles { get; set; }
        /// <summary> The actual permissions for specific actions available to concrete roles. </summary>
        /// <remarks> The <see cref="Entities.Role.RoleTypes"/> should be understood as subsets
        /// – e.g. it's not possible to have a permission for patients only without the director having it as well.</remarks>
        public DbSet<Permission> Permissions { get; set; }
        /// <summary> A user account in the system. </summary>
        public DbSet<User> Users { get; set; }
        /// <summary> All medical records of a single patient. </summary>
        public DbSet<Card> Cards { get; set; }
        /// <summary> An event or a problem with which the patient goes to a doctor. </summary>
        public DbSet<Issue> Issues { get; set; }
        /// <summary> An illness defined by its name and a set of symptoms. </summary>
        public DbSet<Ailment> Ailments { get; set; }
        /// <summary> A single session of a doctor and a patient. </summary>
        /// <remarks> Provides information about a single visit or some important event, e.g. an operation, new discovery or death.</remarks>
        public DbSet<Treatment> Treatments { get; set; }
        /// <summary> A symptom of an ailment. </summary>
        public DbSet<Symptom> Symptoms { get; set; }
        /// <summary> A relation of a <see cref="Symptom"/> and a specific <see cref="Ailment"/>. </summary>
        public DbSet<AilmentSymptom> AilmentSymptoms { get; set; }
        /// <summary> A relation of <see cref="Symptom"/> discovered during a patient's <see cref="Treatment"/>. </summary>
        public DbSet<TreatmentSymptom> TreatmentSymptoms { get; set; }

        public HospitalDbContext() : base(EntityFrameworkInstaller.ConnectionString) // TODO: EntityFrameworkInstaller.ConnectionString
        {
            var instance = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
            Database.SetInitializer(new HospitalDbInitializer());
        }

        /// <summary>
        /// Ctor with db connection, required by data access layer tests
        /// </summary>
        /// <param name="connection">The database connection</param>
        public HospitalDbContext(DbConnection connection) : base(connection, true)
        {
            Database.CreateIfNotExists();
        }

        /*

        const string ConnectionString = "Data source=(localdb)\\mssqllocaldb;Database=Lab03DemoEshopDatabaseSample;Trusted_Connection=True;MultipleActiveResultSets=true";

        /// <summary>
        /// Non-parametric ctor used by data access layer
        /// </summary>
        public DemoEshopDbContext() : base(ConnectionString)
        {
            // force load of EntityFramework.SqlServer.dll into build
            var instance = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
        }

        /// <summary>
        /// Ctor with db connection, required by data access layer tests
        /// </summary>
        /// <param name="connection">The database connection</param>
        public DemoEshopDbContext(DbConnection connection) : base(connection, true)
        {
            Database.CreateIfNotExists();
        }

        */
    }
}
