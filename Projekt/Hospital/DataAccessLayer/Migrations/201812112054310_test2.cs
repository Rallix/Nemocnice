namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Issues", "Card_UserId", "dbo.Cards");
            DropIndex("dbo.Issues", new[] { "Card_UserId" });
            AlterColumn("dbo.Issues", "Card_UserId", c => c.Guid());
            CreateIndex("dbo.Issues", "Card_UserId");
            AddForeignKey("dbo.Issues", "Card_UserId", "dbo.Cards", "UserId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Issues", "Card_UserId", "dbo.Cards");
            DropIndex("dbo.Issues", new[] { "Card_UserId" });
            AlterColumn("dbo.Issues", "Card_UserId", c => c.Guid(nullable: false));
            CreateIndex("dbo.Issues", "Card_UserId");
            AddForeignKey("dbo.Issues", "Card_UserId", "dbo.Cards", "UserId", cascadeDelete: true);
        }
    }
}
