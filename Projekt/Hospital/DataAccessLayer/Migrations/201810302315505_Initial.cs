namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Ailments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 64),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true);
            
            CreateTable(
                "dbo.AilmentSymptoms",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AilmentId = c.Int(nullable: false),
                        SymptomId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Ailments", t => t.AilmentId, cascadeDelete: true)
                .ForeignKey("dbo.Symptoms", t => t.SymptomId, cascadeDelete: true)
                .Index(t => t.AilmentId)
                .Index(t => t.SymptomId);
            
            CreateTable(
                "dbo.Symptoms",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 25),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Description, unique: true);
            
            CreateTable(
                "dbo.TreatmentSymptoms",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SymptomId = c.Int(nullable: false),
                        TreatmentId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Symptoms", t => t.SymptomId, cascadeDelete: true)
                .ForeignKey("dbo.Treatments", t => t.TreatmentId, cascadeDelete: true)
                .Index(t => t.SymptomId)
                .Index(t => t.TreatmentId);
            
            CreateTable(
                "dbo.Treatments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        Message = c.String(maxLength: 1000),
                        IssueId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Issues", t => t.IssueId, cascadeDelete: true)
                .Index(t => t.IssueId);
            
            CreateTable(
                "dbo.Issues",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Since = c.DateTime(nullable: false),
                        Until = c.DateTime(nullable: false),
                        Solved = c.Boolean(nullable: false),
                        Ailment_Id = c.Int(),
                        Card_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Ailments", t => t.Ailment_Id)
                .ForeignKey("dbo.Cards", t => t.Card_Id, cascadeDelete: true)
                .Index(t => t.Ailment_Id)
                .Index(t => t.Card_Id);
            
            CreateTable(
                "dbo.Cards",
                c => new
                    {
                        Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Username = c.String(nullable: false, maxLength: 32),
                        Password = c.String(nullable: false, maxLength: 64),
                        BirthNumber = c.String(nullable: false, maxLength: 11),
                        BirthDate = c.DateTime(nullable: false),
                        Alive = c.Boolean(nullable: false),
                        Role_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Roles", t => t.Role_Id, cascadeDelete: true)
                .Index(t => t.Username, unique: true)
                .Index(t => t.Role_Id);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RoleType = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Permissions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Action = c.String(nullable: false),
                        Role_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Roles", t => t.Role_Id)
                .Index(t => t.Role_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AilmentSymptoms", "SymptomId", "dbo.Symptoms");
            DropForeignKey("dbo.TreatmentSymptoms", "TreatmentId", "dbo.Treatments");
            DropForeignKey("dbo.Treatments", "IssueId", "dbo.Issues");
            DropForeignKey("dbo.Issues", "Card_Id", "dbo.Cards");
            DropForeignKey("dbo.Cards", "Id", "dbo.Users");
            DropForeignKey("dbo.Users", "Role_Id", "dbo.Roles");
            DropForeignKey("dbo.Permissions", "Role_Id", "dbo.Roles");
            DropForeignKey("dbo.Issues", "Ailment_Id", "dbo.Ailments");
            DropForeignKey("dbo.TreatmentSymptoms", "SymptomId", "dbo.Symptoms");
            DropForeignKey("dbo.AilmentSymptoms", "AilmentId", "dbo.Ailments");
            DropIndex("dbo.Permissions", new[] { "Role_Id" });
            DropIndex("dbo.Users", new[] { "Role_Id" });
            DropIndex("dbo.Users", new[] { "Username" });
            DropIndex("dbo.Cards", new[] { "Id" });
            DropIndex("dbo.Issues", new[] { "Card_Id" });
            DropIndex("dbo.Issues", new[] { "Ailment_Id" });
            DropIndex("dbo.Treatments", new[] { "IssueId" });
            DropIndex("dbo.TreatmentSymptoms", new[] { "TreatmentId" });
            DropIndex("dbo.TreatmentSymptoms", new[] { "SymptomId" });
            DropIndex("dbo.Symptoms", new[] { "Description" });
            DropIndex("dbo.AilmentSymptoms", new[] { "SymptomId" });
            DropIndex("dbo.AilmentSymptoms", new[] { "AilmentId" });
            DropIndex("dbo.Ailments", new[] { "Name" });
            DropTable("dbo.Permissions");
            DropTable("dbo.Roles");
            DropTable("dbo.Users");
            DropTable("dbo.Cards");
            DropTable("dbo.Issues");
            DropTable("dbo.Treatments");
            DropTable("dbo.TreatmentSymptoms");
            DropTable("dbo.Symptoms");
            DropTable("dbo.AilmentSymptoms");
            DropTable("dbo.Ailments");
        }
    }
}
