namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IntToGuid : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Permissions", "Role_Id", "dbo.Roles");
            DropForeignKey("dbo.AilmentSymptoms", "AilmentId", "dbo.Ailments");
            DropForeignKey("dbo.Issues", "Ailment_Id", "dbo.Ailments");
            DropForeignKey("dbo.TreatmentSymptoms", "SymptomId", "dbo.Symptoms");
            DropForeignKey("dbo.AilmentSymptoms", "SymptomId", "dbo.Symptoms");
            DropForeignKey("dbo.TreatmentSymptoms", "TreatmentId", "dbo.Treatments");
            DropForeignKey("dbo.Treatments", "IssueId", "dbo.Issues");
            DropForeignKey("dbo.Issues", "Card_Id", "dbo.Cards");
            DropForeignKey("dbo.Cards", "Id", "dbo.Users");
            DropForeignKey("dbo.Users", "Role_Id", "dbo.Roles");
            DropIndex("dbo.AilmentSymptoms", new[] { "AilmentId" });
            DropIndex("dbo.AilmentSymptoms", new[] { "SymptomId" });
            DropIndex("dbo.TreatmentSymptoms", new[] { "SymptomId" });
            DropIndex("dbo.TreatmentSymptoms", new[] { "TreatmentId" });
            DropIndex("dbo.Treatments", new[] { "IssueId" });
            DropIndex("dbo.Issues", new[] { "Ailment_Id" });
            DropIndex("dbo.Issues", new[] { "Card_Id" });
            DropIndex("dbo.Cards", new[] { "Id" });
            DropIndex("dbo.Users", new[] { "Role_Id" });
            DropIndex("dbo.Permissions", new[] { "Role_Id" });
            RenameColumn(table: "dbo.Issues", name: "Ailment_Id", newName: "AilmentId");
            RenameColumn(table: "dbo.Issues", name: "Card_Id", newName: "CardId");
            RenameColumn(table: "dbo.Permissions", name: "Role_Id", newName: "RoleId");

            // int ID => GUID id
            DropPrimaryKey("dbo.Ailments");
            DropPrimaryKey("dbo.AilmentSymptoms");
            DropPrimaryKey("dbo.Symptoms");
            DropPrimaryKey("dbo.TreatmentSymptoms");
            DropPrimaryKey("dbo.Treatments");
            DropPrimaryKey("dbo.Issues");
            DropPrimaryKey("dbo.Cards");
            DropPrimaryKey("dbo.Users");
            DropPrimaryKey("dbo.Roles");
            DropPrimaryKey("dbo.Permissions");            
            AddColumn("dbo.Cards", "UserId", c => c.Guid(nullable: false));
            AddColumn("dbo.Users", "CardId", c => c.Guid());

            DropColumn("dbo.Ailments", "Id");
            AddColumn("dbo.Ailments", "Id", c => c.Guid(nullable: false));
            DropColumn("dbo.AilmentSymptoms", "Id");
            AddColumn("dbo.AilmentSymptoms", "Id", c => c.Guid(nullable: false));
            DropColumn("dbo.AilmentSymptoms", "AilmentId");
            AddColumn("dbo.AilmentSymptoms", "AilmentId", c => c.Guid(nullable: false));
            DropColumn("dbo.AilmentSymptoms", "SymptomId");
            AddColumn("dbo.AilmentSymptoms", "SymptomId", c => c.Guid(nullable: false));
            DropColumn("dbo.Symptoms", "Id");
            AddColumn("dbo.Symptoms", "Id", c => c.Guid(nullable: false));
            DropColumn("dbo.TreatmentSymptoms", "Id");
            AddColumn("dbo.TreatmentSymptoms", "Id", c => c.Guid(nullable: false));
            DropColumn("dbo.TreatmentSymptoms", "SymptomId");
            AddColumn("dbo.TreatmentSymptoms", "SymptomId", c => c.Guid(nullable: false));
            DropColumn("dbo.TreatmentSymptoms", "TreatmentId");
            AddColumn("dbo.TreatmentSymptoms", "TreatmentId", c => c.Guid(nullable: false));
            DropColumn("dbo.Treatments", "Id");
            AddColumn("dbo.Treatments", "Id", c => c.Guid(nullable: false));
            DropColumn("dbo.Treatments", "IssueId");
            AddColumn("dbo.Treatments", "IssueId", c => c.Guid(nullable: false));
            DropColumn("dbo.Issues", "Id");
            AddColumn("dbo.Issues", "Id", c => c.Guid(nullable: false));
            DropColumn("dbo.Issues", "AilmentId");
            AddColumn("dbo.Issues", "AilmentId", c => c.Guid());
            DropColumn("dbo.Issues", "CardId");
            AddColumn("dbo.Issues", "CardId", c => c.Guid(nullable: false));
            DropColumn("dbo.Cards", "Id");
            AddColumn("dbo.Cards", "Id", c => c.Guid(nullable: false));
            DropColumn("dbo.Users", "Id");
            AddColumn("dbo.Users", "Id", c => c.Guid(nullable: false));
            DropColumn("dbo.Users", "Role_Id");
            AddColumn("dbo.Users", "Role_Id", c => c.Guid(nullable: false));
            DropColumn("dbo.Roles", "Id");
            AddColumn("dbo.Roles", "Id", c => c.Guid(nullable: false));
            DropColumn("dbo.Permissions", "Id");
            AddColumn("dbo.Permissions", "Id", c => c.Guid(nullable: false));
            DropColumn("dbo.Permissions", "RoleId");                       
            AddColumn("dbo.Permissions", "RoleId", c => c.Guid(nullable: false));

            AddPrimaryKey("dbo.Ailments", "Id");
            AddPrimaryKey("dbo.AilmentSymptoms", "Id");
            AddPrimaryKey("dbo.Symptoms", "Id");
            AddPrimaryKey("dbo.TreatmentSymptoms", "Id");
            AddPrimaryKey("dbo.Treatments", "Id");
            AddPrimaryKey("dbo.Issues", "Id");
            AddPrimaryKey("dbo.Cards", "Id");
            AddPrimaryKey("dbo.Users", "Id");
            AddPrimaryKey("dbo.Roles", "Id");
            AddPrimaryKey("dbo.Permissions", "Id");

            CreateIndex("dbo.AilmentSymptoms", "AilmentId");
            CreateIndex("dbo.AilmentSymptoms", "SymptomId");
            CreateIndex("dbo.Cards", "Id");
            CreateIndex("dbo.Issues", "CardId");
            CreateIndex("dbo.Issues", "AilmentId");
            CreateIndex("dbo.Treatments", "IssueId");
            CreateIndex("dbo.TreatmentSymptoms", "SymptomId");
            CreateIndex("dbo.TreatmentSymptoms", "TreatmentId");
            CreateIndex("dbo.Users", "Role_Id");
            CreateIndex("dbo.Permissions", "RoleId");
            AddForeignKey("dbo.Permissions", "RoleId", "dbo.Roles", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AilmentSymptoms", "AilmentId", "dbo.Ailments", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Issues", "AilmentId", "dbo.Ailments", "Id");
            AddForeignKey("dbo.AilmentSymptoms", "SymptomId", "dbo.Symptoms", "Id", cascadeDelete: true);
            AddForeignKey("dbo.TreatmentSymptoms", "SymptomId", "dbo.Symptoms", "Id", cascadeDelete: true);
            AddForeignKey("dbo.TreatmentSymptoms", "TreatmentId", "dbo.Treatments", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Treatments", "IssueId", "dbo.Issues", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Issues", "CardId", "dbo.Cards", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Cards", "Id", "dbo.Users", "Id");
            AddForeignKey("dbo.Users", "Role_Id", "dbo.Roles", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Users", "Role_Id", "dbo.Roles");
            DropForeignKey("dbo.Cards", "Id", "dbo.Users");
            DropForeignKey("dbo.Issues", "CardId", "dbo.Cards");
            DropForeignKey("dbo.Treatments", "IssueId", "dbo.Issues");
            DropForeignKey("dbo.TreatmentSymptoms", "TreatmentId", "dbo.Treatments");
            DropForeignKey("dbo.TreatmentSymptoms", "SymptomId", "dbo.Symptoms");
            DropForeignKey("dbo.AilmentSymptoms", "SymptomId", "dbo.Symptoms");
            DropForeignKey("dbo.Issues", "AilmentId", "dbo.Ailments");
            DropForeignKey("dbo.AilmentSymptoms", "AilmentId", "dbo.Ailments");
            DropForeignKey("dbo.Permissions", "RoleId", "dbo.Roles");
            DropIndex("dbo.Permissions", new[] { "RoleId" });
            DropIndex("dbo.Users", new[] { "Role_Id" });
            DropIndex("dbo.TreatmentSymptoms", new[] { "TreatmentId" });
            DropIndex("dbo.TreatmentSymptoms", new[] { "SymptomId" });
            DropIndex("dbo.Treatments", new[] { "IssueId" });
            DropIndex("dbo.Issues", new[] { "AilmentId" });
            DropIndex("dbo.Issues", new[] { "CardId" });
            DropIndex("dbo.Cards", new[] { "Id" });
            DropIndex("dbo.AilmentSymptoms", new[] { "SymptomId" });
            DropIndex("dbo.AilmentSymptoms", new[] { "AilmentId" });

            DropPrimaryKey("dbo.Permissions");
            DropPrimaryKey("dbo.Roles");
            DropPrimaryKey("dbo.Users");
            DropPrimaryKey("dbo.Cards");
            DropPrimaryKey("dbo.Issues");
            DropPrimaryKey("dbo.Treatments");
            DropPrimaryKey("dbo.TreatmentSymptoms");
            DropPrimaryKey("dbo.Symptoms");
            DropPrimaryKey("dbo.AilmentSymptoms");
            DropPrimaryKey("dbo.Ailments");

            DropColumn("dbo.Permissions", "RoleId");
            AddColumn("dbo.Permissions", "RoleId", c => c.Int());
            DropColumn("dbo.Permissions", "Id");
            AddColumn("dbo.Permissions", "Id", c => c.Int(nullable: false, identity: true));
            DropColumn("dbo.Roles", "Id");
            AddColumn("dbo.Roles", "Id", c => c.Int(nullable: false, identity: true));
            DropColumn("dbo.Users", "Role_Id");
            AddColumn("dbo.Users", "Role_Id", c => c.Int(nullable: false));
            DropColumn("dbo.Users", "Id");
            AddColumn("dbo.Users", "Id", c => c.Int(nullable: false, identity: true));
            DropColumn("dbo.Cards", "Id");
            AddColumn("dbo.Cards", "Id", c => c.Int(nullable: false));
            DropColumn("dbo.Issues", "CardId");
            AddColumn("dbo.Issues", "CardId", c => c.Int(nullable: false));
            DropColumn("dbo.Issues", "AilmentId");
            AddColumn("dbo.Issues", "AilmentId", c => c.Int());
            DropColumn("dbo.Issues", "Id");
            AddColumn("dbo.Issues", "Id", c => c.Int(nullable: false, identity: true));
            DropColumn("dbo.Treatments", "IssueId");
            AddColumn("dbo.Treatments", "IssueId", c => c.Int(nullable: false));
            DropColumn("dbo.Treatments", "Id");
            AddColumn("dbo.Treatments", "Id", c => c.Int(nullable: false, identity: true));
            DropColumn("dbo.TreatmentSymptoms", "TreatmentId");
            AddColumn("dbo.TreatmentSymptoms", "TreatmentId", c => c.Int(nullable: false));
            DropColumn("dbo.TreatmentSymptoms", "SymptomId");
            AddColumn("dbo.TreatmentSymptoms", "SymptomId", c => c.Int(nullable: false));
            DropColumn("dbo.TreatmentSymptoms", "Id");
            AddColumn("dbo.TreatmentSymptoms", "Id", c => c.Int(nullable: false, identity: true));
            DropColumn("dbo.Symptoms", "Id");
            AddColumn("dbo.Symptoms", "Id", c => c.Int(nullable: false, identity: true));
            DropColumn("dbo.AilmentSymptoms", "SymptomId");
            AddColumn("dbo.AilmentSymptoms", "SymptomId", c => c.Int(nullable: false));
            DropColumn("dbo.AilmentSymptoms", "AilmentId");
            AddColumn("dbo.AilmentSymptoms", "AilmentId", c => c.Int(nullable: false));
            DropColumn("dbo.AilmentSymptoms", "Id");
            AddColumn("dbo.AilmentSymptoms", "Id", c => c.Int(nullable: false, identity: true));
            DropColumn("dbo.Ailments", "Id");
            AddColumn("dbo.Ailments", "Id", c => c.Int(nullable: false, identity: true));

            DropColumn("dbo.Users", "CardId");
            DropColumn("dbo.Cards", "UserId");
            AddPrimaryKey("dbo.Permissions", "Id");
            AddPrimaryKey("dbo.Roles", "Id");
            AddPrimaryKey("dbo.Users", "Id");
            AddPrimaryKey("dbo.Cards", "Id");
            AddPrimaryKey("dbo.Issues", "Id");
            AddPrimaryKey("dbo.Treatments", "Id");
            AddPrimaryKey("dbo.TreatmentSymptoms", "Id");
            AddPrimaryKey("dbo.Symptoms", "Id");
            AddPrimaryKey("dbo.AilmentSymptoms", "Id");
            AddPrimaryKey("dbo.Ailments", "Id");

            RenameColumn(table: "dbo.Permissions", name: "RoleId", newName: "Role_Id");
            RenameColumn(table: "dbo.Issues", name: "CardId", newName: "Card_Id");
            RenameColumn(table: "dbo.Issues", name: "AilmentId", newName: "Ailment_Id");
            CreateIndex("dbo.Permissions", "Role_Id");
            CreateIndex("dbo.Users", "Role_Id");
            CreateIndex("dbo.Cards", "Id");
            CreateIndex("dbo.Issues", "Card_Id");
            CreateIndex("dbo.Issues", "Ailment_Id");
            CreateIndex("dbo.Treatments", "IssueId");
            CreateIndex("dbo.TreatmentSymptoms", "TreatmentId");
            CreateIndex("dbo.TreatmentSymptoms", "SymptomId");
            CreateIndex("dbo.AilmentSymptoms", "SymptomId");
            CreateIndex("dbo.AilmentSymptoms", "AilmentId");
            AddForeignKey("dbo.Users", "Role_Id", "dbo.Roles", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Cards", "Id", "dbo.Users", "Id");
            AddForeignKey("dbo.Issues", "Card_Id", "dbo.Cards", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Treatments", "IssueId", "dbo.Issues", "Id", cascadeDelete: true);
            AddForeignKey("dbo.TreatmentSymptoms", "TreatmentId", "dbo.Treatments", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AilmentSymptoms", "SymptomId", "dbo.Symptoms", "Id", cascadeDelete: true);
            AddForeignKey("dbo.TreatmentSymptoms", "SymptomId", "dbo.Symptoms", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Issues", "Ailment_Id", "dbo.Ailments", "Id");
            AddForeignKey("dbo.AilmentSymptoms", "AilmentId", "dbo.Ailments", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Permissions", "Role_Id", "dbo.Roles", "Id");
        }
    }
}
