﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DataAccessLayer.Entities;
using Hospital.Infrastructure.Query;
using Hospital.Infrastructure.Query.Predicates;
using Hospital.Infrastructure.Query.Predicates.Operators;
using Hospital.Infrastructure.UnitOfWork;
using NUnit.Framework;

namespace DataAccessLayer.Tests.Query
{

    [TestFixture]
    public class SymptomQueryTest
    {

        readonly IUnitOfWorkProvider unitOfWorkProvider = Initializer.Container.Resolve<IUnitOfWorkProvider>();

        readonly Guid symptom1Id = Guid.Parse("292a57dd-7682-411d-9614-7c9c243369ea");
        readonly Guid symptom2Id = Guid.Parse("c87532c9-6341-462a-a592-e3c9880a4999");
        readonly Guid symptom3Id = Guid.Parse("3b6ea64d-33b4-458c-82e8-db6cb736a0d1");
        readonly Guid symptom4Id = Guid.Parse("da2e8ef7-f6d0-49ac-a5f9-2f753d33d6df");
        readonly Guid symptom5Id = Guid.Parse("e1b9730a-b8c9-4784-b8c3-1dd5b09918eb");

        [Test]
        public async Task ExecuteAsync_SimpleWherePredicate_ReturnsCorrectQueryResult()
        {
            QueryResult<Symptom> actualQueryResult;
            var symptomQuery = Initializer.Container.Resolve<IQuery<Symptom>>();
            var expectedQueryResult = new QueryResult<Symptom>(new List<Symptom>
            {
                    new Symptom
                    {
                            Id = symptom4Id, Description = "Bolest břicha"
                    },
                    new Symptom
                    {
                            Id = symptom5Id, Description = "Bolest hlavy"
                    },
            }, 2);

            var predicate = new SimplePredicate(nameof(Symptom.Description), ValueComparingOperator.StringContains, "Bolest");
            using (unitOfWorkProvider.Create())
            {
                actualQueryResult = await symptomQuery.Where(predicate).ExecuteAsync();
            }

            Assert.AreEqual(actualQueryResult, expectedQueryResult);
        }

        [Test]
        public async Task ExecuteAsync_ComplexWherePredicate_ReturnsCorrectQueryResult()
        {
            QueryResult<Symptom> actualQueryResult;
            var symptomQuery = Initializer.Container.Resolve<IQuery<Symptom>>();
            var expectedQueryResult = new QueryResult<Symptom>(new List<Symptom>
            {
                    new Symptom
                    {
                            Id = symptom4Id, Description = "Bolest břicha"
                    },
                    new Symptom
                    {
                            Id = symptom5Id, Description = "Bolest hlavy"
                    }
            }, 2);

            var predicate = new CompositePredicate(new List<IPredicate>
            {
                    new SimplePredicate(nameof(Symptom.Description), ValueComparingOperator.StringContains, "Bolest"),
                    new CompositePredicate(new List<IPredicate>
                    {
                            new SimplePredicate(nameof(Symptom.Description), ValueComparingOperator.Equal, "Bolest břicha"),
                            new SimplePredicate(nameof(Symptom.Description), ValueComparingOperator.Equal, "Bolest hlavy")
                    }, LogicalOperator.OR)
            });
            using (unitOfWorkProvider.Create())
            {
                actualQueryResult = await symptomQuery.Where(predicate).ExecuteAsync();
            }

            Assert.AreEqual(actualQueryResult, expectedQueryResult);
        }

        [Test]
        public async Task ExecuteAsync_OrderAllSymptomsByDescription_ReturnsCorrectlyOrderedQueryResult()
        {
            QueryResult<Symptom> actualQueryResult;
            var categoryQuery = Initializer.Container.Resolve<IQuery<Symptom>>();
            var expectedQueryResult = new QueryResult<Symptom>(new List<Symptom>
            {
                    new Symptom
                    {
                            Id = symptom4Id,
                            Description = "Bolest břicha",
                    },
                    new Symptom
                    {
                            Id = symptom5Id,
                            Description = "Bolest hlavy",
                    },
                    new Symptom
                    {
                            Id = symptom3Id,
                            Description = "Horečka",
                    },
                    new Symptom
                    {
                            Id = symptom1Id,
                            Description = "Křeče",
                    },
                    new Symptom
                    {
                            Id = symptom2Id,
                            Description = "Ztráta vědomí",
                    },
            }, 5);

            using (unitOfWorkProvider.Create())
            {
                actualQueryResult = await categoryQuery.SortBy(nameof(Symptom.Description), true).ExecuteAsync();
            }

            Assert.AreEqual(actualQueryResult, expectedQueryResult);
        }

        [Test]
        public async Task ExecuteAsync_RetrieveSecondSymptomsPage_ReturnsCorrectPage()
        {
            const int pageSize = 3;
            const int requestedPage = 2;
            QueryResult<Symptom> actualQueryResult;
            var symptomQuery = Initializer.Container.Resolve<IQuery<Symptom>>();
            var expectedQueryResult = new QueryResult<Symptom>(new List<Symptom>
            {
                    new Symptom
                    {
                            Id = symptom4Id, Description = "Bolest břicha"
                    },
                    new Symptom
                    {
                            Id = symptom5Id, Description = "Bolest hlavy"
                    }
            }, 5, pageSize, requestedPage);

            using (unitOfWorkProvider.Create())
            {
                actualQueryResult = await symptomQuery.Page(requestedPage, pageSize).ExecuteAsync();
            }

            Assert.AreEqual(actualQueryResult, expectedQueryResult);
        }

    }

}
