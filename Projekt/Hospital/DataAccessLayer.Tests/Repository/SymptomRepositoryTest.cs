﻿using System;
using System.Threading.Tasks;
using DataAccessLayer.Entities;
using DataAccessLayer.Tests;
using Hospital.Infrastructure;
using Hospital.Infrastructure.UnitOfWork;
using NUnit.Framework;

namespace DAL.Tests.Repository
{

    [TestFixture]
    class SymptomRepositoryTest
    {

        #if REPO_TESTS

        //! Doesn't work even in Lab_Solution
        readonly IUnitOfWorkProvider unitOfWorkProvider = Initializer.Container.Resolve<IUnitOfWorkProvider>();
        readonly IRepository<Symptom> symptomRepository = Initializer.Container.Resolve<IRepository<Symptom>>();

        readonly Guid symptom1Id = Guid.Parse("292a57dd-7682-411d-9614-7c9c243369ea");
        readonly Guid symptom2Id = Guid.Parse("c87532c9-6341-462a-a592-e3c9880a4999");
        readonly Guid symptom3Id = Guid.Parse("3b6ea64d-33b4-458c-82e8-db6cb736a0d1");
        readonly Guid symptom4Id = Guid.Parse("da2e8ef7-f6d0-49ac-a5f9-2f753d33d6df");
        readonly Guid symptom5Id = Guid.Parse("e1b9730a-b8c9-4784-b8c3-1dd5b09918eb");

        [Test]
        public async Task GetSymptomAsync_AlreadyStoredInDBNoIncludes_ReturnsCorrectSymptom()
        {
            // Arrange
            Symptom symptom;

            // Act
            using (unitOfWorkProvider.Create())
            {
                symptom = await symptomRepository.GetAsync(symptom1Id);
            }

            // Assert
            Assert.AreEqual(symptom.Id, symptom1Id);
        }
        
        /*
        [Test]
        public async Task CreateCategoryAsync_CategoryIsNotPreviouslySeeded_CreatesNewCategory()
        {
            var windows10Mobile = new Category { Name = "Windows 10", ParentId = smartphonesCategoryId };

            using (var uow = unitOfWorkProvider.Create())
            {
                categoryRepository.Create(windows10Mobile);
                await uow.Commit();
                
            }
            Assert.IsTrue(!windows10Mobile.Id.Equals(Guid.Empty));
        }

        [Test]
        public async Task UpdateCategoryAsync_CategoryIsPreviouslySeeded_UpdatesCategory()
        {
            Category updatedAndroidCategory;
            var newAndroidCategory = new Category { Id = androidCategoryId, Name = "Updated Name", ParentId = null };

            using (var uow = unitOfWorkProvider.Create())
            {
                categoryRepository.Update(newAndroidCategory);
                await uow.Commit();
                updatedAndroidCategory = await categoryRepository.GetAsync(androidCategoryId);
            }

            Assert.IsTrue(newAndroidCategory.Name.Equals(updatedAndroidCategory.Name) && newAndroidCategory.ParentId.Equals(null));
        }

        [Test]
        public async Task DeleteCategoryAsync_CategoryIsPreviouslySeeded_DeletesCategory()
        {
            Category deletedAndroidCategory;

            using (var uow = unitOfWorkProvider.Create())
            {
                categoryRepository.Delete(androidCategoryId);
                await uow.Commit();
                deletedAndroidCategory = await categoryRepository.GetAsync(androidCategoryId);
            }

            Assert.AreEqual(deletedAndroidCategory, null);
        }
        */
    #endif

    }

}
