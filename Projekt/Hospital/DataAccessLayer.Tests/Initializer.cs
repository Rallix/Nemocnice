﻿using System.Data.Entity;
using Castle.Windsor;
using DataAccessLayer.Tests.Config;
using NUnit.Framework;

namespace DataAccessLayer.Tests
{
    [SetUpFixture]
    public class Initializer
    {
        internal static readonly IWindsorContainer Container = new WindsorContainer();

        /// <summary>
        /// Initializes all Business Layer tests
        /// </summary>
        [OneTimeSetUp]
        public void InitializeBusinessLayerTests()
        {
            Effort.Provider.EffortProviderConfiguration.RegisterProvider();
            Database.SetInitializer(new DropCreateDatabaseAlways<HospitalDbContext>());          
            Container.Install(new EntityFrameworkTestInstaller());
        }
    }
}
