﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using DataAccessLayer.Entities;
using DataAccessLayer.Enums;
using Hospital.Infrastructure;
using Hospital.Infrastructure.EntityFramework;
using Hospital.Infrastructure.EntityFramework.UnitOfWork;
using Hospital.Infrastructure.Query;
using Hospital.Infrastructure.UnitOfWork;

namespace DataAccessLayer.Tests.Config
{

    public class EntityFrameworkTestInstaller : IWindsorInstaller
    {

        const string TestDbConnectionString = "InMemoryTestDBDemoHospital";

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                               Component.For<Func<DbContext>>()
                                        .Instance(InitializeDatabase)
                                        .LifestyleTransient(),
                               Component.For<IUnitOfWorkProvider>()
                                        .ImplementedBy<EntityFrameworkUnitOfWorkProvider>()
                                        .LifestyleSingleton(),
                               Component.For(typeof(IRepository<>))
                                        .ImplementedBy(typeof(EntityFrameworkRepository<>))
                                        .LifestyleTransient(),
                               Component.For(typeof(IQuery<>))
                                        .ImplementedBy(typeof(EntityFrameworkQuery<>))
                                        .LifestyleTransient()
                              );
        }

        static DbContext InitializeDatabase()
        {
            var context = new HospitalDbContext(Effort.DbConnectionFactory.CreatePersistent(TestDbConnectionString));
            context.Issues.RemoveRange(context.Issues);
            context.Ailments.RemoveRange(context.Ailments);
            context.Symptoms.RemoveRange(context.Symptoms);
            context.SaveChanges();

            /** Random unused GUIDs **/
            // b9f64530-f039-4a89-bfaf-0159482d0e46
            // e8f3446c-3037-4d85-947d-c55621c9023f
            // 4b2df8ea-6c9c-42b4-b0f5-db25bf5329c5            
            // 2e8d7c7f-60ed-4f14-b612-aae0dae04907
            // 723f59b6-08d1-421c-b5a5-6543d8538a84
            // 5df9bf2a-b04a-46e4-8a22-10b1c44b6714
            // 2c217466-9791-4f83-b9c1-f91e480b9adf
            // 16850ce5-6a31-4fd1-9b8e-1e326f3c87ee
            // d57f7d24-4a4a-4dc4-9bfd-9c0c348a7132
            // 3ccc8478-afcb-4618-a257-5a86080d4f87
            // b7c20cd9-17ab-43ca-9105-73f3bf52887e
            // c62bef61-4143-4476-9527-dab551655f08
            // cf68b7bc-4c60-42a1-8a6f-ca33f21edd23
            // 630e1818-d852-4305-8082-793bb003b69f
            // 0b399386-dcd6-405f-9d14-6e295486508e
            // ce75b4f4-99a6-4f16-a198-006c7ece4461
            // c41bedc0-01a4-4593-80a7-682d73a5d53b
            // d6ac7859-bde3-490d-a7f2-c7f19a035c66
            // 4d7580e9-5aac-462b-9f99-222df511ae16
            // c0fc7d2f-51e7-488f-9a50-37fbbc2f8d6a
            // 2a438451-e395-45be-85dc-2d14a16b6c3e
            // e30998fe-0ec0-4e46-9135-0175ddf26f9c
            // f57fc396-27be-473b-8783-b4d71071e6be
            // 38f56078-ad8f-47c3-93e5-d64364100de5
            // b322bee0-554b-485b-a19e-dd1e6cd3e4de
            // 05a5f7ae-717a-4c5c-8a61-5495ac3ae945
            // 2bcdbf5f-fd1b-423a-9ce2-c5f370301276
            // b492fdeb-ec97-41e7-81df-636a22c79aaa
            // 653bb7f6-0ba1-4fa7-aa47-2e9e0c3efc3a
            // a83c2bad-04a9-4059-9eb7-d3a83f3266f8
            // 77a51b2a-8254-476a-9b89-7b8b8c191100
            // cf633be8-a130-405e-b802-4a3de5d0e479
            // 207c2aef-dbc1-484e-b0d1-da25ee1f7a56

            /** Random RČ **/
            // 780607/4210
            // 840408/5019
            // 760621/3912
            // 760712/3920
            // 580507/1514
            // 850726/5118
            
            #region Demo

            var director = new Role
            {                    
                    Id = Guid.Parse("b9f64530-f039-4a89-bfaf-0159482d0e46"),
                    RoleType = RoleTypes.Director
            };
            var permission = new Permission
            {
                    Id = Guid.Parse("ab9bf228-1def-43f7-964b-2f0b67e01a49"),
                    Action = "Everything",
                    Role = director
            };
            director.Permissions = new List<Permission>(new[] {permission});
            permission.RoleId = director.Id;

            context.Roles.Add(director);
            context.Permissions.Add(permission);

            var directorUser = new User
            {
                    Role = director,
                    Username = "director",
                    Password = "heslo123",
                    Alive = true,
                    BirthDate = new DateTime(1965, 4, 8),
                    BirthNumber = "650408/2112",                    
            };
            var card = new Card
            {
                    Id = Guid.Parse("fa4d0803-f92b-48b2-9977-4e010bc54d1a"),
                    User = directorUser,
                    UserId = directorUser.Id,
                    Issues = new List<Issue>()
            };
            directorUser.Card = card;
            directorUser.CardId = card.Id;

            context.Cards.Add(card);
            context.Users.Add(directorUser);

            /*
            var ailment = new Ailment
            {
                    Name = "something"
            };
            var symptom_1 = new Symptom
            {
                    Description = "back pain"
            };
            var symptom_2 = new Symptom
            {
                    Description = "high temperature"
            };
            var issue = new Issue
            {
                    Card = directorUser.Card,
                    Since = DateTime.Now,
                    Until = DateTime.Now,
                    Solved = false
            };
            var treatment = new Treatment
            {
                    Issue = issue,
                    Date = DateTime.Now,
                    Message = "the director seems to be ill"
            };
            var treatmentSymptom1 = new TreatmentSymptom
            {
                    Symptom = symptom_1,
                    Treatment = treatment
            };

            var treatmentSymptom2 = new TreatmentSymptom
            {
                    Symptom = symptom_2,
                    Treatment = treatment
            };


            var ailmentSymptom1 = new AilmentSymptom
            {
                    Symptom = symptom_1,
                    Ailment = ailment
            };

            //! Add to database
            context.Users.Add(directorUser);
            context.Issues.Add(issue);
            context.Treatments.Add(treatment);
            context.Ailments.Add(ailment);
            context.Cards.Add(directorUser.Card);
            context.AilmentSymptoms.Add(ailmentSymptom1);
            context.TreatmentSymptoms.Add(treatmentSymptom1);
            context.TreatmentSymptoms.Add(treatmentSymptom2);
            context.Symptoms.AddRange(new[] {symptom_1, symptom_2});
            */
            #endregion
            
            #region Symptoms

            var symptom1 = new Symptom
            {
                    Id = Guid.Parse("292a57dd-7682-411d-9614-7c9c243369ea"),
                    Description = "Křeče",
                    SymptomAilments = new List<AilmentSymptom>()
            };

            var symptom2 = new Symptom
            {
                    Id = Guid.Parse("c87532c9-6341-462a-a592-e3c9880a4999"),
                    Description = "Ztráta vědomí",
                    SymptomAilments = new List<AilmentSymptom>()
            };
            var symptom3 = new Symptom
            {
                    Id = Guid.Parse("3b6ea64d-33b4-458c-82e8-db6cb736a0d1"),
                    Description = "Horečka",
                    SymptomAilments = new List<AilmentSymptom>()
            };
            
            var symptom4 = new Symptom
            {
                    Id = Guid.Parse("da2e8ef7-f6d0-49ac-a5f9-2f753d33d6df"),
                    Description = "Bolest břicha",
                    SymptomAilments = new List<AilmentSymptom>()
            };

            var symptom5 = new Symptom
            {
                    Id = Guid.Parse("e1b9730a-b8c9-4784-b8c3-1dd5b09918eb"),
                    Description = "Bolest hlavy",
                    SymptomAilments = new List<AilmentSymptom>()
            };

            context.Symptoms.AddRange(new[] {symptom1, symptom2, symptom3, symptom4, symptom5});
            #endregion

            #region Ailments

            var ailment1 = new Ailment
            {
                    Id = Guid.Parse("aa01dc64-5c07-40fe-a916-175165b9b90f"),
                    Name = "Padoucnice",
                    SymptomAilments = new List<AilmentSymptom>()
            };
            ailment1.AddSymptoms(symptom1, symptom2);

            
            var ailment2 = new Ailment
            {
                    Id = Guid.Parse("aa05dc64-5c07-40fe-a916-175165b9b90f"),
                    Name = "Záškrt",
                    SymptomAilments = new List<AilmentSymptom>()
            };
            ailment2.AddSymptoms(symptom3);

            var ailment3 = new Ailment
            {
                    Id = Guid.Parse("aa04dc64-5c07-40fe-a916-175165b9b90f"),
                    Name = "Zápal švindlu",
                    SymptomAilments = new List<AilmentSymptom>()
            };

            context.Ailments.AddRange(new []{ailment1, ailment2, ailment3});

            #endregion
            /**/
                        
            context.SaveChanges(); // TODO: All required fields must be filled
            return context;
        }
        
    }
    
}
