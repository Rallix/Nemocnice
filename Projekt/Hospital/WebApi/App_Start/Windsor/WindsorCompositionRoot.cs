﻿using System;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Dispatcher;
using Castle.Windsor;

namespace WebApi.Windsor
{
    public class WindsorCompositionRoot : IHttpControllerActivator
    {

        readonly IWindsorContainer container;

        public WindsorCompositionRoot(IWindsorContainer container)
        {
            this.container = container;
        }

        public IHttpController Create(HttpRequestMessage request, HttpControllerDescriptor controllerDescriptor, Type controllerType)
        {
            var controller = (IHttpController) container.Resolve(controllerType);
            request.RegisterForDispose(new Release(() => container.Release(controller)));
            return controller;
        }

        class Release : IDisposable
        {

            readonly Action release;

            public Release(Action release)
            {
                this.release = release;
            }

            public void Dispose()
            {
                release();
            }

        }

    }

}
