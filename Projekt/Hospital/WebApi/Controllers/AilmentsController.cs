﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using BusinessLayer.DataTransferObjects;
using BusinessLayer.DataTransferObjects.Filters;
using BusinessLayer.Facades;
using WebApi.Models;

namespace WebApi.Controllers
{
    public class AilmentsController : ApiController
    {
        
        public AilmentFacade AilmentFacade { get; set; }

        /// <summary> Query ailments according to given query parameters. </summary>
        /// <example>
        /// Example URL call: http://localhost:56118/api/Ailments/Query?sort=name&asc=true&name=Záškrt
        /// </example>
        /// <param name="sort">Name of the ailment attribute to sort according to.</param>
        /// <param name="asc">Sort ailment collection in an ascending manner.</param>
        /// <param name="name">Ailment name (can be only partial: "Žloutenka [typu A/B/C]")</param>      
        [HttpGet, Route("api/Ailments/Query")]
        public async Task<IEnumerable<AilmentDto>> Query(string sort = null, bool asc = true, 
                                                         string name = null)
        {
            //! Note: Only "name" –> sort is useless
            var filter = new AilmentFilterDto
            {
                    SortCriteria = sort,
                    SortAscending = asc,                    
                    Name = name                    
            };
            var ailments = (await AilmentFacade.GetAilmentsAsync(filter)).Items;
            foreach (var ailment in ailments)
            {
                ailment.Id = Guid.Empty; // security
            }
            return ailments;
        }

        /// <summary> Gets ailment info for an ailment with a given name. </summary>
        /// <example>
        /// Example URL call: http://localhost:56118/api/Ailments/Get?name=Záškrt
        /// </example>        
        /// <param name="name">Ailment name.</param>
        /// <returns>Complete ailment info.</returns>
        public async Task<AilmentDto> Get(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
            var ailment = await AilmentFacade.GetAilmentAsync(name);         
            if (ailment == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            ailment.Id = Guid.Empty;
            return ailment;
        }

        /// <summary>
        /// Creates an ailment.
        /// </summary>
        /// <param name="model">Created ailment details.</param>
        /// <returns>Message describing the action result.</returns>
        public async Task<string> Post([FromBody]AilmentCreateModel model)
        {
            if (!ModelState.IsValid)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
            Guid ailmentId = await AilmentFacade.CreateAilmentWithSymptoms(model.Ailment, model.Symptoms);
            if (ailmentId.Equals(Guid.Empty))
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
            return $"Created an ailment with id {ailmentId} and following symptoms: {string.Join(", ", model.Symptoms)}.";
        }

        /// <summary> Updates an ailment with a given id. </summary>
        /// <param name="id">Id of the ailment to update.</param>
        /// <param name="ailment">Ailment to update</param>
        /// /// <exception cref="DbEntityValidationException">Throws a validation exception
        /// when the PUT request is incomplete.</exception>
        /// <returns>Message describing the action result.</returns>
        public async Task<string> Put(Guid id, [FromBody]AilmentDto ailment)
        {
            if (!ModelState.IsValid)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
            bool success = await AilmentFacade.EditAilmentAsync(ailment);
            if (!success)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            return $"Updated the ailment with id: {id}";
        }

        /// <summary> Deletes ailment with a given guid. </summary>        
        /// <param name="id">Id of the ailment to delete.</param>
        /// <returns>Message describing the action result.</returns>
        public async Task<string> Delete(Guid id)
        {
            bool success = await AilmentFacade.DeleteAilmentAsync(id);
            if (!success)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            return $"Deleted the ailment with id: {id}";
        }
    }
}