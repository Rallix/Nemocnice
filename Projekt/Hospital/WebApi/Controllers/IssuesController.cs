﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using BusinessLayer.DataTransferObjects;
using BusinessLayer.DataTransferObjects.Filters;
using BusinessLayer.Facades;
using WebApi.Models;

namespace WebApi.Controllers
{
    public class IssuesController : ApiController
    {
        
        public IssueFacade IssueFacade { get; set; }

        /// <summary> Query issues according to given query parameters. </summary>
        /// <example>
        /// Example URL call: http://localhost:56118/api/Issues/Query?sort=name&asc=true&name=Záškrt
        /// </example>
        /// <param name="sort">Name of the issue attribute to sort according to.</param>
        /// <param name="asc">Sort issue collection in an ascending manner.</param>
        /// <param name="solved">'true' or 'false' to show only solved/unsolved issues.</param>
        /// <param name="since">The date in a 'yyyyMMdd' format to start searching from.</param>
        /// <param name="until">The date in a 'yyyyMMdd' format to search up to.</param> 
        [HttpGet, Route("api/Issues/Query")]
        public async Task<IEnumerable<IssueDto>> Query(string sort = null, bool asc = true, 
                                                         bool solved = false, 
                                                         string since = "00010101",
                                                         string until = "99991231")
        {            
            var filter = new IssueFilterDto
            {
                    SortCriteria = sort,
                    SortAscending = asc,   
                    Solved = solved,
                    Since = DateTime.ParseExact(since, "yyyyMMdd", CultureInfo.InvariantCulture),
                    Until = DateTime.ParseExact(until, "yyyyMMdd", CultureInfo.InvariantCulture),
            };
            var issues = (await IssueFacade.GetAllIssuesAsync(filter)).Items;
            foreach (var issue in issues)
            {
                issue.Id = Guid.Empty; // security
            }
            return issues;
        }

        /// <summary> Gets the issue with a given <see cref="Guid"/>. </summary>
        /// <example>
        /// Example URL call: http://localhost:56118/api/Issues/Get?guid=9c445694-c963-592e-ad23-426c53494519
        /// </example>        
        /// <param name="guid">The <see cref="Guid"/> of an issue.</param>
        /// <returns>An issue with the given <see cref="Guid"/>.</returns>        
        public async Task<IssueDto> Get(string guid)
        {
            if (string.IsNullOrWhiteSpace(guid))
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
            var issue = await IssueFacade.GetIssueAsync(Guid.Parse(guid));         
            if (issue == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            issue.Id = Guid.Empty;
            return issue;
        }

        /// <summary> Gets all issues of a card with a given <see cref="Guid"/>. </summary>
        /// <example>
        /// Example URL call: http://localhost:56118/api/Issues/Card?guid=9c445694-c963-592e-ad23-426c53494519
        /// </example>        
        /// <param name="guid">The <see cref="Guid"/> of a card.</param>
        /// <returns>All card's issues.</returns>
        [HttpGet, Route("api/Issues/Card")]
        public async Task<IEnumerable<IssueDto>> GetFromCard(string guid)
        {
            if (string.IsNullOrWhiteSpace(guid))
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
            var issues = (await IssueFacade.GetIssuesOfCardAsync(Guid.Parse(guid))).Items.ToList();         
            if (!issues.Any())
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            foreach (IssueDto issue in issues)
            {
                issue.Id = Guid.Empty; // security
            }
            return issues;
        }

        /// <summary> Gets all treatments of an issue with a given <see cref="Guid"/>. </summary>
        /// <example>
        /// Example URL call: http://localhost:56118/api/Issues/Treatments?guid=9c445694-c963-592e-ad23-426c53494519
        /// </example>        
        /// <param name="guid">The <see cref="Guid"/> of an issue.</param>
        /// <returns>All issue's treatments.</returns>
        [HttpGet, Route("api/Issues/Treatments")]
        public async Task<IEnumerable<TreatmentDto>> GetTreatments(string guid)
        {
            if (string.IsNullOrWhiteSpace(guid))
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
            var treatments = (await IssueFacade.GetTreatmentsOfIssue(Guid.Parse(guid))).ToList();         
            if (!treatments.Any())
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            foreach (var treatment in treatments)
            {
                treatment.Id = Guid.Empty; // security
            }
            return treatments;
        }
        
        // /// <summary>
        // /// Creates an issue.
        // /// </summary>
        // /// <param name="model">Created issue details.</param>
        // /// <returns>Message describing the action result.</returns>
        public async Task<string> Post([FromBody]IssueCreateModel model)
        {
            if (!ModelState.IsValid)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
            Guid issueId = await IssueFacade.CreateIssue(model.Issue);
            if (issueId.Equals(Guid.Empty))
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
            return $"Created an issue with id: {issueId}.";
        }


        /// <summary> Updates an issue with a given id. </summary>
        /// <param name="id">Id of the issue to update.</param>
        /// <param name="issue">Issue to update</param>
        /// <returns>Message describing the action result.</returns>
        public async Task<string> Put(Guid id, [FromBody]IssueDto issue)
        {
            if (!ModelState.IsValid)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
            bool success = await IssueFacade.EditIssueAsync(issue);
            if (!success)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            return $"Updated the issue with id: {id}";
        }

        /// <summary> Deletes the issue with a given guid. </summary>        
        /// <param name="id">Id of the issue to delete.</param>
        /// <returns>Message describing the action result.</returns>
        public async Task<string> Delete(Guid id)
        {
            bool success = await IssueFacade.DeleteIssueAsync(id);
            if (!success)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            return $"Deleted the issue with id: {id}";
        }
    }
}