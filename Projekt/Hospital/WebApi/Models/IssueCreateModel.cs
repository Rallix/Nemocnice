﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessLayer.DataTransferObjects;

namespace WebApi.Models {
    public class IssueCreateModel {

        /// <summary> Ailment to create. </summary>
        public IssueDto Issue { get; set; }

    }
}