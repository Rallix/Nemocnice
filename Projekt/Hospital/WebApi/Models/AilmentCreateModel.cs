﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using BusinessLayer.DataTransferObjects;

namespace WebApi.Models {
    public class AilmentCreateModel {
        
        /// <summary> Ailment to create. </summary>
        public AilmentDto Ailment { get; set; }
        
        /// <summary> Symptoms of the ailment. </summary>        
        public IEnumerable<string> Symptoms { get; set; } = new string[] {};
    }
}