﻿using System;
using System.Linq;
using DataAccessLayer;
using DataAccessLayer.Entities;

namespace Demo
{

    class Program
    {

        static void Main(string[] args)
        {
            using (var db = new HospitalDbContext())
            {
                Issue firstIssue = db.Cards.Select(c => c.Issues).First().First();
                Treatment firstTreatment = db.Treatments.First(t => t.IssueId == firstIssue.Id);
                Console.WriteLine(firstTreatment.Message);
            }
            Console.Read();
        }

    }

}
