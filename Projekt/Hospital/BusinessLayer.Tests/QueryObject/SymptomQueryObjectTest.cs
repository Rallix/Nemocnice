﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BusinessLayer.DataTransferObjects;
using BusinessLayer.DataTransferObjects.Filters;
using BusinessLayer.QueryObjects;
using DataAccessLayer.Entities;
using Hospital.BusinessLayer.Tests.QueryObjectsTests.Common;
using Hospital.Infrastructure.Query.Predicates;
using Hospital.Infrastructure.Query.Predicates.Operators;
using NUnit.Framework;

namespace BusinessLayer.Tests.QueryObject
{
    [TestFixture]
    class SymptomQueryObjectTest
    {

        [Test]
        public async Task ApplyWhereClause_SimpleFilterDescription_ReturnsCorrectSimplePredicate()
        {
            const string desiredName1 = "Bolest hlavy";
            const string desiredName2 = "Bolest břicha";
            var mockManager = new QueryMockManager();
            var expectedPredicate = new CompositePredicate(new List<IPredicate>
            {
                    new SimplePredicate(nameof(Symptom.Description), ValueComparingOperator.Equal, desiredName1),
                    new SimplePredicate(nameof(Symptom.Description), ValueComparingOperator.Equal, desiredName2)
            }, LogicalOperator.OR);

            // var expectedPredicate = new SimplePredicate(nameof(Symptom.Description), ValueComparingOperator.Equal, desiredName1);

            var mapperMock = mockManager.ConfigureMapperMock<Symptom, SymptomDto, SymptomFilterDto>();
            var queryMock = mockManager.ConfigureQueryMock<Symptom>();
            var symptomQueryObject = new SymptomQueryObject(mapperMock.Object, queryMock.Object);
            
            // new SymptomFilterDto {Description = desiredName1}
            var unused = await symptomQueryObject.ExecuteQuery(new SymptomFilterDto {AnyOfDescriptions = new []{desiredName1, desiredName2}});

            Assert.AreEqual(mockManager.CapturedPredicate, expectedPredicate);
        }
    }
}
