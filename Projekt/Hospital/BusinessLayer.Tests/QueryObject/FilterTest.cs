﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BusinessLayer.DataTransferObjects;
using BusinessLayer.DataTransferObjects.Filters;
using BusinessLayer.QueryObjects;
using DataAccessLayer.Entities;
using Hospital.BusinessLayer.Tests.QueryObjectsTests.Common;
using Hospital.Infrastructure.Query.Predicates;
using Hospital.Infrastructure.Query.Predicates.Operators;
using NUnit.Framework;

namespace Hospital.BusinessLayer.Tests
{
    [TestFixture]
    public class FilterTest {
        [Test]
        public async Task ApplyWhereClause_SimpleFilterWithMultipleIds_ReturnsCorrectSimplePredicate() {
            Guid desiredId1 = Guid.NewGuid();
            Guid desiredId2 = Guid.NewGuid();

            var mockManager = new QueryMockManager();
            var expectedPredicate = new CompositePredicate(
            new List<IPredicate>
                {
                    new SimplePredicate(nameof(Ailment.Id), ValueComparingOperator.Equal, desiredId1),
                    new SimplePredicate(nameof(Ailment.Id), ValueComparingOperator.Equal, desiredId2)
                }, LogicalOperator.OR);

            var mapperMock = mockManager.ConfigureMapperMock<Ailment, AilmentDto, AilmentFilterDto>();

            var queryMock = mockManager.ConfigureQueryMock<Ailment>();
            var categoryQueryObject = new AilmentQueryObject(mapperMock.Object, queryMock.Object);

            var unused = await categoryQueryObject.ExecuteQuery(new AilmentFilterDto { AnyId = new[] { desiredId1, desiredId2 } });

            Assert.AreEqual(mockManager.CapturedPredicate, expectedPredicate);
        }

        [Test]
        public async Task ApplyWhereClause_CompositeFilterWithMultipleIdsAndName_ReturnsCorrectSimplePredicate()
        {
            Guid desiredId1 = Guid.NewGuid();
            Guid desiredId2 = Guid.NewGuid();
            string desiredName = "cojavim";

            var mockManager = new QueryMockManager();
            var expectedPredicate = new CompositePredicate(
                new List<IPredicate>{
                    new CompositePredicate(
                        new List<IPredicate>
                            {
                                new SimplePredicate(nameof(Ailment.Id), ValueComparingOperator.Equal, desiredId1),
                                new SimplePredicate(nameof(Ailment.Id), ValueComparingOperator.Equal, desiredId2)
                            }, LogicalOperator.OR),
                    new SimplePredicate(nameof(Ailment.Name), ValueComparingOperator.StringContains, desiredName)
                }, LogicalOperator.AND);


            var mapperMock = mockManager.ConfigureMapperMock<Ailment, AilmentDto, AilmentFilterDto>();

            var queryMock = mockManager.ConfigureQueryMock<Ailment>();
            var categoryQueryObject = new AilmentQueryObject(mapperMock.Object, queryMock.Object);

            var unused = await categoryQueryObject.ExecuteQuery(new AilmentFilterDto { AnyId = new[] { desiredId1, desiredId2 }, Name = desiredName });

            Assert.AreEqual(mockManager.CapturedPredicate, expectedPredicate);
        }

    }
}
