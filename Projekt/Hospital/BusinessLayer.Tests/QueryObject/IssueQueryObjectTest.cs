﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BusinessLayer.DataTransferObjects;
using BusinessLayer.DataTransferObjects.Filters;
using BusinessLayer.QueryObjects;
using DataAccessLayer.Entities;
using Hospital.BusinessLayer.Tests.QueryObjectsTests.Common;
using Hospital.Infrastructure.Query.Predicates;
using Hospital.Infrastructure.Query.Predicates.Operators;
using NUnit.Framework;

namespace BusinessLayer.Tests.QueryObject {

    [TestFixture]
    class IssueQueryObjectTest
    {

        [Test]
        public async Task ApplyWhereClause_SimpleFilterDescription_ReturnsCorrectSimplePredicate()
        {            
            const bool desiredStatus = false;
            DateTime sinceDate = default(DateTime);
            DateTime untilDate = default(DateTime);

            var mockManager = new QueryMockManager();            
            var expectedPredicate = new CompositePredicate(new List<IPredicate>
            {
                    new SimplePredicate(nameof(Issue.Solved), ValueComparingOperator.Equal, desiredStatus),
                    new SimplePredicate(nameof(Issue.Since), ValueComparingOperator.Equal, sinceDate),
                    new SimplePredicate(nameof(Issue.Until), ValueComparingOperator.Equal, untilDate),
            }, LogicalOperator.AND);
            var mapperMock = mockManager.ConfigureMapperMock<Issue, IssueDto, IssueFilterDto>();
            var queryMock = mockManager.ConfigureQueryMock<Issue>();
            var issueQueryObject = new IssueQueryObject(mapperMock.Object, queryMock.Object);
            
            var unused = await issueQueryObject.ExecuteQuery(new IssueFilterDto { Solved = desiredStatus });

            Assert.AreEqual(mockManager.CapturedPredicate, expectedPredicate);
        }
    }
}
