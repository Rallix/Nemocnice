﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BusinessLayer.DataTransferObjects;
using BusinessLayer.DataTransferObjects.Filters;
using BusinessLayer.QueryObjects;
using DataAccessLayer.Entities;
using Hospital.BusinessLayer.Tests.QueryObjectsTests.Common;
using Hospital.Infrastructure.Query.Predicates;
using Hospital.Infrastructure.Query.Predicates.Operators;
using NUnit.Framework;

namespace BusinessLayer.Tests.QueryObject {

    [TestFixture]
    class AilmentQueryObjectTest
    {

        [Test]
        public async Task ApplyWhereClause_SimpleFilterDescription_ReturnsCorrectSimplePredicate()
        {            
            const string desiredPart2 = "itis";
            var mockManager = new QueryMockManager();
            var expectedPredicate = new SimplePredicate(nameof(Ailment.Name), ValueComparingOperator.StringContains, "Arthritis");

            var mapperMock = mockManager.ConfigureMapperMock<Ailment, AilmentDto, AilmentFilterDto>();
            var queryMock = mockManager.ConfigureQueryMock<Ailment>();
            var ailmentQueryObject = new AilmentQueryObject(mapperMock.Object, queryMock.Object);
            
            var unused = await ailmentQueryObject.ExecuteQuery(new AilmentFilterDto {Name = $"Arthr{desiredPart2}"});

            Assert.AreEqual(mockManager.CapturedPredicate, expectedPredicate);
        }
    }
}
