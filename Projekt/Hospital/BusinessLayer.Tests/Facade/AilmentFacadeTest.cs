﻿using BusinessLayer.DataTransferObjects;
using BusinessLayer.DataTransferObjects.Filters;
using BusinessLayer.Facades;
using BusinessLayer.Services.Ailments;
using DataAccessLayer.Entities;
using Hospital.BusinessLayer.Tests.FacadesTests.Common;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BusinessLayer.Tests.Facade
{
    [TestFixture]
    public class AilmentFacadeTest
    {

        [Test]
        public async Task GetAilmentsWhichHaveSymptomsAsync()
        {
            var expectedQueryResult = new QueryResultDto<AilmentDto, AilmentFilterDto>
            {
                Filter = new AilmentFilterDto(),
                Items = new List<AilmentDto> { new AilmentDto { Id = Guid.NewGuid() }, new AilmentDto { Id = Guid.NewGuid() } },
                PageSize = 10,
                RequestedPageNumber = null

            };
            var mockManager = new FacadeMockManager();
            var ailmentFacade = CreateAilmentFacade(mockManager, expectedQueryResult);
            var actualQueryResult = await ailmentFacade.GetAilmentsWhichHaveSymptomsAsync(new string[] { });
            Assert.AreEqual(actualQueryResult, expectedQueryResult.Items);
        }

        [Test]
        public async Task GetAllAilmentsAsync_TwoExistingAilments_ReturnsCorrectQueryResult()
        {
            var expectedQueryResult = new QueryResultDto<AilmentDto, AilmentFilterDto>
            {
                Filter = new AilmentFilterDto(),
                Items = new List<AilmentDto> { new AilmentDto { Id = Guid.NewGuid() }, new AilmentDto { Id = Guid.NewGuid() } },
                PageSize = 10,
                RequestedPageNumber = null

            };
            var mockManager = new FacadeMockManager();
            var ailmentFacade = CreateAilmentFacade(mockManager, expectedQueryResult);
            var actualQueryResult = await ailmentFacade.GetAilmentsAsync(new AilmentFilterDto());

            Assert.AreEqual(actualQueryResult, expectedQueryResult);
        }


        private static AilmentFacade CreateAilmentFacade(FacadeMockManager mockManager, QueryResultDto<AilmentDto, AilmentFilterDto> expectedQueryResult)
        {
            
            var uowMock = FacadeMockManager.ConfigureUowMock();
            var mapper = FacadeMockManager.ConfigureRealMapper();
            var repositoryMock = mockManager.ConfigureCreateRepositoryMock<Ailment>(nameof(Ailment.Id));
            var symptomRepositoryMock = mockManager.ConfigureCreateRepositoryMock<Symptom>(nameof(Symptom.Id));
            var ailmentSymptomRepositoryMock = mockManager.ConfigureCreateRepositoryMock<AilmentSymptom>(nameof(AilmentSymptom.Id));
            var ailmentQueryMock = mockManager.ConfigureQueryObjectMock<AilmentDto, Ailment, AilmentFilterDto>(expectedQueryResult);
            var symptomQueryMock = mockManager.ConfigureQueryObjectMock<SymptomDto, Symptom, SymptomFilterDto>(new QueryResultDto<SymptomDto, SymptomFilterDto> { Items = new List<SymptomDto>(), Filter = new SymptomFilterDto() });
            var ailmentSymptomQueryMock = mockManager.ConfigureQueryObjectMock<AilmentSymptomDto, AilmentSymptom, AilmentSymptomFilterDto>(new QueryResultDto<AilmentSymptomDto, AilmentSymptomFilterDto> {Items=new List<AilmentSymptomDto>(), Filter=new AilmentSymptomFilterDto() });
            var ailmentService = new AilmentService(mapper, repositoryMock.Object, ailmentQueryMock.Object, ailmentSymptomRepositoryMock.Object , ailmentSymptomQueryMock.Object, symptomRepositoryMock.Object, symptomQueryMock.Object);
            var ailmentFacade = new AilmentFacade(uowMock.Object, ailmentService);
            return ailmentFacade;
        }
    }
}
