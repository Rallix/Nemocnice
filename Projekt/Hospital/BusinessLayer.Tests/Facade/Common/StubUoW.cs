﻿using System.Threading.Tasks;
using Hospital.Infrastructure.UnitOfWork;

namespace Hospital.BusinessLayer.Tests.FacadesTests.Common
{

    class StubUow : UnitOfWorkBase
    {

        protected override Task CommitCore()
        {
            return Task.Delay(15);
        }

        public override void Dispose() { }

    }

}
