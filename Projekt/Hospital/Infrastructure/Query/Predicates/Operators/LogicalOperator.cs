﻿namespace Hospital.Infrastructure.Query.Predicates.Operators
{
    public enum LogicalOperator
    {
        AND, OR
    }
}
