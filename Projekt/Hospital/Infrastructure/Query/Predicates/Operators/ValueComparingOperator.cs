﻿using System.ComponentModel;

namespace Hospital.Infrastructure.Query.Predicates.Operators
{
    public enum ValueComparingOperator
    {
        [Description("")]
        None,
        [Description(">")]
        GreaterThan,
        [Description(">=")]
        GreaterThanOrEqual,
        [Description("=")]
        Equal,
        [Description("!=")]
        NotEqual,
        [Description("<")]
        LessThan,
        [Description("<=")]
        LessThanOrEqual,
        [Description("contains")]
        StringContains
    }
}
