﻿namespace Hospital.Infrastructure.Query.Predicates
{
    /// <summary>
    /// Empty interface defining query predicate.
    /// </summary>
    public interface IPredicate { }
}
