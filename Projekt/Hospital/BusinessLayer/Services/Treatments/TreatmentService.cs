﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BusinessLayer.DataTransferObjects;
using BusinessLayer.DataTransferObjects.Filters;
using BusinessLayer.QueryObjects.Common;
using BusinessLayer.Services.Common;
using DataAccessLayer.Entities;
using Hospital.Infrastructure;
using Hospital.Infrastructure.Query;

namespace BusinessLayer.Services.Treatments
{
    public class TreatmentService : CrudQueryServiceBase<Treatment, TreatmentDto, TreatmentFilterDto>, ITreatmentService
    {
        readonly IRepository<Symptom> symptomRepository;
        readonly QueryObjectBase<SymptomDto, Symptom, SymptomFilterDto, IQuery<Symptom>> symptomQueryObject;

        readonly IRepository<TreatmentSymptom> treatmentSymtomRepository;
        readonly QueryObjectBase<TreatmentSymptomDto, TreatmentSymptom, TreatmentSymptomFilterDto, IQuery<TreatmentSymptom>> treatmentSymptomQueryObject;


        public TreatmentService(IMapper mapper,
            IRepository<Treatment> treatmentRepository, QueryObjectBase<TreatmentDto, Treatment, TreatmentFilterDto, IQuery<Treatment>> treatmentQueryObject,
            IRepository<Symptom> symptomRepository, QueryObjectBase<SymptomDto, Symptom, SymptomFilterDto, IQuery<Symptom>> symptomQueryObject,
            IRepository<TreatmentSymptom> treatmentSymtomRepository,  QueryObjectBase<TreatmentSymptomDto, TreatmentSymptom, TreatmentSymptomFilterDto, IQuery<TreatmentSymptom>> treatmentSymptomQueryObject)
                : base(mapper, treatmentRepository, treatmentQueryObject) {
            
            this.symptomRepository = symptomRepository;
            this.symptomQueryObject = symptomQueryObject;
            this.treatmentSymtomRepository = treatmentSymtomRepository;
            this.treatmentSymptomQueryObject = treatmentSymptomQueryObject;
        }

        /// <summary>
        /// Gets entity (with complex types) according to ID
        /// </summary>
        /// <param name="entityId">entity ID</param>
        /// <returns>The DTO representing the entity</returns>
        protected override async Task<Treatment> GetWithIncludesAsync(Guid entityId)
        {
            return await Repository.GetAsync(entityId, nameof(Treatment.Issue));
        }

        public async Task<QueryResultDto<TreatmentDto, TreatmentFilterDto>> ListAllAsync(TreatmentFilterDto filter)
        {
            return await Query.ExecuteQuery(filter);
        }

        private SymptomDto CreateSymptom(string description)
        {
            var symptomDto = new SymptomDto { Id = Guid.NewGuid(), Description = description };
            symptomRepository.Create(Mapper.Map<Symptom>(symptomDto));
            return symptomDto;
        }

        public void AddSymtomsToTreatment(Guid treatmentId, IEnumerable<Guid> symptoms)
        {
            if (symptoms == null) return;            
            foreach (Guid symptomId in symptoms)
            {
                var ts = Mapper.Map<TreatmentSymptom>(new TreatmentSymptomDto
                {
                        Id = Guid.NewGuid(),
                        SymptomId = symptomId,
                        TreatmentId = treatmentId
                });
                treatmentSymtomRepository.Create(ts);
            }
        }

    }

}
