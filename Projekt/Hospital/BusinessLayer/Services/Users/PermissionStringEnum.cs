﻿using System.ComponentModel;

namespace BusinessLayer.Services.Users
{
    public enum Permission
    {
        [Description("View users")]
        viewUsers,
        [Description("Edit users")]
        editUsers,

        [Description("Access cards")]
        accessCards,
        [Description("Edit cards")]
        editCards,

        [Description("Edit ailments")]
        editAilments,
        [Description("View ailments")]
        viewAilments,

        [Description("Edit symptoms")]
        editSymptoms,
        [Description("View symptoms")]
        viewSymptoms,

        [Description("Edit permissions")]
        editPermissions,

    }

}
