﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BusinessLayer.DataTransferObjects;
using BusinessLayer.DataTransferObjects.Enums;
using BusinessLayer.DataTransferObjects.Filters;
using BusinessLayer.QueryObjects.Common;
using BusinessLayer.Services.Common;
using DataAccessLayer.Entities;
using Hospital.Infrastructure;
using Hospital.Infrastructure.Query;

namespace BusinessLayer.Services.Users
{
    public class UserService : CrudQueryServiceBase<User, UserDto, UserFilterDto>, IUserService
    {
        private readonly QueryObjectBase<CardDto, Card, CardFilterDto, IQuery<Card>> cardQueryObject;
        private readonly IRepository<Card> cardRepository;

        private readonly QueryObjectBase<RoleDto, Role, RoleFilterDto, IQuery<Role>> roleQueryObject;
        private readonly IRepository<Role> roleRepository;

        private readonly QueryObjectBase<PermissionDto, DataAccessLayer.Entities.Permission, PermissionFilterDto, IQuery<DataAccessLayer.Entities.Permission>> permissionQueryObject;
        private readonly IRepository<DataAccessLayer.Entities.Permission> permissionRepository;

        public UserService(IMapper mapper,
            IRepository<User> userRepository, QueryObjectBase<UserDto, User, UserFilterDto, IQuery<User>> userQueryObject,
            IRepository<Card> cardRepository, QueryObjectBase<CardDto, Card, CardFilterDto, IQuery<Card>> cardQueryObject,
            IRepository<Role> roleRepository, QueryObjectBase<RoleDto, Role, RoleFilterDto, IQuery<Role>> roleQueryObject,
            IRepository<DataAccessLayer.Entities.Permission> permissionRepository, QueryObjectBase<PermissionDto, DataAccessLayer.Entities.Permission, PermissionFilterDto, IQuery<DataAccessLayer.Entities.Permission>> permissionQueryObject)
                : base(mapper, userRepository, userQueryObject)
        {
            this.cardQueryObject = cardQueryObject;
            this.cardRepository = cardRepository;
            this.roleQueryObject = roleQueryObject;
            this.roleRepository = roleRepository;
            this.permissionQueryObject = permissionQueryObject;
            this.permissionRepository = permissionRepository;
        }

        /// <summary>
        /// Gets entity (with complex types) according to ID
        /// </summary>
        /// <param name="entityId">entity ID</param>
        /// <returns>The DTO representing the entity</returns>
        protected override async Task<User> GetWithIncludesAsync(Guid entityId)
        {
            return await Repository.GetAsync(entityId, nameof(User.Card), nameof(User.Role));
        }

        /// <summary> Manually updates all user properties. </summary>
        public async Task UpdateUser(UserDto userDto)
        {
            User user = await GetWithIncludesAsync(userDto.Id);
            // Mapper.Map(userDto, user); //! Not working properly
            user.Alive = userDto.Alive;
            user.BirthDate = userDto.BirthDate;
            user.BirthNumber = userDto.BirthNumber;
            // user.Card = userDto.Card;
            user.CardId = userDto.CardId;
            user.FirstName = userDto.FirstName;            
            user.Id = userDto.Id;
            user.Password = userDto.Password;
            //user.Role = userDto.Role;
            user.RoleId = userDto.RoleId.GetValueOrDefault();
            user.Surname = userDto.Surname;
            user.Username = userDto.Username;
            Repository.Update(user);
        }

        /// <summary> Gets the customer with a given birth number. </summary>
        /// <param name="birthNumber">Birth number.</param>
        /// <returns>The customer with given email address</returns>
        public async Task<UserDto> GetUserAccordingToBirthNumberAsync(string birthNumber)
        {
            var queryResult = await Query.ExecuteQuery(new UserFilterDto { BirthNumber = birthNumber });
            return queryResult.Items.SingleOrDefault();
        }

        public async Task<CardDto> GetUserCard(Guid userId)
        {
            var queryResult = await cardQueryObject.ExecuteQuery(new CardFilterDto { UserId = userId });
            if (queryResult.Items.Any()) return queryResult.Items.First();
            return null;
            // return queryResult.Items.FirstOrDefault();
        }

        public async Task<CardDto> CreateCard(Guid userId)
        {
            var queryResult = await cardQueryObject.ExecuteQuery(new CardFilterDto { UserId = userId });
            if (queryResult.Items.Any()) return queryResult.Items.First();
            var newCard = new CardDto { Id = Guid.NewGuid(), UserId = userId };
            cardRepository.Create(Mapper.Map<Card>(newCard));
            return newCard;
        }

        public async Task DeleteCard(Guid userId)
        {
            var queryResult = await cardQueryObject.ExecuteQuery(new CardFilterDto { UserId = userId });
            if (queryResult.Items.Any())
            {
                CardDto card = queryResult.Items.First();
                Delete(card.Id);
            }
        }

        public async Task<(RoleDto, bool)> GetOrCreateRole(RoleTypes role)
        {
            var roleQueryResult = await roleQueryObject.ExecuteQuery(new RoleFilterDto { RoleType = role });
            RoleDto roleDto;
            if (!roleQueryResult.Items.Any())
            {
                roleDto = new RoleDto {RoleType = role };
                var roleDb = Mapper.Map<Role>(roleDto);
                roleRepository.Create(roleDb);
                roleDto.Id = roleDb.Id;
                return (roleDto, true);
            }

            roleDto = roleQueryResult.Items.First();

            return (roleDto, false);
        }

        public async Task ChangeUserRole(Guid userId, RoleTypes role)
        {
            var queryResult = await Query.ExecuteQuery(new UserFilterDto { Id = userId });
            if (!queryResult.Items.Any()) throw new ArgumentException($"user with userId {userId} doesn't exist");
            var user = queryResult.Items.First();
            RoleDto roleDto = (await GetOrCreateRole(role)).Item1;
            user.Role = roleDto;
            await Update(user);
        }

        public async Task<bool> AddPermissionToRole(RoleTypes role, Permission permission)
        {
            var roleDto = (await GetOrCreateRole(role)).Item1;
            string permissionString = permission.ToString();
            var queryResult = await permissionQueryObject.ExecuteQuery(new PermissionFilterDto { Action = permissionString });
            if (queryResult.Items.Any()) return false;
            permissionRepository.Create(Mapper.Map<DataAccessLayer.Entities.Permission>(new PermissionDto { Action = permissionString, Role = roleDto, Id = Guid.NewGuid() }));
            return true;
        }

        public async Task<QueryResultDto<UserDto, UserFilterDto>> GetAllAsync(UserFilterDto filter)
        {
            var result = await Query.ExecuteQuery(filter);
            return result;
        }

        public async Task<bool> UserExists(string username)
        {
            var filter = new UserFilterDto { Username = username };
            var result = await Query.ExecuteQuery(filter);
            return result.Items.Any();
        }

        public async Task<(bool, string)> Authorize(string username, string password)
        {
            var filter = new UserFilterDto { Username = username };
            var result = await Query.ExecuteQuery(filter);
            //might wanna hash this or something, registration as well
            if (!result.Items.Any() || result.Items.First().Password != password) return (false, null);
            var user = result.Items.First();
            var roleResult = await roleQueryObject.ExecuteQuery(new RoleFilterDto { Id = user.RoleId });
            string roleString = null;
            if (roleResult.Items.Any())
            {
                var role = roleResult.Items.First();
                roleString = role.RoleType.ToString();
            }
            return (true, roleString);
        }

        public async Task<string[]> GetPermissionsOfUser(string username)
        {
            var filter = new UserFilterDto { Username = username };
            var result = await Query.ExecuteQuery(filter);
            if (!result.Items.Any()) return new string[] { };
            var user = result.Items.First();
            var roleResult = await roleQueryObject.ExecuteQuery(new RoleFilterDto { Id = user.RoleId });
            if (!roleResult.Items.Any()) return new string[] { };
            var permissions = await permissionQueryObject.ExecuteQuery(new PermissionFilterDto { RoleId = roleResult.Items.First().Id });
            return permissions.Items.Select(p => p.Action.ToString()).ToArray();
        }

        public async Task<string[]> GetPermissionsOfRole(Guid roleId)
        {
            var roleResult = await roleQueryObject.ExecuteQuery(new RoleFilterDto { Id = roleId });
            if (!roleResult.Items.Any()) return new string[] { };
            var permissions = await permissionQueryObject.ExecuteQuery(new PermissionFilterDto { RoleId = roleResult.Items.First().Id });
            return permissions.Items.Select(p => p.Action.ToString()).ToArray();
        }

        public async Task SetPermissionsForRole(RoleTypes roleType, string[] permissions)
        {
            var role = (await GetOrCreateRole(roleType)).Item1;
            var rolePermissionResult = await permissionQueryObject.ExecuteQuery(new PermissionFilterDto { RoleId = role.Id });
            if (rolePermissionResult.Items.Any())
            {
                rolePermissionResult.Items.ToList().ForEach(perm => permissionRepository.Delete(perm.Id));
            }
            foreach (var perm in permissions)
            {
                permissionRepository.Create(Mapper.Map<DataAccessLayer.Entities.Permission>(new PermissionDto { Action = perm, RoleId = role.Id }));
            }
        }
    }
}
