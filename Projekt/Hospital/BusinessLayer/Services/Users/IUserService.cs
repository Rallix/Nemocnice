﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BusinessLayer.DataTransferObjects;
using BusinessLayer.DataTransferObjects.Enums;
using BusinessLayer.DataTransferObjects.Filters;
using BusinessLayer.Services.Common;

namespace BusinessLayer.Services.Users
{

    public interface IUserService
    {
        /// <summary>
        /// Gets DTO representing the entity according to ID
        /// </summary>
        /// <param name="entityId">entity ID</param>
        /// <param name="withIncludes">include all entity complex types</param>
        /// <returns>The DTO representing the entity</returns>
        Task<UserDto> GetAsync(Guid entityId, bool withIncludes = true);

        /// <summary>
        /// Creates new entity
        /// </summary>
        /// <param name="entityDto">entity details</param>
        Guid Create(UserDto entityDto);

        /// <summary>
        /// Updates entity
        /// </summary>
        /// <param name="entityDto">entity details</param>
        Task Update(UserDto entityDto);

        /// <summary> Manually updates all user properties. </summary>
        Task UpdateUser(UserDto userDto);

        /// <summary>
        /// Gets all DTOs (for given type)
        /// </summary>
        /// <returns>all available dtos (for given type)</returns>
        Task<QueryResultDto<UserDto, UserFilterDto>> ListAllAsync();

        /// <summary> Gets the customer with a given birth number. </summary>
        /// <param name="birthNumber">Birth number.</param>
        /// <returns>The customer with given email address</returns>
        Task<UserDto> GetUserAccordingToBirthNumberAsync(string birthNumber);

        Task<CardDto> GetUserCard(Guid userId);

        Task<CardDto> CreateCard(Guid userId);
        Task DeleteCard(Guid userId);

        Task<(RoleDto, bool)> GetOrCreateRole(RoleTypes role);


        Task ChangeUserRole(Guid userId, RoleTypes role);


        Task<bool> AddPermissionToRole(RoleTypes role, Permission permission);

        Task<QueryResultDto<UserDto, UserFilterDto>> GetAllAsync(UserFilterDto filter);

        Task<bool> UserExists(string username);

        Task<(bool, string)> Authorize(string username, string password);

        Task<string[]> GetPermissionsOfUser(string username);

        Task<string[]> GetPermissionsOfRole(Guid roleId);

        Task SetPermissionsForRole(RoleTypes roleType, string[] permissions);
        

    }

}
