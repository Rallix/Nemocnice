﻿using BusinessLayer.DataTransferObjects;
using BusinessLayer.DataTransferObjects.Filters;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BusinessLayer.Services.Issues
{
    public interface IIssueService
    {

        /// <summary>
        /// Creates new entity
        /// </summary>
        /// <param name="entityDto">entity details</param>
        Guid Create(IssueDto entityDto);

        /// <summary>
        /// Updates entity
        /// </summary>
        /// <param name="entityDto">entity details</param>
        Task Update(IssueDto entityDto);

        /// <summary>
        /// Deletes entity with given Id
        /// </summary>
        /// <param name="entityId">Id of the entity to delete</param>
        void Delete(Guid entityId);

        /// <summary>
        /// Gets all DTOs (for given type)
        /// </summary>
        /// <returns>all available dtos (for given type)</returns>
        Task<QueryResultDto<IssueDto, IssueFilterDto>> ListAllAsync();

        Task<QueryResultDto<IssueDto, IssueFilterDto>> ListIssuesAsync(IssueFilterDto filter);

        Task<QueryResultDto<IssueDto, IssueFilterDto>> ListIssuesFromCardAsync(Guid cardId, bool? solved);

        Task<IEnumerable<SymptomDto>> GetSymptomsOfIssue(Guid issueId);

        Task<IEnumerable<(TreatmentDto Treatment, IEnumerable<SymptomDto> Symptoms)>> GetSymptomsOfTreatments(Guid issueId);
        
        /// <summary> Resolves an issue. </summary>
        Task MarkResolved(IssueDto issueDto);

        Task<IssueDto> GetAsync(Guid entityId, bool withIncludes = true);


    }
}
