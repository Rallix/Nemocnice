﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BusinessLayer.DataTransferObjects;
using BusinessLayer.DataTransferObjects.Filters;
using BusinessLayer.QueryObjects.Common;
using BusinessLayer.Services.Common;
using DataAccessLayer.Entities;
using Hospital.Infrastructure;
using Hospital.Infrastructure.Query;

namespace BusinessLayer.Services.Issues
{

    public class IssueService : CrudQueryServiceBase<Issue, IssueDto, IssueFilterDto>, IIssueService
    {

        public IssueService(IMapper mapper, IRepository<Issue> repository, QueryObjectBase<IssueDto, Issue, IssueFilterDto, IQuery<Issue>> query) : base(mapper, repository, query) { }

        protected override async Task<Issue> GetWithIncludesAsync(Guid entityId)
        {
            return await Repository.GetAsync(entityId, nameof(Issue.Ailment), nameof(Issue.Card), nameof(Issue.Treatments));
        }

        public async Task<QueryResultDto<IssueDto, IssueFilterDto>> ListIssuesAsync(IssueFilterDto filter)
        {
            return await Query.ExecuteQuery(filter);
        }

        public async Task<QueryResultDto<IssueDto, IssueFilterDto>> ListIssuesFromCardAsync(Guid cardId, bool? solved)
        {
            return await Query.ExecuteQuery(new IssueFilterDto {Card = cardId, Solved = solved});
        }       

        public async Task<IEnumerable<SymptomDto>> GetSymptomsOfIssue(Guid issueId)
        {
            Issue result = await Repository.GetAsync(issueId, "Treatments.TreatmentSymptoms.Symptom");
            var symptoms = result.Treatments.Select(t => t.TreatmentSymptoms)
                                 .SelectMany(ts => ts)
                                 .Select(ts => ts.Symptom);
            return symptoms.Select(symptom => Mapper.Map<SymptomDto>(symptom));
        }

        public async Task<IEnumerable<(TreatmentDto Treatment, IEnumerable<SymptomDto> Symptoms)>> GetSymptomsOfTreatments(Guid issueId)
        {
            Issue issue = await Repository.GetAsync(issueId, "Treatments.TreatmentSymptoms.Symptom");
                        
            var pairs = new List<(TreatmentDto, IEnumerable<SymptomDto>)>();
            foreach (Treatment treatment in issue.Treatments)
            {
                var treatmentDto = Mapper.Map<TreatmentDto>(treatment);
                var symptomDtos = treatment.TreatmentSymptoms.Select(ts => ts.Symptom)
                                        .Select(symptom => Mapper.Map<SymptomDto>(symptom));
                pairs.Add((treatmentDto, symptomDtos));
            }            
            return pairs.AsEnumerable();
        }

        /// <summary> Manually updates all user properties. </summary>
        public async Task MarkResolved(IssueDto issueDto)
        {
            Issue issue = await GetWithIncludesAsync(issueDto.Id);
            Mapper.Map(issueDto, issue);
            issue.Solved = true;
            Repository.Update(issue);
        }

    }

}
