﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BusinessLayer.DataTransferObjects;
using BusinessLayer.DataTransferObjects.Filters;
using BusinessLayer.QueryObjects;
using BusinessLayer.QueryObjects.Common;
using BusinessLayer.Services.Common;
using DataAccessLayer.Entities;
using Hospital.Infrastructure;
using Hospital.Infrastructure.Query;

namespace BusinessLayer.Services.Ailments
{
    public class AilmentService : CrudQueryServiceBase<Ailment, AilmentDto, AilmentFilterDto>, IAilmentService
    {
        readonly QueryObjectBase<AilmentSymptomDto, AilmentSymptom, AilmentSymptomFilterDto, IQuery<AilmentSymptom>> ailmentSymptomQueryObject;
        readonly QueryObjectBase<SymptomDto, Symptom, SymptomFilterDto, IQuery<Symptom>> symptomQueryObject;
        readonly IRepository<Symptom> symptomRepository;
        readonly IRepository<AilmentSymptom> ailmentSymptomRepository;

        public AilmentService(IMapper mapper, IRepository<Ailment> ailmentRepository, QueryObjectBase<AilmentDto, Ailment, AilmentFilterDto, IQuery<Ailment>> ailmentQuery,
            IRepository<AilmentSymptom> ailmentSymptomRepository, QueryObjectBase<AilmentSymptomDto, AilmentSymptom, AilmentSymptomFilterDto, IQuery<AilmentSymptom>> ailmentSymptomQueryObject,
            IRepository<Symptom> symptomRepository, QueryObjectBase<SymptomDto, Symptom, SymptomFilterDto, IQuery<Symptom>> symptomQueryObject)
            : base(mapper, ailmentRepository, ailmentQuery)
        {
            this.ailmentSymptomQueryObject = ailmentSymptomQueryObject;
            this.symptomQueryObject = symptomQueryObject;
            this.symptomRepository = symptomRepository;
            this.ailmentSymptomRepository = ailmentSymptomRepository;
        }

        

        public async Task<AilmentDto> GetAilmentByNameAsync(string name)
        {
            var queryResult = await Query.ExecuteQuery(new AilmentFilterDto { Name = name });
            return queryResult.Items.SingleOrDefault();
        }

        public async Task<QueryResultDto<AilmentDto, AilmentFilterDto>> GetAilmentsWhichHaveSymptomsAsync(IEnumerable<string> symptoms)
        {
            var symptomQueryResult = await symptomQueryObject.ExecuteQuery(new SymptomFilterDto { AnyOfDescriptions = symptoms.ToArray() });
            var symptomIds = symptomQueryResult.Items.Select(s => s.Id).ToArray();
            var ailmentSymptomQueryResult = await ailmentSymptomQueryObject.ExecuteQuery(new AilmentSymptomFilterDto { AnySymptom = symptomIds.ToArray() });
            var ailmentIds = ailmentSymptomQueryResult.Items.Select(asq => asq.AilmentId).ToArray();
            return await Query.ExecuteQuery(new AilmentFilterDto { AnyId = ailmentIds });
        }

        public Guid CreateAilmentWithSymptoms(AilmentDto ailmentDto, IEnumerable<string> symptoms)
        {
            Guid ailmentId = Create(ailmentDto);
            AddSymtomsToAilment(ailmentId, symptoms);
            return ailmentId;
        }

        public SymptomDto CreateSymptom(string description)
        {
            var symptomDto = new SymptomDto { Id = Guid.NewGuid(), Description = description };
            symptomRepository.Create(Mapper.Map<Symptom>(symptomDto));
            return symptomDto;
        }

        public void AddSymtomsToAilment(Guid ailmentId, IEnumerable<string> symptoms)
        {
            var symptomIdList = symptoms.Select(async symptom => await symptomQueryObject.ExecuteQuery(new SymptomFilterDto { Description = symptom }))
                .Select(queryResult => queryResult.Result)
                .Where(result => result != null)
                .Select(result => result.Items == null || !result.Items.Any() ? 
                            CreateSymptom(result.Filter.Description)?.Id :
                            result.Items.First()?.Id)
                .Where(id=> id!=null)
                .Select(id=>id.Value).ToList();
            symptomIdList.Select(symtomId => new AilmentSymptomDto { SymptomId = symtomId, AilmentId = ailmentId }).ToList()
                .ForEach(sa => ailmentSymptomRepository.Create(Mapper.Map<AilmentSymptom>(sa)));
        }


        public async Task<QueryResultDto<AilmentDto, AilmentFilterDto>> ListAilmentsAsync(AilmentFilterDto filter)
        {
            return await Query.ExecuteQuery(filter);
        }

        protected override Task<Ailment> GetWithIncludesAsync(Guid entityId)
        {
            return Repository.GetAsync(entityId, nameof(Ailment.SymptomAilments));
        }

        public async Task<QueryResultDto<SymptomDto, SymptomFilterDto>> ListSymptomsAsync(SymptomFilterDto filter)
        {
            return await symptomQueryObject.ExecuteQuery(filter);
        }

        public async Task<QueryResultDto<AilmentSymptomDto, AilmentSymptomFilterDto>> ListAilmentSymptomsAsync(AilmentSymptomFilterDto filter)
        {
            return await ailmentSymptomQueryObject.ExecuteQuery(filter);
        }

        public async Task<bool> SymptomExists(string description)
        {
            return (await symptomQueryObject.ExecuteQuery(new SymptomFilterDto { Description = description })).Items.Any();
        }

        public async Task DeleteSymptom(Guid id)
        {
            var ailmentSymptoms = (await ailmentSymptomQueryObject.ExecuteQuery(new AilmentSymptomFilterDto { SymptomId = id })).Items.ToList();
            ailmentSymptoms.ForEach(a => ailmentSymptomRepository.Delete(a.Id));
            symptomRepository.Delete(id);
        }
        public void DeleteAilmentSymptom(Guid id)
        {
            ailmentSymptomRepository.Delete(id);
        }

        public async Task DeleteAilment(Guid id)
        {
            var ailmentSymptoms = (await ailmentSymptomQueryObject.ExecuteQuery(new AilmentSymptomFilterDto { AilmentId = id })).Items.ToList();
            ailmentSymptoms.ForEach(a => ailmentSymptomRepository.Delete(a.Id));

            // The DELETE statement conflicted with the REFERENCE constraint "FK_dbo.Issues_dbo.Ailments_AilmentId".
            // The conflict occurred in database "PV179Hospital", table "dbo.Issues", column 'AilmentId'.
            // The statement has been terminated.

            Repository.Delete(id);
        }

        public Guid CreateAilmentWithSymptoms(string ailmentName, IEnumerable<Guid> symptomIds)
        {
            var ailment = new AilmentDto { Name = ailmentName};
            var ailmentId = Create(ailment);
            symptomIds.ToList().ForEach(
                symptomId => ailmentSymptomRepository.Create(
                    Mapper.Map<AilmentSymptom>(
                        new AilmentSymptomDto
                        {
                            Id = Guid.NewGuid(),
                            AilmentId = ailmentId,
                            SymptomId = symptomId
                        })
                    )
                );
            return ailment.Id;
        }

        public Guid CreateAilmentSymptom(AilmentSymptomDto asy)
        {
            asy.Id = Guid.NewGuid();
            ailmentSymptomRepository.Create(Mapper.Map<AilmentSymptomDto, AilmentSymptom>(asy));
            return asy.Id;
        }
    }
}
