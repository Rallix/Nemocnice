﻿using BusinessLayer.DataTransferObjects;
using BusinessLayer.DataTransferObjects.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Services.Ailments
{
    public interface IAilmentService
    {
        /// <summary>
        /// Gets ailments according to given filter
        /// </summary>
        /// <param name="filter">The ailments filter</param>
        /// <returns>Filtered results</returns>
        Task<QueryResultDto<AilmentDto, AilmentFilterDto>> ListAilmentsAsync(AilmentFilterDto filter);

        Task<QueryResultDto<AilmentDto, AilmentFilterDto>> GetAilmentsWhichHaveSymptomsAsync(IEnumerable<string> symptoms);

        Guid CreateAilmentWithSymptoms(AilmentDto ailmentDto, IEnumerable<string> symptoms);
        Guid CreateAilmentWithSymptoms(string ailmentName, IEnumerable<Guid> symptomIds);

        /// <summary>
        /// Gets DTO representing the entity according to ID
        /// </summary>
        /// <param name="entityId">entity ID</param>
        /// <param name="withIncludes">include all entity complex types</param>
        /// <returns>The DTO representing the entity</returns>
        Task<AilmentDto> GetAsync(Guid entityId, bool withIncludes = true);

        /// <summary>
        /// Gets ailment with given name
        /// </summary>
        /// <param name="name">ailment name</param>
        /// <returns>ailment with given name</returns>
        Task<AilmentDto> GetAilmentByNameAsync(string name);

        /// <summary>
        /// Creates new entity
        /// </summary>
        /// <param name="entityDto">entity details</param>
        Guid Create(AilmentDto entityDto);

        /// <summary>
        /// Updates entity
        /// </summary>
        /// <param name="entityDto">entity details</param>
        Task Update(AilmentDto entityDto);

        /// <summary>
        /// Deletes entity with given Id
        /// </summary>
        /// <param name="entityId">Id of the entity to delete</param>
        Task DeleteAilment(Guid entityId);

        /// <summary>
        /// Gets all DTOs (for given type)
        /// </summary>
        /// <returns>all available dtos (for given type)</returns>
        Task<QueryResultDto<AilmentDto, AilmentFilterDto>> ListAllAsync();

        Task<QueryResultDto<SymptomDto, SymptomFilterDto>> ListSymptomsAsync(SymptomFilterDto filter);
        Task<QueryResultDto<AilmentSymptomDto, AilmentSymptomFilterDto>> ListAilmentSymptomsAsync(AilmentSymptomFilterDto filter);
        Task<bool> SymptomExists(string description);

        SymptomDto CreateSymptom(string description);

        Task DeleteSymptom(Guid id);

        void DeleteAilmentSymptom(Guid id);

        Guid CreateAilmentSymptom(AilmentSymptomDto asy);
    }
}

