﻿using AutoMapper;
using BusinessLayer.DataTransferObjects;
using BusinessLayer.QueryObjects.Common;
using Hospital.Infrastructure;
using Hospital.Infrastructure.Query;
using System;
using System.Threading.Tasks;

namespace BusinessLayer.Services.Common
{
    public abstract class CrudQueryServiceBase<TEntity, TDto, TFilterDto> : ServiceBase
           where TFilterDto : FilterDtoBase, new()
           where TEntity : class, IEntity, new()
           where TDto : DtoBase
    {
        protected readonly IRepository<TEntity> Repository;

        protected readonly QueryObjectBase<TDto, TEntity, TFilterDto, IQuery<TEntity>> Query;

        protected CrudQueryServiceBase(IMapper mapper, IRepository<TEntity> repository, QueryObjectBase<TDto, TEntity, TFilterDto, IQuery<TEntity>> query) : base(mapper)
        {
            this.Query = query;
            this.Repository = repository;
        }

        /// <summary>
        /// Gets DTO representing the entity according to ID
        /// </summary>
        /// <param name="entityId">Entity <see cref="Guid"/>.</param>
        /// <param name="withIncludes">Include all entity complex types.</param>
        /// <returns>The data transfer object representing the entity.</returns>
        public virtual async Task<TDto> GetAsync(Guid entityId, bool withIncludes = true)
        {
            TEntity entity;
            if (withIncludes)
            {
                entity = await GetWithIncludesAsync(entityId);
            }
            else
            {
                entity = await Repository.GetAsync(entityId);
            }
            return entity != null ? Mapper.Map<TDto>(entity) : null;
        }

        /// <summary>
        /// Gets entity (with complex types) according to its ID.
        /// </summary>
        /// <param name="entityId">Entity ID.</param>
        /// <returns>The data transfer object representing the entity.</returns>
        protected abstract Task<TEntity> GetWithIncludesAsync(Guid entityId);

        /// <summary>
        /// Creates a new entity.
        /// </summary>
        /// <param name="entityDto">Entity details.</param>
        public virtual Guid Create(TDto entityDto)
        {
            var entity = Mapper.Map<TEntity>(entityDto);
            Repository.Create(entity);
            return entity.Id;
        }

        /// <summary>
        /// Updates the entity.
        /// </summary>
        /// <param name="entityDto">Entity details.</param>
        public virtual async Task Update(TDto entityDto)
        {
            TEntity entity = await GetWithIncludesAsync(entityDto.Id);
            Mapper.Map(entityDto, entity);            
            Repository.Update(entity);
        }

        /// <summary>
        /// Deletes a entity with a given Id.
        /// </summary>
        /// <param name="entityId">ID of the entity to delete.</param>
        public virtual void Delete(Guid entityId)
        {
            Repository.Delete(entityId);
        }

        /// <summary>
        /// Gets all DTOs (for the given type).
        /// </summary>
        /// <returns>All available DTOs (for the given type).</returns>
        public virtual async Task<QueryResultDto<TDto, TFilterDto>> ListAllAsync()
        {
            return await Query.ExecuteQuery(new TFilterDto());
        }
    }
}
