﻿using AutoMapper;
using BusinessLayer.DataTransferObjects;
using BusinessLayer.DataTransferObjects.Filters;
using BusinessLayer.QueryObjects.Common;
using BusinessLayer.Services.Common;
using DataAccessLayer.Entities;
using Hospital.Infrastructure;
using Hospital.Infrastructure.Query;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace BusinessLayer.Services.Symptoms
{
    public class SymptomService : CrudQueryServiceBase<Symptom, SymptomDto, SymptomFilterDto>, ISymptomService
    {
        public SymptomService(IMapper mapper, QueryObjectBase<SymptomDto, Symptom, SymptomFilterDto, IQuery<Symptom>> symptomQuery, IRepository<Symptom> symptomRepository)
            : base(mapper, symptomRepository, symptomQuery) { }

        public async Task<SymptomDto> GetSymptomByNameAsync(string name)
        {
            return (await Query.ExecuteQuery(new SymptomFilterDto {Description = name})).Items.SingleOrDefault();
        }

        public async Task<QueryResultDto<SymptomDto, SymptomFilterDto>> ListSymptomsAsync(SymptomFilterDto filter)
        {
            return await Query.ExecuteQuery(filter);
        }

        protected override Task<Symptom> GetWithIncludesAsync(Guid entityId)
        {
            return Repository.GetAsync(entityId);
        }
    }
}
