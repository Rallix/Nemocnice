﻿using BusinessLayer.DataTransferObjects;
using BusinessLayer.DataTransferObjects.Filters;
using System;
using System.Threading.Tasks;

namespace BusinessLayer.Services.Symptoms
{
    public interface ISymptomService
    {
       
            /// <summary>
            /// Gets symptoms according to given filter
            /// </summary>
            /// <param name="filter">The symptoms filter</param>
            /// <returns>Filtered results</returns>
            Task<QueryResultDto<SymptomDto, SymptomFilterDto>> ListSymptomsAsync(SymptomFilterDto filter);

            /// <summary>
            /// Gets DTO representing the entity according to ID
            /// </summary>
            /// <param name="entityId">entity ID</param>
            /// <param name="withIncludes">include all entity complex types</param>
            /// <returns>The DTO representing the entity</returns>
            Task<SymptomDto> GetAsync(Guid entityId, bool withIncludes = true);

            /// <summary>
            /// Gets symptom with given name
            /// </summary>
            /// <param name="name">symptom name</param>
            /// <returns>symptom with given name</returns>
            Task<SymptomDto> GetSymptomByNameAsync(string name);

            /// <summary>
            /// Creates new entity
            /// </summary>
            /// <param name="entityDto">entity details</param>
            Guid Create(SymptomDto entityDto);

            /// <summary>
            /// Updates entity
            /// </summary>
            /// <param name="entityDto">entity details</param>
            Task Update(SymptomDto entityDto);

            /// <summary>
            /// Deletes entity with given Id
            /// </summary>
            /// <param name="entityId">Id of the entity to delete</param>
            void Delete(Guid entityId);

            /// <summary>
            /// Gets all DTOs (for given type)
            /// </summary>
            /// <returns>all available dtos (for given type)</returns>
            Task<QueryResultDto<SymptomDto, SymptomFilterDto>> ListAllAsync();
        
    }
}
