﻿using BusinessLayer.DataTransferObjects;
using BusinessLayer.DataTransferObjects.Filters;
using BusinessLayer.Facades.Common;
using BusinessLayer.Services.Issues;
using Hospital.Infrastructure.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessLayer.Services.Treatments;

namespace BusinessLayer.Facades
{

    public class IssueFacade : FacadeBase
    {

        readonly IIssueService issueService;
        readonly ITreatmentService treatmentService;

        public IssueFacade(IUnitOfWorkProvider unitOfWorkProvider, IIssueService issueService, ITreatmentService treatmentService) : base(unitOfWorkProvider)
        {
            this.issueService = issueService;
            this.treatmentService = treatmentService;
        }

        public async Task<QueryResultDto<IssueDto, IssueFilterDto>> GetAllIssuesAsync(IssueFilterDto filter)
        {
            using (UnitOfWorkProvider.Create())
            {
                return await issueService.ListIssuesAsync(filter);
            }
        }

        public async Task<IssueDto> GetIssueAsync(Guid guid)
        {
            using (UnitOfWorkProvider.Create())
            {
                return await issueService.GetAsync(guid);
            }
        }

        public async Task<AilmentDto> GetAilmentOfIssue(Guid guid)
        {
            using (UnitOfWorkProvider.Create())
            {
                IssueDto issueWithIncludes = await issueService.GetAsync(guid);
                return issueWithIncludes.Ailment;
            }
        }

        public async Task<QueryResultDto<IssueDto, IssueFilterDto>> GetIssuesOfCardAsync(Guid cardId, bool? solved = null)
        {
            using (UnitOfWorkProvider.Create())
            {
                return await issueService.ListIssuesFromCardAsync(cardId, solved);
            }
        }

        public async Task<Guid> CreateIssue(IssueDto issueDto)
        {
            using (IUnitOfWork uow = UnitOfWorkProvider.Create())
            {
                issueDto.Id = Guid.NewGuid();
                var issueId = issueService.Create(issueDto);
                await uow.Commit();
                return issueId;
            }
        }

        public async Task<bool> EditIssueAsync(IssueDto issueDto)
        {
            using (var uow = UnitOfWorkProvider.Create())
            {
                if ((await issueService.GetAsync(issueDto.Id, false)) == null)
                {
                    return false;
                }
                
                // Delete to prevent foreign key errors
                issueDto.Treatments.ToList().ForEach(async treatment => await DeleteTreatmentAsync(treatment.Id));
                
                var pairs = await GetSymptomsOfTreatments(issueDto.Id);

                // Update
                await issueService.Update(issueDto);
                await uow.Commit();

                foreach (var pair in pairs)
                {
                    // Symptomps are now deleted – add again
                    await AddSymptomsToTreatment(pair.Treatment.Id, pair.Symptoms.Select(s => s.Id));
                }
                return true;
            }
        }

        public async Task<bool> DeleteIssueAsync(Guid id)
        {
            using (var uow = UnitOfWorkProvider.Create())
            {
                if ((await issueService.GetAsync(id, false)) == null)
                {
                    return false;
                }
                issueService.Delete(id);
                await uow.Commit();
                return true;
            }
        }

        public async Task<Guid> AddTreatment(TreatmentDto treatment)
        {
            using (var uow = UnitOfWorkProvider.Create())
            {
                var id = treatmentService.Create(treatment);
                await uow.Commit();
                return id;
            }
        }

        public async Task<bool> DeleteTreatmentAsync(Guid id)
        {
            using (var uow = UnitOfWorkProvider.Create())
            {
                if ((await treatmentService.GetAsync(id, false)) == null)
                {
                    return false;
                }
                treatmentService.Delete(id);
                await uow.Commit();
                return true;
            }
        }

        public async Task<IEnumerable<TreatmentDto>> GetTreatmentsOfIssue(Guid issueId)
        {
            using (UnitOfWorkProvider.Create())
            {
                return (await treatmentService.ListAllAsync(new TreatmentFilterDto {IssueId = issueId})).Items;
            }
        }

        public async Task<IEnumerable<SymptomDto>> GetSymptomsOfIssue(Guid issueId)
        {
            using (UnitOfWorkProvider.Create())
            {
                return await issueService.GetSymptomsOfIssue(issueId);
            }
        }

        public async Task<IEnumerable<(TreatmentDto Treatment, IEnumerable<SymptomDto> Symptoms)>> GetSymptomsOfTreatments(Guid issueId)
        {
            using (UnitOfWorkProvider.Create())
            {
                return await issueService.GetSymptomsOfTreatments(issueId);
            }
        }

        public async Task AddSymptomsToTreatment(Guid treatmentId, IEnumerable<Guid> symptoms)
        {
            using (var uow = UnitOfWorkProvider.Create())
            {
                treatmentService.AddSymtomsToTreatment(treatmentId, symptoms);
                await uow.Commit();
            }
        }

    }

}
