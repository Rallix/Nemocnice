﻿using BusinessLayer.DataTransferObjects;
using BusinessLayer.DataTransferObjects.Filters;
using BusinessLayer.Facades.Common;
using BusinessLayer.Services.Ailments;
using Hospital.Infrastructure.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Threading.Tasks;

namespace BusinessLayer.Facades
{

    public class AilmentFacade : FacadeBase
    {

        readonly IAilmentService ailmentService;
        //readonly ISymptomService symptomService;

        public AilmentFacade(IUnitOfWorkProvider unitOfWorkProvider, IAilmentService ailmentService) : base(unitOfWorkProvider)
        {
            this.ailmentService = ailmentService;
            // this.symptomService = symptomService;
        }

        public async Task<QueryResultDto<AilmentDto, AilmentFilterDto>> GetAilmentsAsync(AilmentFilterDto filter)
        {
            using (UnitOfWorkProvider.Create())
            {
                return await ailmentService.ListAilmentsAsync(filter);
            }
        }

        public async Task<QueryResultDto<SymptomDto, SymptomFilterDto>> GetSymptomsAsync(SymptomFilterDto filter)
        {
            using (UnitOfWorkProvider.Create())
            {
                return await ailmentService.ListSymptomsAsync(filter);
            }
        }

        public async Task<QueryResultDto<AilmentSymptomDto, AilmentSymptomFilterDto>> GetAilmentSymptomsAsync(AilmentSymptomFilterDto filter)
        {
            using (UnitOfWorkProvider.Create())
            {
                return await ailmentService.ListAilmentSymptomsAsync(filter);
            }
        }

        public async Task<Guid> CreateAilmentWithSymptoms(AilmentDto ailmentDto, IEnumerable<string> symptoms)
        {
            using (IUnitOfWork uow = UnitOfWorkProvider.Create())
            {
                var ailmentId = ailmentService.CreateAilmentWithSymptoms(ailmentDto, symptoms);
                await uow.Commit();
                return ailmentId;
            }
        }

        public async Task<Guid> CreateAilmentWithSymptoms(string ailmentName, IEnumerable<Guid> symptomIds)
        {
            using (IUnitOfWork uow = UnitOfWorkProvider.Create())
            {
                var ailmentId = ailmentService.CreateAilmentWithSymptoms(ailmentName, symptomIds);
                await uow.Commit();
                return ailmentId;
            }
        }

        public async Task<AilmentDto> GetAilmentAsync(string name)
        {
            using (UnitOfWorkProvider.Create())
            {
                return await ailmentService.GetAilmentByNameAsync(name);
            }
        }

        public async Task<IEnumerable<AilmentDto>> GetAilmentsWhichHaveSymptomsAsync(IEnumerable<string> symptoms)
        {
            using (UnitOfWorkProvider.Create())
            {
                return (await ailmentService.GetAilmentsWhichHaveSymptomsAsync(symptoms)).Items;
            }
        }

        public async Task<bool> EditAilmentAsync(AilmentDto ailmentDto)
        {
            using (var uow = UnitOfWorkProvider.Create())
            {
                if ((await ailmentService.GetAsync(ailmentDto.Id, false)) == null)
                {
                    return false;
                }
                await ailmentService.Update(ailmentDto);
                try
                {
                    await uow.Commit();
                }
                catch (DbEntityValidationException vex)
                {
                    foreach (var evalError in vex.EntityValidationErrors)
                    {
                        foreach (var error in evalError.ValidationErrors)
                        {
                            Console.WriteLine($"{error.PropertyName}: {error.ErrorMessage}");
                        }
                    }
                }

                return true;
            }
        }

        public async Task AddAilmentSymptoms(IEnumerable<AilmentSymptomDto> ailmentSymptoms)
        {
            using (var uow = UnitOfWorkProvider.Create())
            {
                foreach (var asy in ailmentSymptoms)
                {
                    ailmentService.CreateAilmentSymptom(asy);
                }
                await uow.Commit();
            }
        }

        public async Task<bool> DeleteAilmentAsync(Guid id)
        {
            using (var uow = UnitOfWorkProvider.Create())
            {
                if ((await ailmentService.GetAsync(id, false)) == null)
                {
                    return false;
                }
                await ailmentService.DeleteAilment(id);                
                await uow.Commit();
                return true;
            }
        }

        public async Task<bool> SymptomExists(string description)
        {
            using (var uow = UnitOfWorkProvider.Create())
            {
                return await ailmentService.SymptomExists(description);
            }
        }
        public async Task<Guid> AddSymptom(SymptomDto symptom)
        {
            using (var uow = UnitOfWorkProvider.Create())
            {
                symptom = ailmentService.CreateSymptom(symptom.Description);
                await uow.Commit();
                return symptom.Id;
            }
        }

        public async Task<bool> DeleteSymptomAsync(Guid id)
        {
            using (var uow = UnitOfWorkProvider.Create())
            {
                if (!(await ailmentService.ListSymptomsAsync(new SymptomFilterDto { AnyIds = new Guid[] { id } })).Items.Any())
                {
                    return false;
                }
                await ailmentService.DeleteSymptom(id);
                await uow.Commit();
                return true;
            }
        }

        public async Task<bool> DeleteAilmentSymptomAsync(Guid id)
        {
            using (var uow = UnitOfWorkProvider.Create())
            {
                ailmentService.DeleteAilmentSymptom(id);
                await uow.Commit();
                return true;
            }
        }
    }

}
