﻿using BusinessLayer.DataTransferObjects;
using BusinessLayer.DataTransferObjects.Enums;
using BusinessLayer.DataTransferObjects.Filters;
using BusinessLayer.Facades.Common;
using BusinessLayer.Services.Users;
using Hospital.Infrastructure.UnitOfWork;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace BusinessLayer.Facades
{

    public class UserFacade : FacadeBase
    {

        readonly IUserService userService;

        public UserFacade(IUnitOfWorkProvider unitOfWorkProvider, IUserService userService) : base(unitOfWorkProvider)
        {
            this.userService = userService;
        }

        public async Task<bool> EditUserAsync(UserDto userDto)
        {
            using (var uow = UnitOfWorkProvider.Create())
            {
                if ((await userService.GetAsync(userDto.Id, false)) == null)
                {
                    return false;
                }
                await userService.Update(userDto);
                await uow.Commit();
                return true;
            }
        }

        public async Task<UserDto> GetUserAccordingToBirthNumberAsync(string birthNumber)
        {
            using (var uow = UnitOfWorkProvider.Create()) return await userService.GetUserAccordingToBirthNumberAsync(birthNumber);
        }


        /// <summary> Declares the user dead which forbids creating new issues. </summary>
        public async Task DeclareDead(Guid id)
        {
            using (var uow = UnitOfWorkProvider.Create())
            {
                UserDto userDto = await userService.GetAsync(id);
                userDto.Alive = false;
                await userService.UpdateUser(userDto);
                await uow.Commit();
            }
        }

        public async Task UpdateUser(UserDto userDto)
        {
            using (var uow = UnitOfWorkProvider.Create())
            {
                await userService.UpdateUser(userDto);
                await uow.Commit();
            }
        }


        public async Task<Guid?> RegisterUser(UserDto userDto)
        {
            using (var uow = UnitOfWorkProvider.Create())
            {
                if ((await userService.GetAllAsync(new UserFilterDto {Username = userDto.Username})).Items.Any()) return null;
                userService.Create(userDto);
                await uow.Commit();
                return userDto.Id;
            }
        }

        public async Task<CardDto> GetUserCard(Guid userId)
        {
            using (var uow = UnitOfWorkProvider.Create()) return await userService.GetUserCard(userId);
        }

        public async Task DeleteCard(Guid userId)
        {
            using (var uow = UnitOfWorkProvider.Create())
            {
                await userService.DeleteCard(userId);
                await uow.Commit();
            }
        }

        public async Task<bool> HasCard(Guid userId)
        {
            CardDto card = await GetUserCard(userId);
            return card != null;
        }

        public async Task<CardDto> CreateCard(Guid userId)
        {
            using (var uow = UnitOfWorkProvider.Create())
            {
                var card = await userService.CreateCard(userId);
                try
                {
                    await uow.Commit();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

                return card;
            }
        }


        public async Task<RoleDto> GetOrCreateRole(RoleTypes role)
        {
            using (var uow = UnitOfWorkProvider.Create())
            {
                var (roleDto, commit) = await userService.GetOrCreateRole(role);
                if (commit) await uow.Commit();
                return roleDto;
            }
        }

        public async Task ChangeUserRole(Guid userId, RoleTypes role)
        {
            using (var uow = UnitOfWorkProvider.Create())
            {
                await userService.ChangeUserRole(userId, role);
                await uow.Commit();
            }
        }

        public async Task<bool> AddPermissionToRole(RoleTypes role, Permission permission)
        {
            using (var uow = UnitOfWorkProvider.Create())
            {
                var result = await userService.AddPermissionToRole(role, permission);
                await uow.Commit();
                return result;
            }
        }

        public async Task<QueryResultDto<UserDto, UserFilterDto>> GetUsersAsync(UserFilterDto filter)
        {
            using (var uow = UnitOfWorkProvider.Create())
            {
                return await userService.GetAllAsync(filter);
            }
        }

        public async Task<bool> UserExists(string username)
        {
            using (var uow = UnitOfWorkProvider.Create())
            {
                return await userService.UserExists(username);
            }
        }

        public async Task<(bool, string)> Login(string username, string password)
        {
            using (var uow = UnitOfWorkProvider.Create())
            {
                return await userService.Authorize(username, password);
            }
        }

        public async Task<string[]> GetPermissionsOfUser(string username)
        {
            using (var uow = UnitOfWorkProvider.Create())
            {
                return await userService.GetPermissionsOfUser(username);
            }
        }

        public async Task<string[]> GetPermissionsOfRole(Guid roleId)
        {
            using (var uow = UnitOfWorkProvider.Create())
            {
                return await userService.GetPermissionsOfRole(roleId);
            }
        }

        public async Task SetPermissionsForRole(RoleTypes roleType, string[] permissions)
        {
            using (var uow = UnitOfWorkProvider.Create())
            {
                await userService.SetPermissionsForRole(roleType, permissions);
                await uow.Commit();
            }
        }

        public async Task<string> GenerateUsername(string username, string symbols) {
            var random = new Random();
            for (int i = 1; i < symbols.Length; i++) {
                for (int j = 0; j <= 20; j++) {
                    string suffix = new string(Enumerable.Repeat(symbols, random.Next(1, i))
                                                         .Select(s => s[random.Next(s.Length)]).ToArray());
                    string newUsername = $"{username}{suffix}";
                    if (!await UserExists(newUsername)) return newUsername;
                }
            }
            return username;
        }

    }

}
