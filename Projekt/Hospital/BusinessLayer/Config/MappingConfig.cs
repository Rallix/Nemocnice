﻿using AutoMapper;
using BusinessLayer.DataTransferObjects;
using BusinessLayer.DataTransferObjects.Filters;
using DataAccessLayer.Entities;
using Hospital.Infrastructure.Query;


namespace BusinessLayer.Config
{
    public class MappingConfig
    {
        public static void ConfigureMapping(IMapperConfigurationExpression config)
        {
            config.CreateMap<Ailment, AilmentDto>();
            config.CreateMap<AilmentSymptom, AilmentSymptomDto>();
            config.CreateMap<AilmentSymptomDto, AilmentSymptom>().ConstructUsing(x=> new AilmentSymptom()) ;
            config.CreateMap<Card, CardDto>(); 
            config.CreateMap<Issue, IssueDto>();
            config.CreateMap<Permission, PermissionDto>();
            config.CreateMap<Role, RoleDto>();
            config.CreateMap<Symptom, SymptomDto>();
            config.CreateMap<Treatment, TreatmentDto>();
            config.CreateMap<TreatmentSymptom, TreatmentSymptomDto>();
            config.CreateMap<TreatmentSymptomDto, TreatmentSymptom>().ConstructUsing(x => new TreatmentSymptom());
            config.CreateMap<User, UserDto>();

            config.CreateMap<QueryResult<Ailment>, QueryResultDto<AilmentDto, AilmentFilterDto>>();
            config.CreateMap<QueryResult<AilmentSymptom>, QueryResultDto<AilmentSymptomDto, AilmentSymptomFilterDto>>();
            config.CreateMap<QueryResult<Card>, QueryResultDto<CardDto, CardFilterDto>>();
            config.CreateMap<QueryResult<Issue>, QueryResultDto<IssueDto, IssueFilterDto>>();
            config.CreateMap<QueryResult<Permission>, QueryResultDto<PermissionDto, PermissionFilterDto>>();
            config.CreateMap<QueryResult<Role>, QueryResultDto<RoleDto, RoleFilterDto>>();
            config.CreateMap<QueryResult<Symptom>, QueryResultDto<SymptomDto, SymptomFilterDto>>();
            config.CreateMap<QueryResult<Treatment>, QueryResultDto<TreatmentDto, TreatmentFilterDto>>();
            config.CreateMap<QueryResult<TreatmentSymptom>, QueryResultDto<TreatmentSymptomDto, TreatmentSymptomFilterDto>>();
            config.CreateMap<QueryResult<User>, QueryResultDto<UserDto, UserFilterDto>>();

            /*
            config.CreateMap<ProductDto, Product>().ForMember(dest => dest.Category, opt => opt.Ignore());
            config.CreateMap<Category, CategoryDto>().ForMember(categoryDto => categoryDto.CategoryPath, opts => opts.ResolveUsing(category =>
            {
                var categoryPath = category.Name;
                while (category.Parent != null)
                {
                    categoryPath = category.Parent.Name + "/" + categoryPath;
                    category = category.Parent;
                }
                return categoryPath;
            })).ReverseMap();

            config.CreateMap<Order, OrderDto>().ReverseMap();
            config.CreateMap<OrderItem, OrderItemDto>().ReverseMap();
            config.CreateMap<Customer, CustomerDto>().ReverseMap();
            config.CreateMap<QueryResult<Product>, QueryResultDto <ProductDto, ProductFilterDto>>();
            config.CreateMap<QueryResult<Category>, QueryResultDto<CategoryDto, CategoryFilterDto>>();
            config.CreateMap<QueryResult<Order>, QueryResultDto<OrderDto, OrderFilterDto>>();
            config.CreateMap<QueryResult<OrderItem>, QueryResultDto<OrderItemDto, OrderItemFilterDto>>();
            config.CreateMap<QueryResult<Customer>, QueryResultDto<CustomerDto, CustomerFilterDto>>();
            */
        }

    }
}
