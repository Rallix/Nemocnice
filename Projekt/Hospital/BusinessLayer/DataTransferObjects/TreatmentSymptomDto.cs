﻿using System;

namespace BusinessLayer.DataTransferObjects
{
    /// <summary> A relation of <see cref="Symptom"/> discovered during a patient's <see cref="Treatment"/>. </summary>
    public class TreatmentSymptomDto : DtoBase
    {
        public SymptomDto Symptom { get; set; }
        public Guid SymptomId { get; set; }

        public TreatmentDto Treatment { get; set; }
        public Guid TreatmentId { get; set; }
    }
}
