﻿using BusinessLayer.DataTransferObjects;
using BusinessLayer.DataTransferObjects.Enums;
using System.Collections.Generic;

namespace BusinessLayer.DataTransferObjects
{

    /// <summary> A role a <see cref="User"/> can have in the system. A permission tier tied to a list of permissions. </summary>
    public class RoleDto : DtoBase
    {
        /// <summary> The permission tier of the role. </summary>
        public RoleTypes RoleType { get; set; }
        /// <summary> A list of permissions available to this specific role. </summary>
        public List<PermissionDto> Permissions { get; set; }

    }

}
