﻿using System;

namespace BusinessLayer.DataTransferObjects.Filters
{
    public class UserFilterDto : FilterDtoBase
    {
        public string Username { get; set; }
        public string UsernameLike { get; set; }
        public string BirthNumber { get; set; }
        public DateTime? BirthDateFrom { get; set; }
        public DateTime? BirthDateTo { get; set; }
        public bool? Alive { get; set; }
        public Guid? Card { get;set; }
        public Guid? Id { get; set; }

        public string FirstName { get; set; }
        public string Surname { get; set; }
        public DateTime? BirthDate { get; set; }
    }
}
