﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.DataTransferObjects.Filters
{
    public class AilmentSymptomFilterDto : FilterDtoBase
    {
        public Guid? AilmentId { get; set; }
        public Guid? SymptomId { get; set; }
        public Guid[] AnySymptom { get; set; }
    }
}
