﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.DataTransferObjects.Filters
{
    public class AilmentFilterDto : FilterDtoBase
    {
        public string Name { get; set; }

        public Guid[] AnyId { get; set; }

        public Guid? Id { get; set; }

    }
}
