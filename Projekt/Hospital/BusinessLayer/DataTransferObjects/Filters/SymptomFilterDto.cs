﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.DataTransferObjects.Filters
{
    public class SymptomFilterDto : FilterDtoBase
    {
        public string Description { get; set; }
        public string[] AnyOfDescriptions { get; set; }
        public Guid[] AnyIds { get; set; }
    }
}
