﻿using BusinessLayer.DataTransferObjects.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.DataTransferObjects.Filters
{
    public class RoleFilterDto : FilterDtoBase
    {
        public RoleTypes? RoleType { get; set; }
        public RoleTypes? MinimumRoleTypeLevel { get; set; }
        public RoleTypes? MaximumRoleTypeLevel { get; set; }
        public Guid? Id { get; set; }
    }
}
