﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.DataTransferObjects.Filters
{
    public class TreatmentSymptomFilterDto : FilterDtoBase
    {
        public Guid? SymptomId { get; set; }
        public Guid? TreatmentId { get; set; }
        public Guid[] AnySymptom { get; set; }
    }
    
}
