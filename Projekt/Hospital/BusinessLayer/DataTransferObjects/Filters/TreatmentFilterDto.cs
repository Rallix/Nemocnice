﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.DataTransferObjects.Filters
{
    public class TreatmentFilterDto : FilterDtoBase
    {
        public DateTime? MinDate { get; set; }
        public DateTime? MaxDate { get; set; }
        public string MessagePart { get; set; }
        public Guid? IssueId { get; set; }
    }
}
