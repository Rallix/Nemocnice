﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.DataTransferObjects.Filters
{
    public class IssueFilterDto : FilterDtoBase
    {
        public bool? Solved { get; set; }
        public Guid? AilmentId { get; set; }
        public DateTime? Since { get; set; }
        public DateTime? Until { get; set; }
        public Guid? Card { get; set; }
    }
}
