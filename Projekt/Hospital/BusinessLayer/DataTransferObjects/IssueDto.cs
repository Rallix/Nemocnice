﻿using System;
using System.Collections.Generic;

namespace BusinessLayer.DataTransferObjects
{

    /// <summary> An event or a problem with which the patient goes to a doctor. </summary>
    public class IssueDto : DtoBase
    {

        /// <summary> The first time when this problem was discovered. </summary>
        public DateTime Since { get; set; }
        /// <summary> The time when this problem was finished or stopped being actively treated. </summary>
        public DateTime? Until { get; set; }
        /// <summary> Is this issue already solved? </summary>
        public bool Solved { get; set; }

        /// <summary> The name of an ailment if discovered/applicable. </summary>
        public AilmentDto Ailment { get; set; }
        public Guid? AilmentId { get; set; }
        /// <summary> Treatment sessions related to this issue. </summary>
        public List<TreatmentDto> Treatments { get; set; }

        public CardDto Card { get; set; }
        public Guid CardId { get; set; }

        public override string ToString()
        {
            string dateString = !Until.HasValue
                    ? $"since {Since:d. M. yyyy}"
                    : $"{Since:d. M. yyyy} – {Until.Value:d. M. yyyy}";
            string ailment = string.IsNullOrWhiteSpace(Ailment?.ToString()) ? "❓" : Ailment.ToString();
            return $"{ailment} | {dateString}, {Treatments?.Count ?? 0} treatment session{((Treatments?.Count ?? 0) != 1 ? "s" : "")}";
        }

    }

}
