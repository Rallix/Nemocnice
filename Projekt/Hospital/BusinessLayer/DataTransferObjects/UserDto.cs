﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BusinessLayer.DataTransferObjects
{

    public class UserDto : DtoBase
    {

        /// <summary> A short unique name chosen by the user for login. </summary>
        public string Username { get; set; }

        /// <summary> A password used to log into the account. </summary>
        public string Password { get; set; }

        /// <summary> A national identification number of a citizen. </summary>
        /// <remarks> Birth date can be discerned from a birth number.
        /// Birth numbers are *not* guaranteed to be unique.</remarks>
        public string BirthNumber { get; set; }

        /// <summary> The date of birth. </summary>        
        public DateTime BirthDate { get; set; }

        /// <summary> Is the patient currently still alive? </summary>
        public bool Alive { get; set; }

        /// <summary> All health documentation about a patient. </summary>
        /// <remarks> Doctors and directors do not necessarily have their own card.</remarks>
        public CardDto Card { get; set; }
        public Guid? CardId { get; set; }

        /// <summary> The role the user has in the system. Sets appropriate permissions. </summary>
        public RoleDto Role { get; set; }
        public Guid? RoleId { get; set; }

        public string FirstName { get; set; }
        public string Surname { get; set; }

        public string FullName
        {
            get { return $"{FirstName} {Surname}"; }
        }

        public bool HasCard { get; set; }


    }

}
