﻿using System;

namespace BusinessLayer.DataTransferObjects
{
    public abstract class DtoBase
    {
        public Guid Id { get; set; }
    }
}