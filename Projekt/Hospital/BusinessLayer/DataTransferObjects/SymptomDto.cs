﻿using System;
using System.Collections.Generic;

namespace BusinessLayer.DataTransferObjects
{

    /// <summary> A symptom of an ailment. </summary>
    public class SymptomDto : DtoBase
    {
        /// <summary> A short summary of the symptom. </summary>
        public string Description { get; set; }
        public List<AilmentSymptomDto> SymptomAilments { get; set; }

        /// <summary>Returns a string that represents the current object.</summary>
        /// <returns>A string that represents the current object.</returns>
        public override string ToString()
        {
            return char.ToUpper(Description[0]) + Description.Substring(1);
        }

    }

}
