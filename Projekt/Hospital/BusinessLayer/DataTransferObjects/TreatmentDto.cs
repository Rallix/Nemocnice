﻿using BusinessLayer.DataTransferObjects;
using System;
using System.Collections.Generic;

namespace BusinessLayer.DataTransferObjects
{
    /// <summary> A single session of a doctor and a patient. </summary>
    /// <remarks> Provides information about a single visit or some important event, e.g. an operation, new discovery or death.</remarks>
    public class TreatmentDto : DtoBase
    {

        /// <summary> The date in which this treatment session occured. </summary>
        public DateTime Date { get; set; }
        /// <summary> The details of this treatment session. </summary>
        public string Message { get; set; }

        public IssueDto Issue { get; set; }
        public Guid IssueId { get; set; }

        /// <summary> A list of new symptoms discovered during this treatment session. </summary>
        public List<TreatmentSymptomDto> TreatmentSymptoms { get; set; }

    }

}
