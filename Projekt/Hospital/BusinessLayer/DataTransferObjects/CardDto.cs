﻿using System;
using System.Collections.Generic;

namespace BusinessLayer.DataTransferObjects
{

    /// <summary> All medical records of a single patient. </summary>
    public class CardDto : DtoBase
    {
        /// <summary> A list of issues (illnesses, important events) related to the patient's health. </summary>
        public List<IssueDto> Issues { get; set; }
        public UserDto User { get; set; }
        public Guid UserId { get; set; }

    }

}
