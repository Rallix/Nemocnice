﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.DataTransferObjects
{
    public class AilmentWithSymptomsDto
    {
        public AilmentDto Ailment { get; set; }
        public IEnumerable<SymptomDto> Symptoms { get; set; }
    }
}
