﻿using System;

namespace BusinessLayer.DataTransferObjects
{

    /// <summary> A relation of a <see cref="Symptom"/> and a specific <see cref="Ailment"/>. </summary>
    public class AilmentSymptomDto : DtoBase
    {
        public AilmentDto Ailment { get; set; }
        public Guid AilmentId { get; set; }

        public SymptomDto Symptom { get; set; }
        public Guid SymptomId { get; set; }

    }
}
