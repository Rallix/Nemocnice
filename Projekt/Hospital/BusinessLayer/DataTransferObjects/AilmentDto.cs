﻿using System.Collections.Generic;


namespace BusinessLayer.DataTransferObjects
{

    /// <summary> An illness defined by its name and a set of symptoms. </summary>
    public class AilmentDto : DtoBase
    {

        /// <summary> A unique name of an illness or a certain condition. </summary>
        public string Name { get; set; }

        /// <summary> A list of symptoms this illness exhibits. </summary>
        public List<AilmentSymptomDto> SymptomAilments { get; set; }

        public override string ToString() {
            return Name;
        }
    }

}
