﻿using System;

namespace BusinessLayer.DataTransferObjects
{

    /// <summary> The actual permissions for specific actions available to concrete roles. </summary>
    /// <remarks> The <see cref="Entities.Role.RoleTypes"/> should be understood as subsets
    /// – e.g. it's not possible to have a permission for patients only without the director having it as well.</remarks>
    public class PermissionDto : DtoBase
    {
        /// <summary> An action a user can perform. </summary>
        public string Action { get; set; }
        /// <summary> The minimal role with an access to the action. </summary>
        public RoleDto Role { get; set; }
        public Guid RoleId { get; set; }

        public override string ToString() {
            return $"{Role.RoleType}: {Action}";
        }
    }

}
