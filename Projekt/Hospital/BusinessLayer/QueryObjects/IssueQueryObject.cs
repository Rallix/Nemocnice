﻿using AutoMapper;
using BusinessLayer.DataTransferObjects;
using BusinessLayer.DataTransferObjects.Filters;
using BusinessLayer.QueryObjects.Common;
using DataAccessLayer.Entities;
using Hospital.Infrastructure.Query;
using Hospital.Infrastructure.Query.Predicates;
using Hospital.Infrastructure.Query.Predicates.Operators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.QueryObjects
{
    public class IssueQueryObject : QueryObjectBase<IssueDto, Issue, IssueFilterDto, IQuery<Issue>>
    {
        public IssueQueryObject(IMapper mapper, IQuery<Issue> query) : base(mapper, query) { }

        IPredicate FilterAilment(IssueFilterDto filter)
        {
            if (filter.AilmentId == null) return null;
            return new SimplePredicate(nameof(Issue.AilmentId), ValueComparingOperator.Equal, filter.AilmentId);
        }
        IPredicate FilterSolved(IssueFilterDto filter)
        {
            if (filter.Solved == null) return null;
            return new SimplePredicate(nameof(Issue.Solved), ValueComparingOperator.Equal, filter.Solved.Value);
        }
        IPredicate FilterSince(IssueFilterDto filter)
        {
            if (filter.Since == null) return null;
            return new SimplePredicate(nameof(Issue.Since), ValueComparingOperator.Equal, filter.Since);
        }

        IPredicate FilterUntil(IssueFilterDto filter)
        {
            if (filter.Until == null) return null;
            return new SimplePredicate(nameof(Issue.Until), ValueComparingOperator.Equal, filter.Until);
        }

        IPredicate FilterCard(IssueFilterDto filter)
        {
            if (filter.Card == null) return null;
            return new SimplePredicate(nameof(Issue.CardId), ValueComparingOperator.Equal, filter.Card.Value);
        }
    }
}