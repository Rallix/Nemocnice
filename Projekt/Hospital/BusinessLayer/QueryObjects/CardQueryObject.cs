﻿using AutoMapper;
using BusinessLayer.DataTransferObjects;
using BusinessLayer.DataTransferObjects.Filters;
using BusinessLayer.QueryObjects.Common;
using DataAccessLayer.Entities;
using Hospital.Infrastructure.Query;
using Hospital.Infrastructure.Query.Predicates;
using Hospital.Infrastructure.Query.Predicates.Operators;

namespace BusinessLayer.QueryObjects
{
    public class CardQueryObject : QueryObjectBase<CardDto, Card, CardFilterDto, IQuery<Card>>
    {
        public CardQueryObject(IMapper mapper, IQuery<Card> query) : base(mapper, query) { }

        IPredicate FilterUser(CardFilterDto filter)
        {
            if (filter.UserId == null) return null;
            return new SimplePredicate(nameof(Card.UserId), ValueComparingOperator.Equal, filter.UserId);

        }
    }
}

