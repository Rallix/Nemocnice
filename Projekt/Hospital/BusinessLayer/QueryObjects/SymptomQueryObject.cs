﻿using AutoMapper;
using BusinessLayer.DataTransferObjects;
using BusinessLayer.DataTransferObjects.Filters;
using BusinessLayer.QueryObjects.Common;
using DataAccessLayer.Entities;
using Hospital.Infrastructure.Query;
using Hospital.Infrastructure.Query.Predicates;
using Hospital.Infrastructure.Query.Predicates.Operators;
using System.Collections.Generic;
using System.Linq;

namespace BusinessLayer.QueryObjects
{
    public class SymptomQueryObject : QueryObjectBase<SymptomDto, Symptom, SymptomFilterDto, IQuery<Symptom>>
    {
        public SymptomQueryObject(IMapper mapper, IQuery<Symptom> query) : base(mapper, query) { }

        IPredicate FilterDescription(SymptomFilterDto filter)
        {
            if (filter.Description == null) return null;
            return new SimplePredicate(nameof(Symptom.Description), ValueComparingOperator.Equal, filter.Description);
        }

        IPredicate FilterAnyDescription(SymptomFilterDto filter)
        {
            if (filter.AnyOfDescriptions == null || filter.AnyOfDescriptions.Length == 0) return null;
            var filterList = filter.AnyOfDescriptions.Select(desc =>(IPredicate) new SimplePredicate(nameof(Symptom.Description), ValueComparingOperator.Equal, desc)).ToList();
            return new CompositePredicate(filterList, LogicalOperator.OR);
        }
        IPredicate FilterAnyId(SymptomFilterDto filter)
        {
            if (filter.AnyIds == null || filter.AnyIds.Length == 0) return null;
            var filterList = filter.AnyIds.Select(id => (IPredicate)new SimplePredicate(nameof(Symptom.Id), ValueComparingOperator.Equal, id)).ToList();
            return new CompositePredicate(filterList, LogicalOperator.OR);
        }

    }
}
