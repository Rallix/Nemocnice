﻿using AutoMapper;
using BusinessLayer.DataTransferObjects;
using BusinessLayer.DataTransferObjects.Filters;
using BusinessLayer.QueryObjects.Common;
using DataAccessLayer.Entities;
using Hospital.Infrastructure.Query;
using Hospital.Infrastructure.Query.Predicates;
using Hospital.Infrastructure.Query.Predicates.Operators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.QueryObjects
{
    public class PermissionQueryObject : QueryObjectBase<PermissionDto, Permission, PermissionFilterDto, IQuery<Permission>>
    {
        public PermissionQueryObject(IMapper mapper, IQuery<Permission> query) : base(mapper, query) { }

        IPredicate FilterAction(PermissionFilterDto filter)
        {
            if (filter.Action == null) return null;
            return new SimplePredicate(nameof(Permission.Action), ValueComparingOperator.Equal, filter.Action);
        }

        IPredicate FilterRole(PermissionFilterDto filter)
        {
            if (filter.RoleId == null) return null;
            return new SimplePredicate(nameof(Permission.RoleId), ValueComparingOperator.Equal, filter.RoleId);
        }

    }
}
