﻿using AutoMapper;
using BusinessLayer.DataTransferObjects;
using BusinessLayer.DataTransferObjects.Filters;
using BusinessLayer.QueryObjects.Common;
using DataAccessLayer.Entities;
using Hospital.Infrastructure.Query;
using Hospital.Infrastructure.Query.Predicates;
using Hospital.Infrastructure.Query.Predicates.Operators;

namespace BusinessLayer.QueryObjects
{
    public class UserQueryObject : QueryObjectBase<UserDto, User, UserFilterDto, IQuery<User>>
    {
        public UserQueryObject(IMapper mapper, IQuery<User> query) : base(mapper, query) { }
        IPredicate FilterBirthDateFrom(UserFilterDto filter)
        {
            if (filter.BirthDateFrom == null) return null;
            return new SimplePredicate(nameof(User.BirthDate), ValueComparingOperator.GreaterThanOrEqual, filter.BirthDateFrom);
        }

        IPredicate FilterBirthDateTo(UserFilterDto filter)
        {
            if (filter.BirthDateTo == null) return null;
            return new SimplePredicate(nameof(User.BirthDate), ValueComparingOperator.LessThanOrEqual, filter.BirthDateTo);
        }

        IPredicate FilterAlive(UserFilterDto filter)
        {
            if (filter.Alive == null) return null;
            return new SimplePredicate(nameof(User.Alive), ValueComparingOperator.Equal, filter.Alive.Value);
        }

        IPredicate FilterBirthNumber(UserFilterDto filter)
        {
            if (filter.BirthNumber == null) return null;
            return new SimplePredicate(nameof(User.BirthNumber), ValueComparingOperator.Equal, filter.BirthNumber);
        }

        IPredicate FilterCard(UserFilterDto filter)
        {
            if (filter.Card == null) return null;
            return new SimplePredicate(nameof(User.CardId), ValueComparingOperator.Equal, filter.Card.Value);
        }

        IPredicate FilterId(UserFilterDto filter)
        {
            if (filter.Id == null) return null;
            return new SimplePredicate(nameof(User.Id), ValueComparingOperator.Equal, filter.Id.Value);
        }

        IPredicate FilterUsername(UserFilterDto filter)
        {
            if (filter.Username == null) return null;
            return new SimplePredicate(nameof(User.Username), ValueComparingOperator.Equal, filter.Username);
        }
        IPredicate FilterUsernameLike(UserFilterDto filter)
        {
            if (filter.UsernameLike == null) return null;
            return new SimplePredicate(nameof(User.Username), ValueComparingOperator.StringContains, filter.UsernameLike);
        }

        IPredicate FilterFirstname(UserFilterDto filter)
        {
            if (filter.FirstName == null) return null;
            return new SimplePredicate(nameof(User.FirstName), ValueComparingOperator.StringContains, filter.FirstName);
        }

        IPredicate FilterSurname(UserFilterDto filter)
        {
            if (filter.Surname == null) return null;
            return new SimplePredicate(nameof(User.Surname), ValueComparingOperator.StringContains, filter.Surname);
        }

        IPredicate FilterBirthDate(UserFilterDto filter)
        {
            if (filter.BirthDate == null) return null;
            return new SimplePredicate(nameof(User.BirthDate), ValueComparingOperator.Equal, filter.BirthDate);
        }

    }
}
