﻿using AutoMapper;
using BusinessLayer.DataTransferObjects;
using BusinessLayer.DataTransferObjects.Filters;
using BusinessLayer.QueryObjects.Common;
using DataAccessLayer.Entities;
using Hospital.Infrastructure.Query;
using Hospital.Infrastructure.Query.Predicates;
using Hospital.Infrastructure.Query.Predicates.Operators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.QueryObjects
{
    public class AilmentSymptomQueryObject : QueryObjectBase<AilmentSymptomDto, AilmentSymptom, AilmentSymptomFilterDto, IQuery<AilmentSymptom>>
    {
        public AilmentSymptomQueryObject(IMapper mapper, IQuery<AilmentSymptom> query) : base(mapper, query) { }

        IPredicate FilterSymptomId(AilmentSymptomFilterDto filter)
        {
            if (filter.SymptomId == null) return null;
            return new SimplePredicate(nameof(AilmentSymptom.SymptomId), ValueComparingOperator.Equal, filter.SymptomId.Value);
        }

        IPredicate FilterAilmentId(AilmentSymptomFilterDto filter)
        {
            return filter.AilmentId == null 
                    ? null 
                    : new SimplePredicate(nameof(AilmentSymptom.AilmentId), ValueComparingOperator.Equal, filter.AilmentId.Value);
        }

        IPredicate FilterAnySymptomId(AilmentSymptomFilterDto filter)
        {
            if (filter.AnySymptom == null || filter.AnySymptom.Length == 0) return null;

            List<IPredicate> simpleFilters = filter.AnySymptom.Select(sid => (IPredicate)new SimplePredicate(nameof(AilmentSymptom.SymptomId), ValueComparingOperator.Equal, sid)).ToList();
            return new CompositePredicate(simpleFilters, LogicalOperator.OR);
        }

    }
}
