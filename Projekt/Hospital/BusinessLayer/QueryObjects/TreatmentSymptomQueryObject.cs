﻿using AutoMapper;
using BusinessLayer.DataTransferObjects;
using BusinessLayer.DataTransferObjects.Filters;
using BusinessLayer.QueryObjects.Common;
using DataAccessLayer.Entities;
using Hospital.Infrastructure.Query;
using Hospital.Infrastructure.Query.Predicates;
using Hospital.Infrastructure.Query.Predicates.Operators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.QueryObjects
{
    public class TreatmentSymptomQueryObject : QueryObjectBase<TreatmentSymptomDto, TreatmentSymptom, TreatmentSymptomFilterDto, IQuery<TreatmentSymptom>>
    {
        public TreatmentSymptomQueryObject(IMapper mapper, IQuery<TreatmentSymptom> query) : base(mapper, query) { }

        IPredicate FilterSymptomId(TreatmentSymptomFilterDto filter)
        {
            if (filter.SymptomId == null) return null;
            return new SimplePredicate(nameof(Treatment.IssueId), ValueComparingOperator.Equal, filter.SymptomId);
        }
        IPredicate FilterTreatmentId(TreatmentSymptomFilterDto filter)
        {
            if (filter.TreatmentId == null) return null;
            return new SimplePredicate(nameof(Treatment.IssueId), ValueComparingOperator.Equal, filter.TreatmentId);
        }
       // todo : anysymptom filter

    }
}
