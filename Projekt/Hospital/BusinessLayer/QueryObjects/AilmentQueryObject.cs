﻿using AutoMapper;
using BusinessLayer.DataTransferObjects;
using BusinessLayer.DataTransferObjects.Filters;
using BusinessLayer.QueryObjects.Common;
using DataAccessLayer.Entities;
using Hospital.Infrastructure.Query;
using Hospital.Infrastructure.Query.Predicates;
using Hospital.Infrastructure.Query.Predicates.Operators;
using System.Collections.Generic;
using System.Linq;

namespace BusinessLayer.QueryObjects
{
    public class AilmentQueryObject : QueryObjectBase<AilmentDto, Ailment, AilmentFilterDto, IQuery<Ailment>>
    {
        public AilmentQueryObject(IMapper mapper, IQuery<Ailment> query) : base(mapper, query) { }

        IPredicate FilterName(AilmentFilterDto filter)
        {
            return filter.Name == null 
                    ? null 
                    : new SimplePredicate(nameof(Ailment.Name), ValueComparingOperator.StringContains, filter.Name);
        }

        IPredicate FilterId(AilmentFilterDto filter)
        {
            return filter.Id == null
                    ? null
                    : new SimplePredicate(nameof(Ailment.Id), ValueComparingOperator.Equal, filter.Id);
        }

        IPredicate FilterIds(AilmentFilterDto filter)
        {
            if (filter.AnyId == null || filter.AnyId.Length == 0) return null;
            List<IPredicate> simpleFilters = filter.AnyId.Select(id => (IPredicate)new SimplePredicate(nameof(Ailment.Id), ValueComparingOperator.Equal, id)).ToList();
            return new CompositePredicate(simpleFilters, LogicalOperator.OR);
        }

    }
}
