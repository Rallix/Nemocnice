﻿using AutoMapper;
using BusinessLayer.DataTransferObjects;
using BusinessLayer.DataTransferObjects.Filters;
using BusinessLayer.QueryObjects.Common;
using DataAccessLayer.Entities;
using Hospital.Infrastructure.Query;
using Hospital.Infrastructure.Query.Predicates;
using Hospital.Infrastructure.Query.Predicates.Operators;
using System.Collections.Generic;
using System.Linq;

namespace BusinessLayer.QueryObjects
{
    public class TreatmentQueryObject : QueryObjectBase<TreatmentDto, Treatment, TreatmentFilterDto, IQuery<Treatment>>
    {
        public TreatmentQueryObject(IMapper mapper, IQuery<Treatment> query) : base(mapper, query) { }

        IPredicate FilterMessage(TreatmentFilterDto filter)
        {
            if (string.IsNullOrWhiteSpace(filter.MessagePart)) return null;
            return new SimplePredicate(nameof(Treatment.Message), ValueComparingOperator.StringContains, filter.MessagePart);
        }
        IPredicate FilterMinDate(TreatmentFilterDto filter)
        {
            if (filter.MinDate == null) return null;
            return new SimplePredicate(nameof(Treatment.Date), ValueComparingOperator.GreaterThanOrEqual, filter.MinDate);
        }
        IPredicate FilterMaxDate(TreatmentFilterDto filter)
        {
            if (filter.MaxDate == null) return null;
            return new SimplePredicate(nameof(Treatment.Date), ValueComparingOperator.LessThanOrEqual, filter.MaxDate);
        }
        IPredicate FilterIssueId(TreatmentFilterDto filter)
        {
            if (filter.IssueId == null) return null;
            return new SimplePredicate(nameof(Treatment.IssueId), ValueComparingOperator.Equal, filter.IssueId);
        }

    }
}
