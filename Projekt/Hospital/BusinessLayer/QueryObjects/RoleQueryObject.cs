﻿using AutoMapper;
using BusinessLayer.DataTransferObjects;
using BusinessLayer.DataTransferObjects.Enums;
using BusinessLayer.DataTransferObjects.Filters;
using BusinessLayer.QueryObjects.Common;
using DataAccessLayer.Entities;
using Hospital.Infrastructure.Query;
using Hospital.Infrastructure.Query.Predicates;
using Hospital.Infrastructure.Query.Predicates.Operators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using DAlEnums = DataAccessLayer.Enums;

namespace BusinessLayer.QueryObjects
{
    public class RoleQueryObject : QueryObjectBase<RoleDto, Role, RoleFilterDto, IQuery<Role>>
    {
        public RoleQueryObject(IMapper mapper, IQuery<Role> query) : base(mapper, query) { }

        public DAlEnums.RoleTypes Convert(RoleTypes x)
        {
            return (DAlEnums.RoleTypes)Enum.Parse(typeof(DAlEnums.RoleTypes), x.ToString());
        }
        
        IPredicate FilterRoleType(RoleFilterDto filter)
        {
            
            if (filter.RoleType == null) return null;
            return new SimplePredicate(nameof(Role.RoleType), ValueComparingOperator.Equal, Convert(filter.RoleType.Value));
        }

        IPredicate FilterMinRoleLevel(RoleFilterDto filter)
        {
            if (filter.MinimumRoleTypeLevel == null) return null;
            return new SimplePredicate(nameof(Role.RoleType), ValueComparingOperator.GreaterThanOrEqual, Convert(filter.MinimumRoleTypeLevel.Value));
        }

        IPredicate FilterMaxRoleLevel(RoleFilterDto filter)
        {
            if (filter.MaximumRoleTypeLevel == null) return null;
            return new SimplePredicate(nameof(Role.RoleType), ValueComparingOperator.LessThanOrEqual, Convert(filter.MaximumRoleTypeLevel.Value));
        }

        IPredicate FilterId(RoleFilterDto filter)
        {
            if (filter.Id == null) return null;
            return new SimplePredicate(nameof(Role.Id), ValueComparingOperator.Equal, filter.Id.Value);
        }
    }
}
