﻿using BusinessLayer.DataTransferObjects;
using System.Threading.Tasks;
using Hospital.Infrastructure.Query;
using Hospital.Infrastructure;
using AutoMapper;
using System;
using Hospital.Infrastructure.Query.Predicates;
using System.Linq;
using Hospital.Infrastructure.Query.Predicates.Operators;
using System.Collections.Generic;
using System.Reflection;

namespace BusinessLayer.QueryObjects.Common
{
    public abstract class QueryObjectBase<TDto, TEntity, TFilter, TQuery>
         where TFilter : FilterDtoBase
         where TQuery : IQuery<TEntity>
         where TEntity : class, IEntity, new()
    {
        readonly IMapper mapper;

        protected readonly IQuery<TEntity> Query;

        List<Delegate> FilterFuncs;

        protected QueryObjectBase(IMapper mapper, TQuery query, params Func<TFilter, IPredicate>[] filterFuncs)
        {
            this.mapper = mapper;
            this.Query = query;
            this.FilterFuncs = GetType().GetMethods(BindingFlags.NonPublic | BindingFlags.Instance).Where(m => m.Name.StartsWith("Filter"))
                .Select(m => Delegate.CreateDelegate(typeof(Func<TFilter, IPredicate>), this, m)).ToList();

        }

        IQuery<TEntity> ApplyWhereClause(IQuery<TEntity> query, TFilter filter)
        {
            List<IPredicate> predicates = FilterFuncs.Select(func => (IPredicate)func.DynamicInvoke(filter)).Where(predicate => predicate != null).ToList();
            if (predicates.Count == 1)
            {
                query.Where(predicates.First());
            }
            else if (predicates.Count >= 2)
            {
                query.Where(new CompositePredicate(predicates, LogicalOperator.AND));

            }
            return query;
        }

        public virtual async Task<QueryResultDto<TDto, TFilter>> ExecuteQuery(TFilter filter)
        {
            IQuery<TEntity> query = ApplyWhereClause(Query, filter);
            if (!string.IsNullOrWhiteSpace(filter.SortCriteria))
            {
                query = query.SortBy(filter.SortCriteria, filter.SortAscending);
            }
            if (filter.RequestedPageNumber.HasValue)
            {
                query = query.Page(filter.RequestedPageNumber.Value, filter.PageSize);
            }
            QueryResult<TEntity> queryResult = await query.ExecuteAsync(); // TODO: 'POST' takes too long

            var queryResultDto = mapper.Map<QueryResultDto<TDto, TFilter>>(queryResult);
            queryResultDto.Filter = filter;
            return queryResultDto;
        }
    }
}