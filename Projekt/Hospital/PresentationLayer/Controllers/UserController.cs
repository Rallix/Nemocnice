using BusinessLayer.DataTransferObjects;
using BusinessLayer.DataTransferObjects.Enums;
using BusinessLayer.DataTransferObjects.Filters;
using BusinessLayer.Services.Users;
using PagedList;
using PresentationLayer.Helpers;
using PresentationLayer.Models;
using PresentationLayer.Models.Base;
using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace PresentationLayer.Controllers
{

    public class UserController : BaseController
    {

        // GET: User
        private readonly int PageSize = 20;
        //private readonly string filterSessionKey = "filter";

        [PermissionAuthorize(Permission.viewUsers)]
        public async Task<ActionResult> Index(int page = 1)
        {
            UserListModel model = await GetUserListModel( /*Session[filterSessionKey] as UserFilterDto*/ null, page);
            return View("UserListView", model);
        }

        [HttpGet]
        [PermissionAuthorize(Permission.viewUsers)]
        public async Task<ActionResult> FilterList(UserListModel filterModel, int page = 1)
        {
            //Session[filterSessionKey] = filter;
            var filter = new UserFilterDto
            {
                    FirstName = filterModel.FirstNameFilter,
                    Surname = filterModel.SurnameFilter,
                    BirthDate = filterModel.BirthDateFilter,
                    BirthNumber = filterModel.BirthNumFilter
            };
            UserListModel model = await GetUserListModel(filter, page);
            return View("UserListView", model);
        }

        [PermissionAuthorize(Permission.viewUsers)]
        public async Task<UserListModel> GetUserListModel(UserFilterDto filter = null, int page = 1)
        {
            filter = filter ?? new UserFilterDto();
            filter.PageSize = PageSize;
            filter.RequestedPageNumber = page;
            var result = await UserFacade.GetUsersAsync(filter);
            return new UserListModel
            {
                    Users = new StaticPagedList<UserDto>(result.Items, result.RequestedPageNumber ?? 1, filter.PageSize, (int) result.TotalItemsCount),
                    FirstNameFilter = filter.FirstName,
                    SurnameFilter = filter.Surname,
                    BirthDateFilter = filter.BirthDate,
                    BirthNumFilter = filter.BirthNumber
            };
        }

        public ActionResult Register()
        {
            return View("UserRegisterView");
        }

        [HttpPost]
        public async Task<ActionResult> Register(UserRegisterModel info)
        {
            if (await UserFacade.UserExists(info.Username))
            {
                ModelState.AddModelError("Username", $"User {info.Username} already exists. ");
            }
            if (!ModelState.IsValid)
            {
                return View("UserRegisterView", info);
            }

            var role = await UserFacade.GetOrCreateRole(info.Role);
            Guid? result = await UserFacade.RegisterUser(
                                                         new UserDto
                                                         {
                                                                 BirthDate = info.BirthDate,
                                                                 BirthNumber = info.BirthNumber,
                                                                 Username = info.Username,
                                                                 Password = info.Password,
                                                                 Alive = true,
                                                                 RoleId = role.Id,
                                                                 FirstName = info.FirstName,
                                                                 Surname = info.Surname
                                                         });

            return await Login(new LoginModel {Username = info.Username, Password = info.Password}, null);
        }

        [HttpPost]
        [PermissionAuthorize(RoleTypes.Doctor)]
        public async Task<ActionResult> RegisterPatient(PatientRegisterModel model, string nonGeneratedUserName)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            string username = model.Username ?? model.Surname + model.BirthNumber.Split('/').First();
            const string symbols = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

            if (await UserFacade.UserExists(username))
            {
                string newUsername = await UserFacade.GenerateUsername(username, symbols);
                if (await UserFacade.UserExists(newUsername))
                {
                    ModelState.AddModelError("Username", $"Failed to generate username, User {username} and its variations already exist.");
                    return View(model);
                }
                username = newUsername;
            }
            RoleDto role = await UserFacade.GetOrCreateRole(RoleTypes.Patient);
            var random = new Random();
            string password = new string(Enumerable.Repeat(symbols, random.Next(8, 12))
                                                   .Select(s => s[random.Next(s.Length)]).ToArray());

            Guid? result = await UserFacade.RegisterUser(
                                                         new UserDto
                                                         {
                                                                 BirthDate = model.BirthDate,
                                                                 BirthNumber = model.BirthNumber,
                                                                 Username = username,
                                                                 Password = password,
                                                                 Alive = true,
                                                                 RoleId = role.Id,
                                                                 FirstName = model.FirstName,
                                                                 Surname = model.Surname
                                                         });

            try
            {
                SendEmail(
                          model.Email,
                          "Hospital account registration details",
                          $"Username: {username}\nPassword: {password}"
                         );
            }
            catch
            {
                // nejake info o selhani ci neco
            }

            if (HasPermission(Permission.viewUsers))
            {
               
                var userResult = await UserFacade.GetUsersAsync(new UserFilterDto {Username = username});
                if (userResult.Items.Any())
                {
                    var returnModel = new UserDetailModel {UserDetail = userResult.Items.First()};
                    returnModel.UserDetail.HasCard = await UserFacade.HasCard(returnModel.UserDetail.Id);
                    return View("UserDetailView", returnModel);
                }                
            }
            return RedirectToAction("Index", "Home");
        }

        [PermissionAuthorize(RoleTypes.Doctor)]
        public ActionResult RegisterPatient()
        {
            return View();
        }

        [Route("User/{username}/Detail")]
        [PermissionAuthorize("username", Permission.viewUsers)]
        public async Task<ActionResult> Detail(string username)
        {
            UserDetailModel model;
            if (username == null)
            {
                model = new UserDetailModel {Alert = new AlertMessage("User not specified.", AlertMessage.MessageType.warning)};
            }
            else
            {
                var result = await UserFacade.GetUsersAsync(new UserFilterDto {Username = username});

                if (result.Items.Any())
                {
                    model = new UserDetailModel {UserDetail = result.Items.First()};
                    model.UserDetail.HasCard = await UserFacade.HasCard(model.UserDetail.Id);
                }
                else
                {
                    model = new UserDetailModel {Alert = new AlertMessage("User doesn't exist.", AlertMessage.MessageType.danger)};
                }
            }
            return View("UserDetailView", model);
        }


        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Login(LoginModel model, string returnUrl)
        {
            (bool success, string roles) = await UserFacade.Login(model.Username, model.Password);
            if (success)
            {
                //FormsAuthentication.SetAuthCookie(model.Username, false);

                var authTicket = new FormsAuthenticationTicket(1, model.Username, DateTime.Now,
                                                               DateTime.Now.AddMinutes(30), false, roles);
                string encryptedTicket = FormsAuthentication.Encrypt(authTicket);
                var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                HttpContext.Response.Cookies.Add(authCookie);

                string decodedUrl = string.Empty;
                if (!string.IsNullOrEmpty(returnUrl))
                {
                    decodedUrl = Server.UrlDecode(returnUrl);
                }

                if (Url.IsLocalUrl(decodedUrl))
                {
                    return Redirect(decodedUrl);
                }
                return RedirectToAction("Index", "Home");
            }
            ModelState.AddModelError("", "Wrong username or password!");
            return View();
        }

        [PermissionAuthorize("username", Permission.editCards)]
        public async Task<ActionResult> DeclareDead(string username)
        {
            var userResult = await UserFacade.GetUsersAsync(new UserFilterDto {Username = username});
            if (userResult.Items.Any())
            {
                await UserFacade.DeclareDead(userResult.Items.First().Id);
            }
            return RedirectToAction("Detail", new {username});
        }


        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

        [HttpPost, Route("User/{username}/Edit")]
        public async Task<ActionResult> Edit(string username, UserDetailModel model)
        {
            var returnModel = new UserDetailModel();
            if (string.IsNullOrWhiteSpace(model.UserDetail.FirstName) || string.IsNullOrWhiteSpace(model.UserDetail.Surname))
            {
                string message = "The name can't be blank.";
                ModelState.AddModelError(nameof(model.UserDetail.FirstName), message);
                returnModel.Alert = new AlertMessage(message, AlertMessage.MessageType.warning);
            }
            if (model.UserDetail.BirthDate.Year < 1754) // SQL Error
            {
                string message = "Our hospital does not treat people born more than two centuries ago.";
                ModelState.AddModelError(nameof(model.UserDetail.BirthDate), message);

                if (!string.IsNullOrWhiteSpace(returnModel.Alert?.Message)) returnModel.Alert.Message += $" \n{message}";
                else returnModel.Alert = new AlertMessage(message, AlertMessage.MessageType.warning);
            }

            var userResult = await UserFacade.GetUsersAsync(new UserFilterDto {Username = username});

            if (userResult.Items.Any())
            {
                UserDto userDto = userResult.Items.First();

                if (!ModelState.IsValid)
                {
                    returnModel.UserDetail = userDto;
                    returnModel.UserDetail.HasCard = await UserFacade.HasCard(userDto.Id);
                    return View("UserDetailView", returnModel);
                }

                userDto.FirstName = model.UserDetail.FirstName;
                userDto.Surname = model.UserDetail.Surname;
                userDto.BirthDate = model.UserDetail.BirthDate;

                await UserFacade.DeleteCard(userDto.Id); // prevent foreign key errors
                await UserFacade.UpdateUser(userDto);
            }
            return RedirectToAction("Detail", new {username});
        }

    }

}
