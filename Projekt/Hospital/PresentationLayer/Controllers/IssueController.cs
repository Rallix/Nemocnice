﻿using BusinessLayer.DataTransferObjects;
using BusinessLayer.DataTransferObjects.Filters;
using BusinessLayer.Facades;
using BusinessLayer.Services.Users;
using PresentationLayer.Helpers;
using PresentationLayer.Models;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace PresentationLayer.Controllers
{

    public class IssueController : BaseController
    {

        public AilmentFacade AilmentFacade { get; set; }
        public IssueFacade IssueFacade { get; set; }

        [Route("User/{username}/Issues")]
        [PermissionAuthorize("username", Permission.accessCards)]
        public async Task<ActionResult> Index(Guid id)
        {
            var model = new IssueDetailModel
            {
                    Issue = await IssueFacade.GetIssueAsync(id),
                    Treatments = await IssueFacade.GetTreatmentsOfIssue(id),
                    DiscoveredSymptoms = await IssueFacade.GetSymptomsOfTreatments(id),
                    AllSymptoms = await IssueFacade.GetSymptomsOfIssue(id)
            };
            var ailments = await AilmentFacade.GetAilmentsWhichHaveSymptomsAsync(model.AllSymptoms.Select(s => s.Description));
            Guid? chosenAilment = model.Issue.AilmentId;

            model.ApplicableAilments = ailments;
            model.ChosenAilment = chosenAilment;

            return View("IssueDetailView", model);
        }


        [Route("User/{username}/Issues/Create")]
        [PermissionAuthorize(Permission.editCards, Permission.accessCards)]
        public async Task<ActionResult> CreateIssue(string username)
        {
            /*
            var userResult = (await UserFacade.GetUsers(new UserFilterDto { Username = username })).Items.First();
            var card = await UserFacade.GetUserCard(userResult.Id);
            */
            // var symptoms = (await AilmentFacade.GetSymptomsAsync(new SymptomFilterDto())).Items;
            var ailments = (await AilmentFacade.GetAilmentsAsync(new AilmentFilterDto())).Items;
            return View("IssueCreateView", new IssueAddModel
            {
                    SymptomFilterActionName = nameof(CreateWithSymptomFilter),
                    Symptoms = await GetAllSymptoms(1),
                    Ailments = ailments,
                    Date = DateTime.Now
            });
        }

        [Route("User/{username}/Issues/CreateTreatment")]
        [PermissionAuthorize(Permission.editCards, Permission.accessCards)]
        public async Task<ActionResult> CreateTreatment(string username, Guid issue)
        {
            var symptoms = (await AilmentFacade.GetSymptomsAsync(new SymptomFilterDto())).Items;
            return View("TreatmentCreateView", new TreatmentAddModel
            {
                    SymptomFilterActionName = nameof(CreateWithSymptomFilter),
                    Symptoms = await GetAllSymptoms(1),
                    Date = DateTime.Now,
                    IssueId = issue
            });
        }

        [Route("User/{username}/Issues/CreateWithSymptomFilter")]
        [PermissionAuthorize(Permission.editCards, Permission.accessCards)]
        public async Task<ActionResult> CreateWithSymptomFilter(string username, IssueAddModel model, int symptomPage = 1)
        {
            model.Symptoms = await GetAllSymptoms(symptomPage);
            model.Ailments = (await AilmentFacade.GetAilmentsAsync(new AilmentFilterDto())).Items;
            if (model.IssueId != null)
            {
                return View("TreatmentCreateView", model);
            }
            return View("IssueCreateView", model);
        }

        [HttpPost, Route("User/{username}/Issues/Create")]
        [PermissionAuthorize(Permission.editCards, Permission.accessCards)]
        public async Task<ActionResult> CreateIssue(string username, IssueAddModel model)
        {
            if (string.IsNullOrWhiteSpace(model.Message))
            {
                ModelState.AddModelError(nameof(model.Message), "The name can't be blank.");
            }
            if (!ModelState.IsValid)
            {
                var ailments = (await AilmentFacade.GetAilmentsAsync(new AilmentFilterDto())).Items;
                return View("IssueCreateView", new IssueAddModel
                {
                        SymptomFilterActionName = nameof(CreateWithSymptomFilter),
                        Symptoms = await GetAllSymptoms(1),
                        Ailments = ailments,
                        ChosenAilment = model.ChosenAilment,
                        Date = model.Date,
                        Message = model.Message
                });
            }

            var userResult = (await UserFacade.GetUsersAsync(new UserFilterDto {Username = username})).Items.First();
            var card = await UserFacade.GetUserCard(userResult.Id);
            var issueId = await IssueFacade.CreateIssue(
                                                        new IssueDto
                                                        {
                                                                Since = model.Date,
                                                                CardId = card.Id,
                                                                Solved = false,
                                                                AilmentId = model.ChosenAilment
                                                        });

            var treatmentId = await IssueFacade.AddTreatment(
                                                             new TreatmentDto
                                                             {
                                                                     IssueId = issueId,
                                                                     Message = model.Message,
                                                                     Date = model.Date
                                                             });
            await IssueFacade.AddSymptomsToTreatment(treatmentId, model.SymptomGuidsFilter);

            return RedirectToAction("Index", "Card", new {username = userResult.Username});
        }

        [HttpPost, Route("User/{username}/Issues/CreateTreatment")]
        [PermissionAuthorize(Permission.editCards, Permission.accessCards)]
        public async Task<ActionResult> CreateTreatment(string username, TreatmentAddModel model)
        {
            if (string.IsNullOrWhiteSpace(model.Message))
            {
                ModelState.AddModelError(nameof(model.Message), "The name can't be blank.");
            }
            if (!ModelState.IsValid)
            {
                var returnModel = new TreatmentAddModel
                {
                        // refresh filter
                        SymptomFilterActionName = nameof(CreateWithSymptomFilter),
                        Symptoms = await GetAllSymptoms(1),
                        Date = model.Date,
                        IssueId = model.IssueId
                };
                return View("TreatmentCreateView", returnModel);
            }

            if (model.IssueId != null)
            {
                Guid issueId = model.IssueId.Value;

                Guid treatmentId = await IssueFacade.AddTreatment(new TreatmentDto
                {
                        IssueId = issueId,
                        Message = model.Message,
                        Date = model.Date
                });
                await IssueFacade.AddSymptomsToTreatment(treatmentId, model.SymptomGuidsFilter);
                return RedirectToAction("Index", "Issue", new {id = model.IssueId});
            }
            else
            {
                // Error
                return RedirectToAction("Index", "Card", new {username});
            }
        }

        [HttpPost, Route("User/{username}/Issues/Resolve")]
        [PermissionAuthorize(Permission.editCards, Permission.accessCards)]
        public async Task<ActionResult> Resolve(string username, Guid id)
        {
            IssueDto issue = await IssueFacade.GetIssueAsync(id);
            issue.Solved = true;
            issue.Until = DateTime.Now;

            await IssueFacade.EditIssueAsync(issue);
            return RedirectToAction("Index", "Card", new {username});
        }

        [HttpPost, Route("User/{username}/Issues/ChangeAilment")]
        [PermissionAuthorize(Permission.editCards, Permission.accessCards)]
        public async Task<ActionResult> ChangeAilment(string username, Guid issue, Guid? ailment)
        {
            IssueDto issueDto = await IssueFacade.GetIssueAsync(issue);                    
            issueDto.AilmentId = ailment;

            await IssueFacade.EditIssueAsync(issueDto);
            return RedirectToAction("Index", new {id = issue});
        }

         [HttpPost, Route("Ailment/Edit")]
         [PermissionAuthorize(Permission.editAilments)]
         public async Task<ActionResult> DeleteAilmentWithReferences(Guid id)
         {
             var issues = (await IssueFacade.GetAllIssuesAsync(new IssueFilterDto {AilmentId = id})).Items;
             foreach (IssueDto issue in issues)
             {
                 issue.AilmentId = null;
                 try
                 {
                     // The relationship could not be changed because one or more of the foreign-key properties is non-nullable.
                     await IssueFacade.EditIssueAsync(issue);
                 }
                 catch (InvalidOperationException)
                 {
                     int count = issues.Count();                     
                     TempData["AlertMessage"] = $"Failed to delete the ailment because {count} issue{(count == 1 ? "" : "s")} still depend{(count == 1 ? "" : "s")} on it.";
                     return RedirectToAction("Index", "Ailments");
                 }
             }
             
             if (await AilmentFacade.DeleteAilmentAsync(id))
             {
                 TempData["AlertMessage"] = "Ailment was successfuly deleted.";
             }
             else
             {
                 TempData["AlertMessage"] = "Failed to delete ailment.";                 
             }
             return RedirectToAction("Index", "Ailments");
         }

        //ailment delete will probably fail atm if bound to issue

        readonly int symptomItemsPerPage = 21;

        public async Task<PagedList.StaticPagedList<SymptomDto>> GetAllSymptoms(int symptomPage)
        {
            var symptomResult = await AilmentFacade.GetSymptomsAsync(new SymptomFilterDto {RequestedPageNumber = symptomPage, PageSize = symptomItemsPerPage});
            return new PagedList.StaticPagedList<SymptomDto>(symptomResult.Items, symptomPage, symptomItemsPerPage, (int) symptomResult.TotalItemsCount);
        }

    }

}
