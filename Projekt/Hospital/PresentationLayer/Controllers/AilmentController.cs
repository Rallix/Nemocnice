﻿using BusinessLayer.DataTransferObjects;
using BusinessLayer.DataTransferObjects.Filters;
using BusinessLayer.Facades;
using BusinessLayer.Services.Users;
using PresentationLayer.Helpers;
using PresentationLayer.Models;
using PresentationLayer.Models.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;


namespace PresentationLayer.Controllers
{

    public class AilmentsController : BaseController
    {

        // GET: Ailment
        public AilmentFacade AilmentFacade { get; set; }

        readonly int symptomItemsPerPage = 21;
        readonly int ailmentItemsPerPage = 20;

        [PermissionAuthorize(Permission.viewAilments)]
        public async Task<ActionResult> Index(AilmentListModel model = null, int ailmentPage = 1, int symptomPage = 1)
        {
            model = model ?? new AilmentListModel();

            var ailmentFilter = new AilmentFilterDto
            {
                    Name = model.AilmentNameFilter,
                    PageSize = ailmentItemsPerPage,
                    RequestedPageNumber = ailmentPage
            };

            if (model.SymptomGuidsFilter != null && model.SymptomGuidsFilter.Any())
            {
                var ailmentSymptoms = (await AilmentFacade.GetAilmentSymptomsAsync(new AilmentSymptomFilterDto {AnySymptom = model.SymptomGuidsFilter.ToArray()})).Items;
                Guid[] ailmentIdsWithAtLeastOneFilteredSymptom = ailmentSymptoms.Select(asy => asy.AilmentId).ToArray();
                var ailmentsWithAllSymptoms = ailmentIdsWithAtLeastOneFilteredSymptom.Select(async id => new
                {
                        AilmentId = id,
                        SymptomIds = (await AilmentFacade.GetAilmentSymptomsAsync(new AilmentSymptomFilterDto {AilmentId = id})).Items.Select(asy => asy.SymptomId).ToList()
                });

                var ailmentIds = new List<Guid>();
                foreach (var ailmentWithSymptoms in ailmentsWithAllSymptoms)
                {
                    var ASresult = await ailmentWithSymptoms;
                    bool isOk = true;
                    foreach (Guid requiredSymptomId in model.SymptomGuidsFilter)
                    {
                        if (!ASresult.SymptomIds.Contains(requiredSymptomId))
                        {
                            isOk = false;
                            break;
                        }
                    }
                    if (isOk)
                    {
                        ailmentIds.Add(ASresult.AilmentId);
                    }
                }


                ailmentFilter.AnyId = ailmentIds.ToArray();

                if (ailmentFilter.AnyId != null && ailmentFilter.AnyId.Any())
                {
                    var result = await AilmentFacade.GetAilmentsAsync(ailmentFilter);
                    model.Ailments = new PagedList.StaticPagedList<AilmentDto>(result.Items.ToList(), ailmentPage, ailmentItemsPerPage, (int) result.TotalItemsCount);
                }
                else
                {
                    model.Ailments = new PagedList.StaticPagedList<AilmentDto>(new List<AilmentDto>(), 1, ailmentItemsPerPage, 0);
                }
            }
            else
            {
                var result = await AilmentFacade.GetAilmentsAsync(ailmentFilter);
                model.Ailments = new PagedList.StaticPagedList<AilmentDto>(result.Items.ToList(), ailmentPage, ailmentItemsPerPage, (int) result.TotalItemsCount);
            }
            //model.AilmentFilter = ailmentFilter;
            //model.SymptomFilter = model.SymptomFilter ?? new SymptomFilterDto();
            model.Symptoms = await GetAllSymptoms(symptomPage);

            return View("AilmentsView", model);
        }


        [PermissionAuthorize(Permission.editAilments)]
        public async Task<ActionResult> Create()
        {
            var model = new AilmentCreateModel
            {
                    Symptoms = await GetAllSymptoms(1),
                    SymptomFilterActionName = nameof(FilterAilmentEdit)
            };
            return View("AilmentCreateView", model);
        }

        [PermissionAuthorize(Permission.editAilments)]
        public async Task<ActionResult> FilterAilmentEdit(AilmentCreateModel model, int symptomPage = 1)
        {
            model.Symptoms = await GetAllSymptoms(symptomPage);
            return View("AilmentCreateView", model);
        }


        [PermissionAuthorize(Permission.editAilments)]
        public async Task<ActionResult> Edit(string name)
        {
            var ailmentResult = await AilmentFacade.GetAilmentsAsync(new AilmentFilterDto {Name = name});
            if (!ailmentResult.Items.Any())
            {
                return View("AilmentCreateView",
                            new AilmentCreateModel
                            {
                                    Alert = new AlertMessage($"Ailment {name} doesn't exist.", AlertMessage.MessageType.danger)
                            });
            }
            var ailment = ailmentResult.Items.First();
            var model = new AilmentCreateModel
            {
                    AilmentId = ailment.Id,
                    Symptoms = await GetAllSymptoms(1),
                    AilmentName = ailment.Name,
                    SymptomGuidsFilter = (await AilmentFacade.GetAilmentSymptomsAsync(new AilmentSymptomFilterDto {AilmentId = ailment.Id})).Items.Select(i => i.SymptomId).ToList(),
                    SymptomFilterActionName = nameof(FilterAilmentEdit)
            };
            return View("AilmentCreateView", model);
        }


        [HttpPost]
        [PermissionAuthorize(Permission.editAilments)]
        public async Task<ActionResult> Edit(AilmentCreateModel model)
        {
            var returnModel = new AilmentListModel();
            var ailmentResult = await AilmentFacade.GetAilmentsAsync(new AilmentFilterDto {Id = model.AilmentId});
            if (ailmentResult.Items.Any())
            {
                var asResults = await AilmentFacade.GetAilmentSymptomsAsync(new AilmentSymptomFilterDto
                {
                        AilmentId = model.AilmentId
                });

                asResults.Items.ToList().ForEach(async ailmentSymptom => await AilmentFacade.DeleteAilmentSymptomAsync(ailmentSymptom.Id));


                var ailment = ailmentResult.Items.First();
                ailment.Name = model.AilmentName;

                await AilmentFacade.EditAilmentAsync(ailment);

                await AilmentFacade.AddAilmentSymptoms(model.SymptomGuidsFilter.Select(s => new AilmentSymptomDto {AilmentId = ailment.Id, SymptomId = s}).ToList());

                returnModel.Alert = new AlertMessage($"Succesfully edited {model.AilmentName}", AlertMessage.MessageType.success);
            }
            else
            {
                returnModel.Alert = new AlertMessage("Editation failed - Ailment no longer exists.", AlertMessage.MessageType.danger);
            }
            return await Index(returnModel);
        }

        [HttpPost]
        [PermissionAuthorize(Permission.editAilments)]
        public async Task<ActionResult> Delete(Guid id)
        {
            var returnModel = new AilmentListModel();
            if (await AilmentFacade.DeleteAilmentAsync(id))
            {
                returnModel.Alert = new AlertMessage("Ailment was successfuly deleted.", AlertMessage.MessageType.success);
            }
            else
            {
                returnModel.Alert = new AlertMessage("Failed to delete ailment.", AlertMessage.MessageType.danger);
            }
            return await Index(returnModel);
        }

        [HttpPost]
        [PermissionAuthorize(Permission.editAilments)]
        public async Task<ActionResult> Create(AilmentCreateModel model)
        {            
            if (string.IsNullOrWhiteSpace(model.AilmentName))
            {
                ModelState.AddModelError(nameof(model.AilmentName), "The ailment's name can't be blank.");
            }
            if (model.SymptomGuidsFilter == null)
            {
                ModelState.AddModelError(nameof(model.SymptomGuidsFilter), "An ailment can't have no symptoms.");
            }
            if (!ModelState.IsValid)
            {
                var returnModel = new AilmentCreateModel
                {
                        Symptoms = await GetAllSymptoms(1),
                        SymptomFilterActionName = nameof(FilterAilmentEdit)
                };
                return View("AilmentCreateView", returnModel);
            }

            await AilmentFacade.CreateAilmentWithSymptoms(model.AilmentName, model.SymptomGuidsFilter);

            model = new AilmentCreateModel
            {
                    Symptoms = await GetAllSymptoms(1),
                    SymptomFilterActionName = nameof(FilterAilmentEdit),
                    Alert = new AlertMessage($"Ailment {model.AilmentName} succesfully created.",
                                             AlertMessage.MessageType.success),
            };
            return View("AilmentCreateView", model);
        }

        public async Task<PagedList.StaticPagedList<SymptomDto>> GetAllSymptoms(int symptomPage)
        {
            var symptomResult = await AilmentFacade.GetSymptomsAsync(new SymptomFilterDto {RequestedPageNumber = symptomPage, PageSize = symptomItemsPerPage});
            return new PagedList.StaticPagedList<SymptomDto>(symptomResult.Items, symptomPage, symptomItemsPerPage, (int) symptomResult.TotalItemsCount);
        }

    }

}
