﻿using System.Collections.Generic;
using BusinessLayer.DataTransferObjects;
using BusinessLayer.DataTransferObjects.Filters;
using BusinessLayer.Facades;
using BusinessLayer.Services.Users;
using PresentationLayer.Helpers;
using PresentationLayer.Models;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace PresentationLayer.Controllers
{
    public class CardController : BaseController
    {

        public IssueFacade IssueFacade { get; set; }
        readonly int issuePageSize = 10;

        [Route("User/{username}/Card/")]
        [PermissionAuthorize("username", Permission.accessCards)]
        public async Task<ActionResult> Index(string username, int solvedPage = 1, int unsolvedPage = 1)
        {
            var model = new CardDetailModel();
            if (username != null)
            {
                var userResult = await UserFacade.GetUsersAsync(new UserFilterDto { Username = username });

                if (userResult.Items.Any())
                {                    
                    model.UserExists = true;
                    model.User = userResult.Items.First();
                    
                    CardDto card = await UserFacade.GetUserCard(model.User.Id);
                    if (card != null)
                    {
                        model.CardExists = true;
                        
                        var unsolvedIssues = await IssueFacade.GetAllIssuesAsync(new IssueFilterDto
                        {
                            Card = card.Id,
                            Solved = false,
                            PageSize = issuePageSize,
                            RequestedPageNumber = unsolvedPage
                        });
                        var solvedIssues = await IssueFacade.GetAllIssuesAsync(new IssueFilterDto
                        {
                            Card = card.Id,
                            Solved = true,
                            PageSize = issuePageSize,
                            RequestedPageNumber = solvedPage
                        });
                        foreach (IssueDto issue in solvedIssues.Items.Concat(unsolvedIssues.Items)) {                            
                            issue.Treatments = await IssueFacade.GetTreatmentsOfIssue(issue.Id) as List<TreatmentDto>;
                            issue.Ailment = await IssueFacade.GetAilmentOfIssue(issue.Id);
                        }

                        model.SolvedIssues = new PagedList.StaticPagedList<IssueDto>(solvedIssues.Items.ToList(), solvedPage, issuePageSize, (int)solvedIssues.TotalItemsCount);
                        model.UnsolvedIssues = new PagedList.StaticPagedList<IssueDto>(unsolvedIssues.Items.ToList(), unsolvedPage, issuePageSize, (int)unsolvedIssues.TotalItemsCount);
                    }
                }

            }
            return View("CardView", model);
        }

        [Route("User/{username}/Card/Create/")]
        [PermissionAuthorize(Permission.editCards)]
        public async Task<ActionResult> Create(string username)
        {
            var userResult = await UserFacade.GetUsersAsync(new UserFilterDto { Username = username });
            await UserFacade.CreateCard(userResult.Items.First().Id);
            return RedirectToAction(nameof(Index), GetType().Name.Replace("Controller", ""), new {username});

        }
       
    }
}