﻿using BusinessLayer.DataTransferObjects;
using BusinessLayer.DataTransferObjects.Filters;
using BusinessLayer.Facades;
using BusinessLayer.Services.Users;
using PagedList;
using PresentationLayer.Helpers;
using PresentationLayer.Models;
using PresentationLayer.Models.Base;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace PresentationLayer.Controllers
{
    public class SymptomsController : BaseController
    {
        // GET: Symptom
        public AilmentFacade AilmentFacade { get; set; }
        private readonly int pageSize = 20;

        [PermissionAuthorize(Permission.viewSymptoms)]
        public async Task<ActionResult> Index(int page = 1)
        {

            var result = await AilmentFacade.GetSymptomsAsync(new SymptomFilterDto
            {
                PageSize = pageSize,
                RequestedPageNumber = page
            });

            return View("SymptomListView", new SymptomListModel {
                Symptoms = new StaticPagedList<SymptomDto>(result.Items,
                                                           page, pageSize, (int)result.TotalItemsCount),
            });
        }

        [PermissionAuthorize(Permission.editSymptoms)]
        public ActionResult Create()
        {
            return View("SymptomAddView");
        }

        [HttpPost]
        [PermissionAuthorize(Permission.editSymptoms)]
        public async Task<ActionResult> Create(SymptomAddModel model)
        {
            if (await AilmentFacade.SymptomExists(model.Description))
            {
                ModelState.AddModelError(nameof(model.Description), $"Symptom \"{model.Description}\" already exists.");
            }
            if (!ModelState.IsValid)
            {
                return View("SymptomAddView", model);
            }

            await AilmentFacade.AddSymptom(new SymptomDto { Description = model.Description });
            ModelState.Clear();
            return View("SymptomAddView", 
                        new SymptomAddModel{
                            Alert = new AlertMessage($"Succesfully added symptom \"{model.Description}\"",
                                                     AlertMessage.MessageType.success) }
                        );
        }

        [HttpPost]
        [PermissionAuthorize(Permission.editSymptoms)]
        public async Task<ActionResult> Delete(Guid id)
        {
            if (!await AilmentFacade.DeleteSymptomAsync(id))
            {
                return Redirect(nameof(Index));
                // TODO: Add a failure message
            }
            return Redirect(nameof(Index));
        }
    }
}