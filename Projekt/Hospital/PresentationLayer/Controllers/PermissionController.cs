﻿using BusinessLayer.DataTransferObjects.Enums;
using BusinessLayer.Services.Users;
using PresentationLayer.Helpers;
using PresentationLayer.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace PresentationLayer.Controllers
{

    public class PermissionsController : BaseController
    {

        // GET: Permission
        [PermissionAuthorize(RoleTypes.Director, Permission.editPermissions)]
        public async Task<ActionResult> Index()
        {
            var model = new PermissionModel()
            {
                    RolePermissions = new Dictionary<string, string[]>()
            };
            foreach (RoleTypes roleType in (RoleTypes[]) Enum.GetValues(typeof(RoleTypes)))
            {
                var roleDto = await UserFacade.GetOrCreateRole(roleType);
                string[] permissions = await UserFacade.GetPermissionsOfRole(roleDto.Id);
                model.RolePermissions.Add(roleType.ToString(), permissions);
            }
            /*
            var doctor = await UserFacade.GetOrCreateRole(RoleTypes.Doctor);
            var patient = await UserFacade.GetOrCreateRole(RoleTypes.Patient);

            var model = new PermissionModel
            {
                DoctorPermissions = await UserFacade.GetPermissionsOfRole(doctor.Id),
                PatientPermissions = await UserFacade.GetPermissionsOfRole(patient.Id)
            }
            */
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Index(PermissionModel model)
        {
            foreach (var rolePermissions in model.RolePermissions)
            {
                if (Enum.TryParse(rolePermissions.Key, out RoleTypes roleType))
                {
                    await UserFacade.SetPermissionsForRole(roleType, rolePermissions.Value);
                }
            }
            return RedirectToAction(nameof(Index)); // View(model) makes groups with no permissions dissapear
        }

    }

}
