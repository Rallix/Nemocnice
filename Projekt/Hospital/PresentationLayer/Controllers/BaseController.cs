﻿using BusinessLayer.DataTransferObjects.Enums;
using BusinessLayer.Facades;
using BusinessLayer.Services.Users;
using MailKit.Net.Smtp;
using MailKit.Security;
using MimeKit;
using PresentationLayer.Helpers;
using PresentationLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;


namespace PresentationLayer.Controllers
{
    public class BaseController : Controller
    {
        public UserFacade UserFacade { get; set; }
        private string[] permissions;

        public BaseController() : base()
        {
            ViewBag.HasPermission = (Func<Permission, bool>)((Permission p) =>
            {
                return HasPermission(p);
            });
        }

        public bool HasPermission(Permission p)
        {
            if (permissions == null)
            {
                if (User.Identity != null && User.Identity.IsAuthenticated)
                {
                    var userPermissionsTask = Task.Run(async () => await UserFacade.GetPermissionsOfUser(User.Identity.Name)).ConfigureAwait(false);
                    permissions = userPermissionsTask.GetAwaiter().GetResult();
                }
                else
                {
                    var roleTask = Task.Run(async () => await UserFacade.GetOrCreateRole(RoleTypes.Unregistered)).ConfigureAwait(false);
                    var role = roleTask.GetAwaiter().GetResult();
                    var userPermissionsTask = Task.Run(async () => await UserFacade.GetPermissionsOfRole(role.Id)).ConfigureAwait(false);
                    permissions = userPermissionsTask.GetAwaiter().GetResult();
                }
            }
            return permissions.Contains(p.ToString());
        }

        private readonly string emailAddress = "hospital.informer@seznam.cz";
        private readonly string emailPassword = "strongHospitalPW";

        public void SendEmail(string address, string subject, string content)
        {
            var message = new MimeMessage();
            message.From.Add(new MailboxAddress(emailAddress));
            message.To.Add(new MailboxAddress(address));
            message.Subject = subject;
            message.Body = new TextPart("plain") { Text = content };
            using (var client = new SmtpClient())
            {
                client.Connect("smtp.seznam.cz", 465, SecureSocketOptions.SslOnConnect);
                client.Authenticate(emailAddress, emailPassword);
                client.Send(message);
                client.Disconnect(true);
            }
        }

        [ChildActionOnly]
        public ActionResult Navigation()
        {
            return PartialView("Navigation", new LoginModel());
        }
        
    }
}