﻿using System;
using BusinessLayer.DataTransferObjects;
using PresentationLayer.Models.Base;
using System.Collections.Generic;

namespace PresentationLayer.Models
{
    public class IssueDetailModel : BaseModel
    {
        public IssueDto Issue { get; set; }

        public IEnumerable<TreatmentDto> Treatments { get; set; }
        public IEnumerable<SymptomDto> AllSymptoms { get; set; }
        public IEnumerable<(TreatmentDto Treatment, IEnumerable<SymptomDto> Symptoms)> DiscoveredSymptoms { get; set; }

        public IEnumerable<AilmentDto> ApplicableAilments { get; set; }
        public Guid? ChosenAilment { get; set; }
    }
}