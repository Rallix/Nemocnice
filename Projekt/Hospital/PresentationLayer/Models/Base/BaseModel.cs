﻿namespace PresentationLayer.Models.Base
{
    public class AlertMessage
    {
        public AlertMessage(string message, MessageType type)
        {
            Message = message;
            this.type = type;
        }

        public enum MessageType
        {
            primary,
            secondary,
            success,
            danger,
            warning,
            info,
            light,
            dark
        }
        public string Message { get; set; }
        readonly MessageType type;
        public string Type {
            get {
                return "alert alert-" + type.ToString();
            }
        }
    }
    public class BaseModel
    {
        public AlertMessage Alert { get; set; }
        public bool HasAlert()
        {
            return Alert != null;
        }
    }
   
}