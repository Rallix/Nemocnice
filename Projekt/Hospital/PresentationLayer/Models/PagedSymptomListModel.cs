﻿using BusinessLayer.DataTransferObjects;
using PagedList;
using PresentationLayer.Models.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PresentationLayer.Models
{
    public class PagedSymptomListModel : BaseModel
    {
        public StaticPagedList<SymptomDto> Symptoms { get; set; }
        public IEnumerable<Guid> SymptomGuidsFilter { get; set; }
        public string SymptomFilterActionName { get; set; }
    }
}