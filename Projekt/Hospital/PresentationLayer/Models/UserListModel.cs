﻿using BusinessLayer.DataTransferObjects;
using BusinessLayer.DataTransferObjects.Filters;
using PagedList;
using PresentationLayer.Models.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PresentationLayer.Models
{
    public class UserListModel : BaseModel
    {
        public StaticPagedList<UserDto> Users { get; set; }
        //public UserFilterDto Filter { get; set; }

        public string FirstNameFilter { get; set; }
        public string SurnameFilter { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? BirthDateFilter { get; set; }
        public string BirthNumFilter { get; set; }        

    }
}