﻿using PresentationLayer.Helpers;
using PresentationLayer.Models.Base;
using System;
using System.ComponentModel.DataAnnotations;

namespace PresentationLayer.Models
{
    public class PatientRegisterModel : BaseModel
    {
        [Required, EmailAddress]
        public string Email { get; set; }

        [Required, BirthNumberValidator("Invalid Birth number, the required format is \"XXXXXX\\XXXX")]
        public string BirthNumber { get; set; }

        [Required]
        public DateTime BirthDate { get; set; }

        [Required, MaxLength(30)] public string FirstName { get; set; }

        [Required, MaxLength(30)] public string Surname { get; set; }

        [MaxLength(30)]
        public string Username { get; set; }
    }
}
