﻿using BusinessLayer.DataTransferObjects;
using PagedList;
using PresentationLayer.Models.Base;

namespace PresentationLayer.Models
{
    public class CardDetailModel : BaseModel
    {
        public StaticPagedList<IssueDto> SolvedIssues { get; set; }
        public StaticPagedList<IssueDto> UnsolvedIssues { get; set; }
        public bool UserExists { get; set; }
        public bool CardExists { get; set; }        
        public UserDto User { get; set; }
    }
}