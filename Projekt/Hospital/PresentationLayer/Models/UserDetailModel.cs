﻿using BusinessLayer.DataTransferObjects;
using PresentationLayer.Models.Base;

namespace PresentationLayer.Models
{
    public class UserDetailModel : BaseModel
    {
        public UserDto UserDetail { get; set; }        
    }
}