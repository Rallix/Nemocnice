﻿using System;
using System.Collections.Generic;
using BusinessLayer.DataTransferObjects;

namespace PresentationLayer.Models
{
    public class IssueAddModel : TreatmentAddModel
    {
        public IEnumerable<AilmentDto> Ailments { get; set; }
        public Guid? ChosenAilment { get; set; }
    }
}