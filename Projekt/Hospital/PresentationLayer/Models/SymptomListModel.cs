﻿using BusinessLayer.DataTransferObjects;
using PagedList;
using PresentationLayer.Models.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PresentationLayer.Models
{
    public class SymptomListModel : BaseModel
    {
        public StaticPagedList<SymptomDto> Symptoms { get; set; }
    }
}