﻿using BusinessLayer.DataTransferObjects.Enums;
using PresentationLayer.Helpers;
using PresentationLayer.Models.Base;
using System;
using System.ComponentModel.DataAnnotations;

namespace PresentationLayer.Models
{
    public class UserRegisterModel : BaseModel
    {
        [Required, MaxLength(32)]
        public string Username { get; set; }

        [Required, BirthNumberValidator("Invalid Birth number, the required format is \"XXXXXX\\XXXX")]
        public string BirthNumber { get; set; }

        [Required]
        public DateTime BirthDate { get; set; }

        [Required, MaxLength(64)]
        public string Password { get; set; }

        [Required, RoleValidator("Can't register as unregistered")]
        public RoleTypes Role { get; set; }

        [Required, MaxLength(30)] public string FirstName { get; set; }

        [Required, MaxLength(30)] public string Surname { get; set; }

        public class RoleValidator : ValidationAttribute
        {
            public RoleValidator(string errorMessage) : base(errorMessage) { }
            public override bool IsValid(object value)
            {
                var val = (RoleTypes)value;
                return val != RoleTypes.Unregistered;
            }
        }

       
    }
}