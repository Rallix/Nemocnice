﻿using System;

namespace PresentationLayer.Models
{
    public class TreatmentAddModel : PagedSymptomListModel
    {
        public DateTime Date { get; set; }
        public string Message { get; set; }
        public Guid? IssueId { get; set; }
    }
}