﻿using BusinessLayer.DataTransferObjects.Enums;
using PresentationLayer.Models.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PresentationLayer.Models
{
    public class PermissionModel : BaseModel
    {
        public Dictionary<string, string[]> RolePermissions { get; set; }
       /* public string[] DoctorPermissions { get; set; }
        public string[] PatientPermissions { get; set; }*/
    }
}