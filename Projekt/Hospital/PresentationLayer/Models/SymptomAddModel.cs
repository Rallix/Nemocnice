﻿using PresentationLayer.Models.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PresentationLayer.Models
{
    public class SymptomAddModel : BaseModel
    {
        [MaxLength(25), MinLength(1)]
        public string Description { get; set; }
    }
}