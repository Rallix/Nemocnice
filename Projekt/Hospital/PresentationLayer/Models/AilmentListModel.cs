﻿using BusinessLayer.DataTransferObjects;
using BusinessLayer.DataTransferObjects.Filters;
using PagedList;
using PresentationLayer.Models.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PresentationLayer.Models
{
    public class AilmentListModel : PagedSymptomListModel
    {
        public StaticPagedList<AilmentDto> Ailments { get; set; }
        
        //public AilmentFilterDto AilmentFilter { get; set; }
        //public SymptomFilterDto SymptomFilter { get; set; }
        public string AilmentNameFilter { get; set; }

        
    }
}