﻿using System;

namespace PresentationLayer.Models
{
    public class AilmentCreateModel : PagedSymptomListModel
    {
        public string AilmentName { get; set; }
        public Guid? AilmentId { get; set; }
    }
}