﻿using Castle.Windsor;
using PresentationLayer.App_Start.Windsor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Async;

public class WindsorActionInvoker : AsyncControllerActionInvoker
{
    readonly IWindsorContainer container;

    public WindsorActionInvoker(IWindsorContainer container)
    {
        this.container = container;
    }



    protected override ActionExecutedContext InvokeActionMethodWithFilters(
            ControllerContext controllerContext,
            IList<IActionFilter> filters,
            ActionDescriptor actionDescriptor,
            IDictionary<string, object> parameters)
    {
        foreach (IActionFilter actionFilter in filters)
        {
            container.Kernel.InjectProperties(actionFilter);
        }
        return base.InvokeActionMethodWithFilters(controllerContext, filters, actionDescriptor, parameters);
    }
}
