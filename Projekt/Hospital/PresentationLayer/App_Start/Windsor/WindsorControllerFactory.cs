﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Castle.MicroKernel;
using Castle.Windsor;
using PresentationLayer.App_Start.Windsor;

namespace PresentationLayer.Windsor
{
    public class WindsorControllerFactory : DefaultControllerFactory
    {
        private readonly IKernel kernel;
        readonly IWindsorContainer container;

        public WindsorControllerFactory(IKernel kernel, IWindsorContainer container)
        {
            this.kernel = kernel;
            this.container = container;
            DependencyResolver.SetResolver(new WindsorDependencyResolver(container));
        }

        public override void ReleaseController(IController controller)
        {
            kernel.ReleaseComponent(controller);
        }

        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            if (controllerType == null)
            {
                throw new HttpException(404, $"The controller for path '{requestContext.HttpContext.Request.Path}' could not be found.");
            }

            Controller controller =  (Controller)kernel.Resolve(controllerType);

            if (controller != null)
            {
                controller.ActionInvoker = new WindsorActionInvoker(container);
            }

            return controller;
        }
    }
}