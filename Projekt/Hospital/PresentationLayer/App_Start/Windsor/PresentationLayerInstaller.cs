﻿using System.Web.Mvc;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using PresentationLayer.Helpers;

namespace PresentationLayer.Windsor
{
    public class PresentationLayerInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {

            container.Register(
                Classes.FromThisAssembly()
                    .BasedOn<IController>()
                    .LifestyleTransient()
                
            );

            container.Register(
                Classes.FromThisAssembly()
                    .BasedOn<IActionFilter>()
                    .LifestyleTransient());

        }
    }
}