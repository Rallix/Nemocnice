﻿using BusinessLayer.DataTransferObjects.Enums;
using BusinessLayer.Facades;
using BusinessLayer.Services.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace PresentationLayer.Helpers
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class PermissionAuthorizeAttribute : AuthorizeAttribute
    {
        private readonly string[] requiredPermissions;
        private RoleTypes? allowedRole;
        private readonly string usernameAttr;
        public UserFacade UserFacade { get { return DependencyResolver.Current.GetService<UserFacade>(); } }


        public PermissionAuthorizeAttribute(params Permission[] requiredPermissions)
        {
            this.requiredPermissions = requiredPermissions.Select(p=>p.ToString()).ToArray();
        }

        public PermissionAuthorizeAttribute(string nameAttrLink, params Permission[] requiredPermissions) : this(requiredPermissions)
        {              
            usernameAttr = nameAttrLink;
        }

        public PermissionAuthorizeAttribute(RoleTypes exceptRole, params Permission[] requiredPermissions) : this(requiredPermissions)
        {
            allowedRole = exceptRole;
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            string[] userPermissions;
            var authorized = base.AuthorizeCore(httpContext);
            if (!authorized)
            {
                if (allowedRole == RoleTypes.Unregistered)
                {
                    return true;
                }
                var unregisteredTask = Task.Run(async () => await UserFacade.GetOrCreateRole(RoleTypes.Unregistered)).ConfigureAwait(false);
                var unregistered = unregisteredTask.GetAwaiter().GetResult();
                var userPermissionsTask = Task.Run(async () => await UserFacade.GetPermissionsOfRole(unregistered.Id)).ConfigureAwait(false);
                userPermissions = userPermissionsTask.GetAwaiter().GetResult();
            }
            else
            {
                if (usernameAttr != null)
                {
                    var username = (string)httpContext.Request.RequestContext.RouteData.Values[usernameAttr];
                    if (username != null && username == httpContext.User.Identity.Name)
                    {
                        return true;
                    }

                }
                if (allowedRole != null && httpContext.User.IsInRole(allowedRole.ToString()))
                {
                    return true;
                }
                string authenticatedUser = httpContext.User.Identity.Name;
                var userPermissionsTask = Task.Run(async () => await UserFacade.GetPermissionsOfUser(authenticatedUser)).ConfigureAwait(false);
                userPermissions = userPermissionsTask.GetAwaiter().GetResult();
            }

            if (!requiredPermissions.Any()) return false;

            foreach (var req in requiredPermissions)
            {
                if (!userPermissions.Contains(req))
                {
                    return false;
                }
            }
            return true;
           
          
        }
    }
}