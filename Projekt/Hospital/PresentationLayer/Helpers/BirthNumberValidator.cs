﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace PresentationLayer.Helpers
{
    public class BirthNumberValidator : ValidationAttribute
    {
        public BirthNumberValidator(string errorMessage) : base(errorMessage) { }

        public override bool IsValid(object value)
        {
            if (value == null) return false;
            var val = (string)value;
            var rx = new Regex(@"^[0-9]{6}/[0-9]{4}$");
            MatchCollection matches = rx.Matches(val);
            return matches.Count != 0;
        }
    }
}