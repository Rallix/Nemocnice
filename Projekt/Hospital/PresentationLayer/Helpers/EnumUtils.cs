﻿using System;
using System.ComponentModel;
using System.Reflection;

namespace PresentationLayer.Helpers {
    public class EnumUtils {

        /// <summary> Gets the [Description] attribute for an enum. </summary>
        /// <param name="value">An Enum value</param>
        /// <returns>The contents of the [Description] attribute.</returns>
        public static string GetEnumDescription(Enum value)
        {
            FieldInfo fieldInfo = value.GetType().GetField(value.ToString());
            var attributes = (DescriptionAttribute[]) fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : value.ToString();
        }
    }
}