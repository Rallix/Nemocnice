﻿using System;
using System.Collections.Generic;
using System.Web;
using Newtonsoft.Json;

namespace PresentationLayer.Helpers.Cookies
{

    /// <summary>
    /// A template class showing how to store cookies.
    /// </summary>
    public static class PlaceholderCookieManager
    {

        // TODO: Edit and replace when cookies are needed
        public class PlaceholderDto
        {
            public string LengthyText { get; set; }
        }


        #region Constants

        const string BaseCookieName = "Placeholder";

        const string CookieValue = "PlaceholderItems";

        public const int CookieExpirationInMinutes = 60;

        #endregion

        #region PlaceholderManagement

        /// <summary>
        /// Gets all placeholder items from cookie within request according to given user's username
        /// </summary>
        /// <param name="request">HTTP request containing placeholder cookie for given user</param>
        /// <param name="username">Username of the cookie owner</param>
        /// <returns>List of placeholder items</returns>
        public static IList<PlaceholderDto> GetAllPlaceholderItems(this HttpRequestBase request, string username)
        {
            var cookie = GetPlaceholderCookie(request, username);
            if (cookie == null)
            {
                return new List<PlaceholderDto>();
            }
            var placeholderJson = cookie.Values[CookieValue];
            var items = Deserialize<List<PlaceholderDto>>(placeholderJson);
            return items ?? new List<PlaceholderDto>();
        }

        /// <summary>
        /// Saves given placeholder items to corresponding cookie
        /// </summary>
        /// <param name="response">HTTP response to save the placeholder items cookie to</param>
        /// <param name="placeholderItems">Placeholder items to save</param>
        /// <param name="username">Username of the cookie owner</param>
        public static void SaveAllPlaceholderItems(this HttpResponseBase response, string username, IEnumerable<PlaceholderDto> placeholderItems = null)
        {
            ShrinkPlaceholderItem(placeholderItems);
            var placeholderJson = Serialize(placeholderItems ?? new List<PlaceholderDto>());
            var cookie = GetPlaceholderCookie(response, username);
            cookie.Expires = DateTime.Now.AddMinutes(CookieExpirationInMinutes);
            cookie[CookieValue] = placeholderJson;
        }

        /// <summary>
        /// Clears all placeholder items
        /// </summary>
        /// <param name="response">HTTP response to save the placeholder items cookie to</param>
        /// <param name="username">Username of the cookie owner</param>
        public static void ClearAllPlaceholderItems(this HttpResponseBase response, string username)
        {
            // Saving null collection will cause placeholder items cookie clearing
            response.SaveAllPlaceholderItems(username);
        }

        /// <summary>
        /// Reduces placeholder size, so it does not take much space within the cookie
        /// </summary>
        /// <param name="placeholderItems">Placeholder items to reduce</param>
        static void ShrinkPlaceholderItem(IEnumerable<PlaceholderDto> placeholderItems)
        {
            foreach (var placeholderItem in placeholderItems ?? new List<PlaceholderDto>())
            {
                placeholderItem.LengthyText = "";
            }
        }

        #endregion

        #region CookieManagement

        /// <summary>
        /// Creates new PlaceholderCookie for the given HTTP response
        /// </summary>
        /// <param name="response">HTTP response</param>
        /// <param name="username">Username of the cookie owner</param>
        static void CreatePlaceholderCookieCore(this HttpResponseBase response, string username)
        {
            var cookieName = BaseCookieName + username;
            var cookie = new HttpCookie(cookieName)
            {
                    Expires = DateTime.Now.AddMinutes(CookieExpirationInMinutes)
            };
            cookie.Values[CookieValue] = Serialize(new List<PlaceholderDto>());
            response.Cookies.Add(cookie);
        }

        #endregion

        #region CookieRetrieval

        /// <summary>
        /// Gets Cookie from request
        /// </summary>
        /// <param name="request">HTTP request containing placeholder cookie for given user</param>
        /// <param name="username">Username of the cookie owner</param>
        /// <returns>Corresponding cookie</returns>
        static HttpCookie GetPlaceholderCookie(this HttpRequestBase request, string username)
        {
            if (string.IsNullOrWhiteSpace(username))
            {
                throw new ArgumentException("Username cannot be blank.");
            }
            return request.Cookies[BaseCookieName + username];
        }

        /// <summary>
        /// Gets Cookie from response.
        /// </summary>
        /// <param name="response">HTTP response containing placeholder cookie for given user.</param>
        /// <param name="username">Username of the cookie owner.</param>
        /// <returns>Corresponding cookie.</returns>
        static HttpCookie GetPlaceholderCookie(this HttpResponseBase response, string username)
        {
            if (string.IsNullOrWhiteSpace(username))
            {
                throw new ArgumentException("Username cannot be blank.");
            }
            HttpCookie cookie = response.Cookies[BaseCookieName + username];
            if (cookie == null)
            {
                CreatePlaceholderCookieCore(response, username);
                return response.Cookies[BaseCookieName + username];
            }
            return cookie;
        }

        #endregion

        #region Serialization

        static string Serialize<T>(T data) where T : class
        {
            return JsonConvert.SerializeObject(data);
        }

        static T Deserialize<T>(string data) where T : class
        {
            return JsonConvert.DeserializeObject<T>(data);
        }

        #endregion

    }

}
