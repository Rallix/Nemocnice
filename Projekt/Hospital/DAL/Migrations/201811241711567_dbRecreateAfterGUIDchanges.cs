namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dbRecreateAfterGUIDchanges : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Ailments",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false, maxLength: 64),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true);
            
            CreateTable(
                "dbo.AilmentSymptoms",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        AilmentId = c.Guid(nullable: false),
                        SymptomId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Ailments", t => t.AilmentId, cascadeDelete: true)
                .ForeignKey("dbo.Symptoms", t => t.SymptomId, cascadeDelete: true)
                .Index(t => t.AilmentId)
                .Index(t => t.SymptomId);
            
            CreateTable(
                "dbo.Symptoms",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Description = c.String(nullable: false, maxLength: 25),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Description, unique: true);
            
            CreateTable(
                "dbo.Cards",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        UserId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.Issues",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Since = c.DateTime(nullable: false),
                        Until = c.DateTime(nullable: false),
                        Solved = c.Boolean(nullable: false),
                        CardId = c.Guid(nullable: false),
                        AilmentId = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Ailments", t => t.AilmentId)
                .ForeignKey("dbo.Cards", t => t.CardId, cascadeDelete: true)
                .Index(t => t.CardId)
                .Index(t => t.AilmentId);
            
            CreateTable(
                "dbo.Treatments",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Date = c.DateTime(nullable: false),
                        Message = c.String(maxLength: 1000),
                        IssueId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Issues", t => t.IssueId, cascadeDelete: true)
                .Index(t => t.IssueId);
            
            CreateTable(
                "dbo.TreatmentSymptoms",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        SymptomId = c.Guid(nullable: false),
                        TreatmentId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Symptoms", t => t.SymptomId, cascadeDelete: true)
                .ForeignKey("dbo.Treatments", t => t.TreatmentId, cascadeDelete: true)
                .Index(t => t.SymptomId)
                .Index(t => t.TreatmentId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Username = c.String(nullable: false, maxLength: 32),
                        Password = c.String(nullable: false, maxLength: 64),
                        BirthNumber = c.String(nullable: false, maxLength: 11),
                        BirthDate = c.DateTime(nullable: false),
                        Alive = c.Boolean(nullable: false),
                        CardId = c.Guid(),
                        Role_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Roles", t => t.Role_Id, cascadeDelete: true)
                .Index(t => t.Username, unique: true)
                .Index(t => t.Role_Id);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        RoleType = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Permissions",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Action = c.String(nullable: false),
                        RoleId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Roles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.RoleId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Cards", "Id", "dbo.Users");
            DropForeignKey("dbo.Users", "Role_Id", "dbo.Roles");
            DropForeignKey("dbo.Permissions", "RoleId", "dbo.Roles");
            DropForeignKey("dbo.TreatmentSymptoms", "TreatmentId", "dbo.Treatments");
            DropForeignKey("dbo.TreatmentSymptoms", "SymptomId", "dbo.Symptoms");
            DropForeignKey("dbo.Treatments", "IssueId", "dbo.Issues");
            DropForeignKey("dbo.Issues", "CardId", "dbo.Cards");
            DropForeignKey("dbo.Issues", "AilmentId", "dbo.Ailments");
            DropForeignKey("dbo.AilmentSymptoms", "SymptomId", "dbo.Symptoms");
            DropForeignKey("dbo.AilmentSymptoms", "AilmentId", "dbo.Ailments");
            DropIndex("dbo.Permissions", new[] { "RoleId" });
            DropIndex("dbo.Users", new[] { "Role_Id" });
            DropIndex("dbo.Users", new[] { "Username" });
            DropIndex("dbo.TreatmentSymptoms", new[] { "TreatmentId" });
            DropIndex("dbo.TreatmentSymptoms", new[] { "SymptomId" });
            DropIndex("dbo.Treatments", new[] { "IssueId" });
            DropIndex("dbo.Issues", new[] { "AilmentId" });
            DropIndex("dbo.Issues", new[] { "CardId" });
            DropIndex("dbo.Cards", new[] { "Id" });
            DropIndex("dbo.Symptoms", new[] { "Description" });
            DropIndex("dbo.AilmentSymptoms", new[] { "SymptomId" });
            DropIndex("dbo.AilmentSymptoms", new[] { "AilmentId" });
            DropIndex("dbo.Ailments", new[] { "Name" });
            DropTable("dbo.Permissions");
            DropTable("dbo.Roles");
            DropTable("dbo.Users");
            DropTable("dbo.TreatmentSymptoms");
            DropTable("dbo.Treatments");
            DropTable("dbo.Issues");
            DropTable("dbo.Cards");
            DropTable("dbo.Symptoms");
            DropTable("dbo.AilmentSymptoms");
            DropTable("dbo.Ailments");
        }
    }
}
